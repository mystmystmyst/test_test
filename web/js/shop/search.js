/* 
 * Myst 
 * mystsys@gmail.com
 */

$(document).ready(function () {

	$('#search').keyup(function () {
		console.log($(this).val());//берем значение
		var input = $(this);	//выбираем элемент в котором идет набор символов
		var atr = input.val();	
		var catid = '';
		$.ajax({
			url: "index.php?r=shop/filter/find",
			dataType: "json",
			type: "GET",
			data: {value: $(this).val(), catid:catid},
			success: function (data) {
				var d = $.parseJSON(data); //массив с резулт. поиска

				//отображаем результаты поиска
				$("#search-result").remove();	//очищаем результаты		
				input.after('<div id="search-result"></div>'); //строим родительский блок для результатов поиска
				console.log(d);
				if (d.length === 0) {
					$('#search-result').append('<div class="result-item">Ничего не найдено</div>');
				} else {
					//ДОБАВЛЯЕМ ДОЧЕРНИЕ БЛОКИ ИЗ МАССИВА	
					$.each(d, function (k, v) {
						$('#search-result').append('<div class="result-item"><a href="index.php?r=shop/product/view&id=' + v.product_id + '">' + v.value + '</a></div>');
					});
				}
			}
		});
	});
	
	//клик по категории
	$("ul.dropdown-menu li").click(function(e) {
		catid = $(this).data("id");	//берем категорию
		$('button').text($(this).children('a').text());
});

	//закрытие окна результатов
	$("body").click(function(e) {
	if($(e.target).closest("#search-result").length==0) $("#search-result").addClass("hidden");
});

//MAIN IMG

$('.thumbs img').hover(function() {
	src = $(this).data("src");
        $('.main-image img').attr('src', 'images/shop/products/'+src);
    });

});


