/* 
 * Myst 
 * mystsys@gmail.com
 */

$(document).ready(function () {
	$(function () {
		//вкл popover
		$('[data-toggle="popover"]').popover();
	});

	$('#search').keyup(function () {
		console.log($(this).val());//берем значение
		var input = $(this);	//выбираем элемент в котором идет набор символов
		var atr = input.val();	
		var catid = '';
		$.ajax({
			url: "index.php?r=shop/filter/find",
			dataType: "json",
			type: "GET",
			data: {value: $(this).val(), catid:catid},
			success: function (data) {
				var d = $.parseJSON(data); //массив с резулт. поиска

				//отображаем результаты поиска
				$("#search-result").remove();	//очищаем результаты		
				input.after('<div id="search-result"></div>'); //строим родительский блок для результатов поиска
				console.log(d);
				if (d.length === 0) {
					$('#search-result').append('<div class="result-item">Ничего не найдено</div>');
				} else {
					//ДОБАВЛЯЕМ ДОЧЕРНИЕ БЛОКИ ИЗ МАССИВА	
					$.each(d, function (k, v) {
						$('#search-result').append('<div class="result-item"><a href="index.php?r=shop/product/view&id=' + v.product_id + '">' + v.value + '</a></div>');
					});
				}
			}
		});
	});
	
	//клик по категории
	$("ul.dropdown-menu li").click(function(e) {
		catid = $(this).data("id");	//берем категорию
		$('button').text($(this).children('a').text());
});

	//закрытие окна результатов
	$("body").click(function(e) {
	if($(e.target).closest("#search-result").length==0) $("#search-result").addClass("hidden");
});


//ТАБЛИЦА ЗАКАЗОВ
//открыть подробности1
$('.open_order').click(function () {
	id = $(this).parents('tr').data("key");
	$(this).addClass('close_order btn-default').removeClass('open_order btn-info ').text('Свернуть');
	row= $(this).parents('tr');
	$.ajax({
			url: "index.php?r=admin/shop/order/orderdetails",
			//dataType: "json",
			type: "GET",
			data: {orderid: id},
			success: function (data) {
				details = data; //массив с резулт. поиска
				console.log(data);
				
				//под строкой вводим строку с деталями
				row.after('<tr id="order-details"><td  colspan="12">'+details+'</td></tr>'); 

			}			
		});
});

//закрыть окно подробностей1
$('.close_order').click(function () {
	alert('www');
	//$(this).parents('tr').on()
});
//закрытие окна order-details
$('tbody').on('click', ' .close', function() {
	$('tr').on().addClass('click');
	$(this).parents("#order-details").remove();
	
//	alert(this);
});

//кнопка В РАБОТУ
//$('tbody').on('click', '.inwork2', function() {
//$('.inwork1').click(function () {
//	id = $(this).parents("tr").data("key");
//	row= $(this).parents("tr");
//	$.ajax({
//			url: "index.php?r=admin/shop/order/inwork1",
//			//dataType: "json",
//			type: "GET",
//			data: {orderid: id},
//			success: function (data) {
//				//details = data; //массив с резулт. поиска
//				
//			if(data){
//				row.removeClass('success'); 
//				$(this).parents('div#p0').refresh;
//			}else{
//				alert('Заказ уже взят в обработку другим менеджером!');
//				$(this).parents(table).refresh;
//			}	
//				
//			
//
//			}			
//		});
//});


//изменение статуса
$('.status').change(function () {
	id = $(this).parents('tr').data("key");
	status =this.value;
	$('a.change_status').attr('href', 'index.php?r=admin/shop/order/change_status&orderid=' + id + '&status=' + status);
	$('a.change_status').click();
;	//index.php?r=admin/shop/order/inwork1&orderid='.$key.'
	//$(this).parents('tr').on()
});
});


