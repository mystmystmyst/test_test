 
ymaps.ready(init);

function init() {
	var geometry =[];
	//var properties = [];
	
	//задаем центр карты из координат объекта    
	geometry = jQuery.parseJSON($("#object-geometry").val());
	//properties = $("#object-properties").val();
	//options = jQuery.parseJSON($("#object-options").val());
	
	// Создаем карту.
    var myMap = new ymaps.Map("map", {
            center: geometry.coordinates, zoom: 18, type: 'yandex#satellite'            
        });

	// Создаем редактируемый объект.
    var editObject = new ymaps.GeoObject({            
            geometry:	geometry,
            //properties: properties
        },
		{"fillColor":"#ff0000","strokeWidth":0, editorDrawingCursor: "crosshair", draggable: true}
	);
	
	//включаем режим перетаскивания
	//editObject.options.set({draggable: [true]});
	
    // Добавляем редактируемый объект на карту.
    myMap.geoObjects.add(editObject);

	/*Включаем режим редактирования, если не круг.*/
 	if (geometry.type != 'Circle')  editObject.editor.startEditing();
    

}
