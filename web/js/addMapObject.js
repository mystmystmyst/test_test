 
ymaps.ready(init);

function init() {
var geometry = [];
var myMap;
var editObject;
var objectManager;
//var properties =[];
//var options = [];
var params=[];
//загрузка карты и окружающих объектов на карту
function onMap(center,zoom,type){
	//alert(center,zoom,type);
	var myMap = new ymaps.Map("map", {center: center, zoom: zoom, type: type});
	
	
	 $.ajax({		
 		url: "/web/index.php?r=objects/object/objects-json",
 		dataType: "json",
 		success: function(data) {
			//var geoQuery = ymaps.geoQuery(data);
			objectManager = new ymaps.ObjectManager();
			
			//k = geoQuery.search('properties.type == "Колодец"').setOptions(JSON.parse(data.config.defaultKolodec));
			//b = geoQuery.search('properties.type == "Здание"').setOptions(JSON.parse(data.config.defaultBuilding));
			console.log(data.options);
			//objectManager.objects.options.set({"fillcolor":"#FFFB73","strokeColor": "#FFFA40", "opacity": "0.5","strokeWidth": 1, "zIndex":0}); /*JSON.parse(data.options[2]["options"])*/
			objectManager.add(data);
			myMap.geoObjects.add(objectManager);
 		}
 	});
	
	
	
	
	
	
	//составляем геометрию объекта	
	switch ($("#object-object_type_id :selected").data("geometrytype")) {
		case 'Circle':
			geometry = {"type":"Circle","coordinates":myMap.getCenter(),"radius":1};break;				
		case 'Polygon':
			geometry = {"type":"Polygon","coordinates":[]};break;
		case 'LineString':
			geometry = {"type":"LineString","coordinates":[]};break;
		case 'Point':
			geometry = {"type":"Point","coordinates":[]};
	}
		
	// Удаляем все ранее добавленные объекты чтобы не путаться.
	//myMap.geoObjects.removeAll();	
			
	// Создаем редактируемый объект.
    editObject = new ymaps.GeoObject({            
            geometry:	geometry,
            //properties: {"hintContent": "Москва","balloonContent": "<h1>Радиус круга</h1> - <b>10 км</b>"} /*properties*/
        },
		$("#object-object_type_id :selected").data("options")
	);
	editObject.options.set({draggable:true});
	editObject.options.set({editorDrawingCursor:"crosshair"});
	
	// Добавляем редактируемый объект на карту.
    myMap.geoObjects.add(editObject);

	/*Включаем режим добавления, если не круг.*/
 	if ($("#object-object_type_id").val().indexOf('Колодец') == -1) editObject.editor.startDrawing();
}

// Транслит
function translit(w,v) {
   var tr='a b v g d e ["zh","j"] z i y k l m n o p r s t u f h c ch sh ["shh","shch"] ~ y ~ e yu ya ~ ["jo","e"]'.split(' ');
   var ww=''; w=w.toLowerCase();
   for(i=0; i<w.length; ++i) {
     cc=w.charCodeAt(i); ch=(cc>=1072?tr[cc-1072]:w[i]);
     if(ch.length<3) ww+=ch; else ww+=eval(ch)[v];
   }
   return(ww.replace(/[^a-zA-Z0-9\-]/g,'-').replace(/[-]{2,}/gim, '-').replace( /^\-+/g, '').replace( /\-+$/g, ''));
 }


	//выбираем Типа объекта
	$("#object-object_type_id").change(function(){
		//смотрим какую карту создать
		if($("#object-object_type_id :selected").data("level") ==0){
			 onMap(defaultCenter,13,defaultMapType); /*рисуем территорию и добавляем геообъекты*/
		//перезаписываем поле #object-on_map
		$("#object-on_map").empty();
		$("#object-on_map").append( $('<option>это поле заполнять не нужно</option>'));	
		}else{
			//разблокируем поле
			$("#object-on_map").attr('disabled', false);
		//делаем запрос где может быть размещен объект(на level-1)
				$.get( '/web/index.php?r=objects/object/ob1',{"test": $("#object-object_type_id :selected").data("level")-1}, myCallback, "json" );
			function myCallback( returnedData ) {
				// Делаем обработку данных returnedData (приходит невалидный JSON!!! Исправить)
				console.log(JSON.parse(returnedData[0]['geometry'])['coordinates'][0][0]);
				
				//очищаем все пункты добавленные ранее если они были
				$("#object-on_map").empty();
				$("#object-on_map").append( $('<option>Для продолжения выберите тип объекта..</option>')); 
				//добавляем options в select "расположение объекта"
				$.each(returnedData, function(){
					$("#object-on_map").append( $(
						'<option value="'+ this.name +'" data-center="'+ JSON.stringify(JSON.parse(returnedData[0]['geometry'])['coordinates'][0][0]) +'">' + this.name + '</option>')
					);
				});
			}
			//удаляем карту если она была создана предыдущим выбором #object-object_type_id
			if(typeof(myMap) != "undefined" && myMap !== null) {myMap.destroy();}
		}
		//генерируем название и алиас
		//$("#object-name").val($("#object-object_type_id :selected").text());
		//$("#object-alias").val($("#object-object_type_id :selected").data("alias"));
	});	
		
		
	//--------------------------------------------------	
	//выбираем объект, на карте которой будет виден редактируемый объект
	//и создаем его карту
	$("#object-on_map").change(function(){
		
		//создаем карту
		//console.log($.parseJSON($("#object-on_map :selected").data("center")));
		//alert($.parseJSON($("#object-on_map :selected").data("center")));
		//center1 = $.parseJSON($("#object-on_map :selected").data("center"));
		
		//загрузка объекта на карте (перенести в другое место)???
		//onMap($("#object-on_map :selected").data("center"), 13, defaultMapType);
	});
   
	
	//генерируем название при клике на поле name 
	$("#object-adr_s").keyup(function(){setTimeout(genName, 400);});
	$("#object-adr_h").keyup(function(){setTimeout(genName, 400);});
	$("#object-adr_b").keyup(function(){setTimeout(genName, 400);});
	$("#object-adr_r").keyup(function(){setTimeout(genName, 400);});
	$("#object-adr_f").keyup(function(){setTimeout(genName, 400);});
	
	
function genName(){
		$("#object-name").val(''); //сброс
		var name='', a, b, c, ob ={};
		ob=$("#object-object_type_id :selected").data("mask");
		console.log(ob);
		
		for(var key in ob) {
			console.log('key='+key);
			if (ob[key][1] ==false){
				a = "#object-" +ob[key][0];
				name = name + $(a).val();
				name = name.charAt(0).toUpperCase() + name.slice(1);
			}else{
// 				if((key+1) in ob) {b = "#object-" +ob[Number(key)+1][0];}
// 				if((key-1) in ob) {c = "#object-" +ob[key-1][0];}
			
				if($(b).val() !=='' && $(c).val() !=='')
				{name = name + ob[key][0];}
			}
		}
		
		$("#object-name").val(name);
 
 		$("#object-alias").val(translit($("#object-name").val(),0));
}



	
	//--------------------------------------------------
	//записываем координаты при клике по карте в JSON
	$("#map").click(function(){
		//tmp = '{"type":"' + Type + '","coordinates":' + JSON.stringify(GeoObject.geometry.getCoordinates()) +',"Radius":' + radius + '}';
		geometry.coordinates = editObject.geometry.getCoordinates();
		$("#object-geometry").val(JSON.stringify(geometry));
	return false;
	});



//--------------------------------------------------
// При выборе изображения подложки для карты объекта
$("#tiles").change(function(){
	var tiles = new FormData();
		tiles.append('name', 'tiles');
		//tiles.append('type', 'one');
		tiles.append('tiles', $('#tiles')[0].files[0]);
		tiles.append('mapname', $('#object-name').val());
		
//console.log($('#object-name').val());
		
		$("#map1").append('<span class="glyphicon glyphicon-cogwheels" style="font-size:3em"></span>');
//console.log(tiles);
	// Посылаем изображение на обработку в контроллер
	$.ajax({
		type: 'POST',
		url: '/web/index.php?r=objects/object/create-tiles',
		data: tiles,
		processData: false,
		contentType: false,
		dataType: "json",
	success: function(data) {
		//console.log(data);
		data = JSON.parse(data);
		$('#object-name').val(data.mapname);
		test(data);
	},
	error: function(data) {
		console.log(data);
	}
	});
});
	
function test(data){
//var imgmap = '775a.png';
	// Создаем декартову систему координат, на которую будет проектироваться карта.
         // Определяем границы области отображения в декартовых координатах.
         var myProjection = new ymaps.projection.Cartesian([
                 [-1, -1], // координаты левого нижнего угла
                 [1, 1]    // координаты правого верхнего угла
             ]),
             // Создадим собственный слой карты:
             Layer = function () {
                 return new ymaps.Layer(
                     // Зададим функцию, преобразующую номер тайла и уровень масштабировая
                     // в URL до тайла на нашем хостинге
                     function (tile, zoom) {
                         //$("#msgid").html("images/maps/"+tile[0] + "-" + tile[1] +imgmap);
                         return 'images/maps/' +data.mapname+ '/'+zoom+'-'+tile[1] + '-' + tile[0]+ '.jpg';
                         //return "images/maps/slice_" + tile[1] + "_" + tile[0] + ".png";
                     }
                 )
             };
 
         // Добавим конструктор слоя в хранилище слоёв под ключом my#blacksea
         ymaps.layer.storage.add('my#blacksea', Layer);
         // Создадим новый тип карты, состоящий только из нашего слоя тайлов,
         // и добавим его в хранилище типов карты под ключом my#blacksea
         ymaps.mapType.storage.add('my#blacksea', new ymaps.MapType(
             'План помещений',
             ['my#blacksea']
         ));
 
         // Создаем карту в заданной системе координат.
         var myMap1 = new ymaps.Map('map1', {
                 center: [0,0], zoom: 1,
                 // Указываем ключ нашего типа карты
                 type: 'my#blacksea',
				 controls: ["zoomControl", "fullscreenControl","rulerControl"]
             }, {
                 maxZoom: 3, minZoom: 1, 
                 projection: myProjection,
             });
 
             myMap1.controls.add(new ymaps.control.ZoomControl());
             //myMap1.controls.add('mapTools');
 
         var myPlacemark = new ymaps.GeoObject(
             {
                 geometry: {type: "Point",coordinates: [1, 2]},                    
                 properties: {iconContent: 'Системный блок'}
             },
             {
                 preset: 'twirl#greenStretchyIcon',
                 draggable: true
             }
         );
 
         myMap1.geoObjects.add(myPlacemark);
	
}
}
