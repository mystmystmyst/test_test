 
ymaps.ready(init);

function init() {
var geometry = [];
var myMap;
//var properties =[];
//var options = [];

// 	// Создаем карту c координатами центра по умолчанию.
//     $("#object-on_map").change(function(){
// 		var myMap = new ymaps.Map("map", {
//             center: defaultCenter, zoom: 10, type: defaultMapType /*берутся из контроллера */      
//         });
// 	});

	
		//определяем тип объекта при выборе пользоветелем
// 	if(typeof(myMap) != "undefined" && variable !== null) {
//     //проверка, выбрана ли карта пользователем
// 	}

	$("#object-object_type_id").change(function(){
		//смотрим какую карту создать
		if($("#object-object_type_id :selected").data("level") ==0){
			center = defaultCenter; MapType = defaultMapType;zoom = 10;
		}else{
			alert("делаем запрос на выборку карт объектов с уровнем level-1 для селекта и показываем их в селекте выбора карт");
		}
		//создаем карту
		var myMap = new ymaps.Map("map", {  /*территория*/
            center: center, zoom: zoom, type: MapType /*берутся из контроллера */      
        });
			
			
		console.log(defaultCenter,defaultMapType);
		//составляем геометрию объекта	
		switch ($("#object-object_type_id :selected").data("geometrytype")) {
			case 'Circle':
				var geometry = {"type":"Circle","coordinates":myMap.getCenter(),"radius":1};break;				
			case 'Polygon':
				var geometry = {"type":"Polygon","coordinates":[]};break;
			case 'LineString':
				var geometry = {"type":"LineString","coordinates":[]};break;
			case 'Point':
				var geometry = {"type":"Point","coordinates":[]};
		}
		
		
		// Удаляем все ранее добавленные объекты чтобы не путаться.
		myMap.geoObjects.removeAll();	
			
	// Создаем редактируемый объект.
    var editObject = new ymaps.GeoObject({            
            geometry:	geometry,
            //properties: {"hintContent": "Москва","balloonContent": "<h1>Радиус круга</h1> - <b>10 км</b>"} /*properties*/
        },
		$("#object-object_type_id :selected").data("options")
	);
	editObject.options.set({draggable:true});
	editObject.options.set({editorDrawingCursor:"crosshair"});
	
	// Добавляем редактируемый объект на карту.
    myMap.geoObjects.add(editObject);

	/*Включаем режим добавления, если не круг.*/
 	if ($("#object-object_type_id").val().indexOf('Колодец') == -1) editObject.editor.startDrawing();
								 
								 
								 
		//записываем координаты при клике по карте в JSON
		$("#map").click(function(){
			//tmp = '{"type":"' + Type + '","coordinates":' + JSON.stringify(GeoObject.geometry.getCoordinates()) +',"Radius":' + radius + '}';
			geometry.coordinates = editObject.geometry.getCoordinates();
			console.log(geometry);
			$("#object-geometry").val(JSON.stringify(geometry));
		return false;
		});
		return false;
	});
    

}
