/* 
 * Myst 
 * mystsys@gmail.com
 */
									
$(document).ready(function() {
//добавление товара в корзину
    $('.addtocart').on('click',  function (e) {
		var id = $(this).data('product_id');	
	    $.ajax({		
 		url: "index.php?r=shop/cart/add",
		type:"GET",
		data:{id:id},
			success: function (cart) {
				if (!cart) alert('Product ID error!'); //если вызван не существующий товар id
					// Выводим модальное окно корзины
					$('#modal .modal-body').html(cart);
					$('#modal').modal();
			},
			error: function(){
				alert('error!!!');
			}
 	});
	return false;
});

//очистка корзины
$('#clearcart').on('click',  function () {
	$.ajax({		
 		url: "index.php?r=shop/cart/clear",
		type:"GET",
			success: function (cart) {
				if (!cart) alert('Product ID error!'); //если вызван не существующий товар id
					// Выводим модальное окно корзины
					$('#modal .modal-body').html(cart);
					$('#modal').modal();
			},
			error: function(){
				alert('error!!!');
			}
 	});
	return false;
});

// Удаление товара из корзины
$('#modal').on('click', '#del-item', function () {
	var id=$(this).data('id');
	$.ajax({		
 		url: "index.php?r=shop/cart/del-item",
		type:"GET",
		data:{id: id},
			success: function (cart) {
				if (!cart) alert('Product ID error!'); //если вызван не существующий товар id
					// Выводим модальное окно корзины
					$('#modal .modal-body').html(cart);
					$('#modal').modal();
			},
			error: function(){
				alert('error!!!');
			}
 	});
	return false;
});

// открытие minicart по клику
$('#minicart').on('click', function () {
	$.ajax({		
 		url: "index.php?r=shop/cart/minicart",
		type:"GET",
			success: function (cart) {
				if (!cart) alert('Product ID error!'); //если вызван не существующий товар id
					// Выводим модальное окно корзины
					$('#modal .modal-body').html(cart);
					$('#modal').modal();
			},
			error: function(){
				alert('error!!!');
			}
 	});
	return false;
});

});



