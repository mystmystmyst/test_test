 
ymaps.ready(init);

function init() {


//var imgmap = '775a.png';
	// Создаем декартову систему координат, на которую будет проектироваться карта.
         // Определяем границы области отображения в декартовых координатах.
         var myProjection = new ymaps.projection.Cartesian([
                 [-500, -500], /* координаты левого нижнего угла в метрах*/
                 [500, 500]    /* координаты правого верхнего угла в метрах*/
             ]),
             // Создадим собственный слой карты:
             Layer = function () {
                 return new ymaps.Layer(
                     // Зададим функцию, преобразующую номер тайла и уровень масштабировая
                     // в URL до тайла на нашем хостинге
                     function (tile, zoom) {
                         //$("#msgid").html("images/maps/"+tile[0] + "-" + tile[1] +imgmap);
                         return 'images/maps/test/'+zoom+'-'+tile[1] + '-' + tile[0]+ '.jpg';
                         //return "images/maps/slice_" + tile[1] + "_" + tile[0] + ".png";
                     },{notFoundTile: 'images/maps/nulltile.jpg'}
                 )
             };
			
 
         // Добавим конструктор слоя в хранилище слоёв под ключом my#blacksea
         ymaps.layer.storage.add('my#blacksea', Layer);
         // Создадим новый тип карты, состоящий только из нашего слоя тайлов,
         // и добавим его в хранилище типов карты под ключом my#blacksea
         ymaps.mapType.storage.add('my#blacksea', new ymaps.MapType(
             'План помещений',
             ['my#blacksea']
         ));
 
         // Создаем карту в заданной системе координат.
         var myMap1 = new ymaps.Map('myMap1', {
                 center: [0,0], zoom: 1,
                 // Указываем ключ нашего типа карты
                 type: 'my#blacksea',
				 controls: ["zoomControl", "fullscreenControl","rulerControl"]
             }, {
                 maxZoom: 3, minZoom: 1, 
                 projection: myProjection
             });
 
             //myMap1.controls.add(new ymaps.control.ZoomControl());
             //myMap1.controls.add('mapTools');
 
         var myPlacemark = new ymaps.GeoObject(
             {
                 geometry: {type: "Point",coordinates: [0, 0]},                    
                 properties: {iconContent: 'Системный блок'}
             },
             {
                 preset: 'twirl#greenStretchyIcon',
                 draggable: true
             }
         );
 
         myMap1.geoObjects.add(myPlacemark);
	

}
