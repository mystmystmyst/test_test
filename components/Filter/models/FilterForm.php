<?php

namespace app\components\Filter\models;

use Yii;
use yii\base\Model;
use app\modules\pages\models\Widget;
	
/**
 * форма поиска
 * $params - параметры виджета
 */
class FilterForm extends Model
{

    public $manuf;
    public $marka;
    public $category;
// for(i=1,i=10,i++){
//	  public $pooo.$i;$pooo1=333;
//	  echo $pooo1;
// }


    /*
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }
        return false;
    }
}
