<?php

/*
  Виджет фильтра по параметрам
 */

namespace app\components\Filter;

use yii\base\Widget;
//use yii\bootstrap\ActiveForm;
//use yii\captcha\Captcha;
use yii\helpers\Html;
//use app\components\Filter\models\FilterForm;
//use yii\web\View;
use yii\web\JqueryAsset;

class Filter extends Widget {

	//переменные приходят извне
	public $params;
	public $widget_id;
	//для внутр исп
	public $out;
	public $fields;

	public function init() {
		$this->view->registerJsFile('@web/js/shop/filter.js', ['depends' => JqueryAsset::className()]);

		parent::init();
		$this->params = json_decode($this->params, true);

		//вытаскиваем параметры параметров
		$this->fields = \app\modules\shop\models\Param::find()
				//->where(['param_id'=>$this->params])
				->andwhere(['cat_id' => 109])
				->andwhere(['>', 'filter_order', 0])
				->andwhere(['level' => 'popular'])
				->asArray()
				->all();
	}

	public function run() {

// Содержимое формы
		echo '<form id = "filter-' . $this->widget_id . '">';
//Html::beginForm(['order/update', 'id' => $id], 'post', ['enctype' => 'multipart/form-data']);
		//строим инпуты формы
		foreach ($this->fields as $f) {
//dump($f);	
			echo '<div class = "form-group">
				<label for = "' . $f['alias'] . '">' . $f['name'] . '</label>';
			switch ($f['filter_type']) {
				case 'text':
					echo Html::input('text', $f['name'], '', [
						'id' => $f['alias'],
						'class' => 'form-control',
						'data-param_id' => $f['param_id'], 'data-parent' => $f['parent'], 'data-name' => $f['name'],
						'placeholder' => $f['name']]
					); break;
				case 'select':
					$param_variants = \app\modules\shop\models\Variant::find()
									->where(['param_id' => $f['param_id']])
									->asarray()->all();
					$items = \yii\helpers\ArrayHelper::map($param_variants, 'variant_id', 'value');
					echo Html::dropDownList('text', null, $items, [
						'class' => 'form-control  input-sm',
					]);
					break;
				case 'radio':
					$param_variants = \app\modules\shop\models\Variant::find()
									->where(['param_id' => $f['param_id']])
									->asarray()->all();
					$items = \yii\helpers\ArrayHelper::map($param_variants, 'variant_id', 'value');
					//dump($items);
					echo Html::radioList('ddd', null, $items, [
						'class' => 'radio',
					]);
					break;
				case 'checkbox':
					$param_variants = \app\modules\shop\models\Variant::find()
									->where(['param_id' => $f['param_id']])
									->asarray()->all();
					$items = \yii\helpers\ArrayHelper::map($param_variants, 'variant_id', 'value');
					//dump($items);
					echo Html::checkboxList('ddd', null, $items, [
						'class' => 'checkbox',
					]);
					break;
			}
			echo '</div>';
//echo $form->field(new FilterForm(), 'manuf')->textInput(['data-param_id' =>$f['param_id'],'data-parent'=> $f['parent'],'data-name'=>$f['name'],'placeholder'=>$f['name']])->label($f['name']);
		}

		echo '</form>';
		echo Html::submitButton('Очистить фильтр', ['class' => 'btn btn-sm btn-primary']);



		//return $this->out;
	}

}
