<?php

/*
  Виджет фильтра по параметрам
 */

namespace app\components\Filter;

use yii\base\Widget;
use yii\bootstrap\ActiveForm;
//use yii\captcha\Captcha;
use yii\helpers\Html;
use app\components\Filter\models\FilterForm;
//use yii\web\View;
use yii\web\JqueryAsset;

class Filter extends Widget {

	//переменные приходят извне
	public $params;
	public $widget_id;
	//для внутр исп
	public $out;
	public $fields;

	public function init() {
		$this->view->registerJsFile('@web/js/filter.js',['depends' => JqueryAsset::className()]);

		parent::init();
		$this->params = json_decode($this->params, true);
		
		//вытаскиваем параметры параметров
		$this->fields= \app\modules\pages\models\Param::find()
				->where(['param_id'=>$this->params])
				->asArray()
				->all();
		//dump($this->fields);exit;		
	}

	public function run() {
		$form = ActiveForm::begin([
					'id' => 'filter-'.$this->widget_id,
					'options' => [
						//'class' => '',
						//'enctype' => 'multipart/form-data'
					],
		]);
// Содержимое формы
		//строим инпуты формы
		foreach ($this->fields as $f) {
			dump($f);//exit;
					echo $form->field(new FilterForm(), 'manuf')->textInput(['data-param_id' =>$f['param_id'],'data-parent'=> $f['parent'],'data-name'=>$f['name'],'placeholder'=>$f['name']])->label($f['name']);
		}
//			echo $form->field(new FilterForm(), 'manuf')->textInput(['data-param_id' =>'18','data-parent'=>'','data-name'=>'Производитель','placeholder'=>'Производитель'])->label('Производитель');
//			echo $form->field(new FilterForm(), 'marka')->textInput(['data-param_id' =>'18','data-parent'=>'18','data-name'=>'Марка','placeholder'=>'Марка','disabled'=>'disabled'])->label('Марка');
//			echo $form->field(new FilterForm(), 'category')->textInput(['data-param_id' =>'18','data-parent'=>'','data-name'=>'Категория','placeholder'=>'Категория'])->label('Категория');

		
		echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);


		ActiveForm::end();
		//return $this->out;
	}

}
