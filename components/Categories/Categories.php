<?php

/*
  Виджет вывода логотипа
 */

namespace app\components\Categories;

use yii\base\Widget;

class Categories extends Widget {
	
	public $params;
	public $widget_id;
	public $cats; //список категорий
	public $template; //название view
	
	public function init() {				
		parent::init();
		$this->cats = \app\modules\shop\models\Category::find()
				->where(['type'=>1]) //'type',1 = товары
				->andWhere(['published'=>'1'])
				->andWhere(['!=', 'parentcat', 0])
				->asarray()
				->all();
		$this->params = json_decode($this->params, true);//вытаскиваем параметры виджета
//dump($this->params);
	}

	public function run() {
		switch ($this->template) {
			case 'menu':
				$this->template='menu';
				break;
			case 'img':
				$this->template='img';
				break;
			default:	$this->template='menu';
		}
	
		return $this->render($this->template, [
				'cats' => $this->cats,
			]);		 
	}
}
