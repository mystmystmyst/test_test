<?php

use \yii\helpers\Url;

/*
 * Myst 
 * mystsys@gmail.com
 */
//dump('ttt');
?>
<div class="widget-categories">
	<?php
	$out = '';
	foreach ($cats as $cat) {
		?>
		<div class="col-sm-6">
		
				<div class="spacer" href="<?= Url::to(['category/view', 'id' => $cat['cat_id']]) ?>">
					<h4><?= $cat['name'] ?></h4>
					<div class="c1">
						<a class="btn" href="<?= Url::to(['category/view', 'id' => $cat['cat_id']]) ?>"><i class="glyphicon glyphicon-equalizer"></i>   Смотреть все</a>
					</div>
					<div class="c2">
						<?php
						if ($cat['file_name']) {
							echo '<img src="' . Url::to('@web/images/shop/categories/' . $cat['file_name']) . '" alt="' . $cat['name'] . '" />';
						} else {
							echo '<img src="' . Url::to('@web/images/shop/noimage.png') . '" alt="" />';
						}
						?>
					</div>
				</div>
		
		</div>
	<?php
	}
	unset($cat);
	?>
</div>