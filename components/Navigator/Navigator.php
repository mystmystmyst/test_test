<?php

/*
  Виджет навигатора
 */

namespace app\components\Navigator;

use yii\base\Widget;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

class Navigator extends Widget {

	public $out;
	public $params;
	public $widget_id;

	public function init() {
		parent::init();
		$this->params = json_decode($this->params, true);
		$this->out = '';
	}

	public function run() {
		NavBar::begin([
			'options' => [
				//'class' => 'navbar-inverse',
			],
		]);
		echo Nav::widget([
			'options' => ['class' => 'navbar-nav'],
			'items' => [
				['label' => 'Телевизоры', 'url' => ['/shop/category/view','id'=>109]],
				['label' => 'Пылесосы', 'url' => ['/shop/category/view','id'=>110]],
				['label' => 'Кондиционеры', 'url' => ['/shop/category/view','id'=>113]],	
				['label' => 'Музыкальные центры', 'url' => ['/shop/category/view','id'=>115]],
				['label' => 'Усилители/Ресиверы', 'url' => ['/shop/category/view','id'=>116]],
						],
				
		]);
		NavBar::end();



		return $this->out;
	}

}
