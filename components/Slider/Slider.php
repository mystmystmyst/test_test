<?php
/*
  Виджет Slider
 */

namespace app\components\Slider;

use yii\base\Widget;

class Slider extends Widget {

	public $out;
	public $params;
	public $widget_id;

	public function init() {
		parent::init();

		$this->params = json_decode($this->params, true);//вытаскиваем параметры виджета
		
	}

	public function run() {

		return $this->render('bootstrap1', [
					'slides' => $this->params,
		]);
	}

}
