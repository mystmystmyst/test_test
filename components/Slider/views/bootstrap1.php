<?php

use \yii\helpers\Url;

/*
 * Myst 
 * mystsys@gmail.com
 */

?>
<div class="widget-slider">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
<?php			$active = ' class="active"';
				foreach ($slides as $key => $slide) {?>
					<li data-target="#carousel-example-generic" data-slide-to="<?=$key?>" <?=$active?>></li>
<?php			$active ='';
				}
				unset($slide);
				?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				
<?php			$active = 'active';
				foreach ($slides as $key => $slide) {?>
					<div class="item item<?=$key?> <?= $active?>">
						<img src="<?= Url::to('@web/images/slider/'.$slide['img'].'')?>" alt="<?= $slide['title']?>">
						<div class="carousel-caption">
							<?=$slide['desc']?>
						</div>
					</div>
<?php			$active ='';
				}
				unset($slide);
				?>				

			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
</div>