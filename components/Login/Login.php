<?php

/*
  Виджет фильтра по параметрам
 */

namespace app\components\Login;

use yii\base\Widget;
use Yii;
use yii\helpers\Url;

class Login extends Widget {
	public $out;
	public $params;
	public $widget_id;
	public function init() {		
		parent::init();
		//$this->params = json_decode($this->params, true);
		if(Yii::$app->user->identity)  {
			$this->out='<a href="'.Url::to(['/users/default/logout']).'" class="logout"><i class="glyphicon glyphicon-off"></i>  '
					.Yii::$app->user->identity['username'].
					'</a>';
			$this->out .='<a href="'.Url::to(['/users/default/logout']).'" class="client"><i class="glyphicon glyphicon-off"></i>  '
					.'Личный кабинет'.
					'</a>';
		}else{
			$this->out='<a href="'.Url::to(['/users/default/login']).'" class="login"><i class="glyphicon glyphicon-user"></i> Вход</a>';
			$this->out .= '<a href="'.Url::to(['/users/default/signup']).'" class="registration"><i class="glyphicon glyphicon-pencil"></i> Регистрация</a>';;
		}

	}

	public function run() {
		return $this->out;
	}
}
