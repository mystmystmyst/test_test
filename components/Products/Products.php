<?php

/*
  Виджет вывода Products
 */

namespace app\components\Products;

use yii\base\Widget;

class Products extends Widget {
	public $out;
	public $params;
	public $widget_id;
	public function init() {				
		parent::init();
		$this->params = json_decode($this->params, true);//вытаскиваем параметры виджета
		
		$this->params['view']='featured';
		switch ($this->params['view']){
			case 'featured': $this->params['where']=['featured'=>'1'];
		}
		$this->out = \app\modules\shop\models\Product::find()
					->where($this->params['where'])->andwhere(['published'=>'1'])
					->asArray()->limit(5)
					//->JoinWith('value.param', true, 'LEFT JOIN')
					->with(['value.param'])
					->all();
//				\app\modules\shop\models\Product::find()
//				->where($this->params['where'])
//				->andwhere(['published'=>'1'])
//				->joinwith(['value.param' => function ($query) {
//							$query->indexby('param_id');
//						}])
//				->asarray()
//				->limit(5)
//				->all();
				

	}

	public function run() {
		return $this->render('view1', [
					'products' => $this->out,
		]);
	}
}
