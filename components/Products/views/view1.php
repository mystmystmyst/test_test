<?php

use \yii\helpers\Url;
use \app\modules\shop\helpers\CookHtml;

/*
 * Myst 
 * mystsys@gmail.com
 */
?>
<div class="widget-products">
<div class="clearfix"></div>
<h3 class="title">Популярные товары</h3>

	
		<?php 
		//dump($products);
		foreach ($products as $product) {?>
		
		<div class="item col-sm-3">
			<div class="spacer">
				<?php if($product['featured'] == '1') echo '<div class="label featured">Popular!</div>';?>
				<?= CookHtml::getParam($product, 'main_img'); //main img?>
				<div class="clearfix"></div>
				<?= CookHtml::getParam($product, 'Заголовок'); ?>
				<?= CookHtml::getParam($product, 'Рейтинг'); ?>
				<?= CookHtml::getParam($product, 'Цена:'); ?>
			</div>
		</div>	
		
	<?php	}?>
	</div>
<div class="clearfix"></div>