<?php

/*
  Виджет корзины магазина
 */

namespace app\components\Minicart;

use yii\base\Widget;
use yii;

class Minicart extends Widget {
	public $out;
	public $params;
	public $widget_id;
	public function init() {		
		//parent::init();
	}

	public function run() {
			echo '<div id="minicart"><div class="wishlist">'
					. '<i class="wishlist glyphicon glyphicon-heart-empty"  data-toggle="tooltip" data-placement="left" title="Мои отложенные товары"></i>'
					. '<i class="comparison glyphicon glyphicon-random"  data-toggle="tooltip" data-placement="left" title="Сравнение товаров"></i>'
					. '</div>';
		$session =Yii::$app->session;
			$session->open(); 
		if(empty($session['qty'])){
			echo '<div  class="minicart-empty"><i class="glyphicon glyphicon-shopping-cart"></i></div>';
		}else{
			echo 	'<div class="minicart-full"><i class="glyphicon glyphicon-shopping-cart"></i>'
					. '<div class="minicart-qty label label-primary">'.$session['qty'].'</div>'
				. '</div>';
		}
		echo '</div>';
	}
}
