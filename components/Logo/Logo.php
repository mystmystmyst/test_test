<?php

/*
  Виджет вывода логотипа
 */

namespace app\components\Logo;

use yii\base\Widget;

class Logo extends Widget {
	public $out;
	public $params;
	public $widget_id;
	public function init() {	
				
		parent::init();
		$this->params = json_decode($this->params, true);//dump($this->params);exit;
		$this->out=
				
			'<a class="logo" href="'.  \yii\helpers\Url::home().'">'
				. '<img src="public_html/'.$this->params['img'].'" alt="'.$this->params['alt'].'" class="logo-img"/>'
				. '<div class="slogan">'.$this->params['slogan']
				. '</div>'
			. '</a>';
	}

	public function run() {
		return $this->out;
	}
}
