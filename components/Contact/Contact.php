<?php
/*
  Виджет формы обратной связи
 */

namespace app\components\Contact;

use yii;
use yii\base\Widget;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;
use app\components\Contact\models\ContactForm;

//use app\components\Filter\models\FilterForm;
//use yii\web\View;
//use yii\web\JqueryAsset;

class Contact extends Widget {

	//переменные приходят извне
	public $params;
	public $widget_id;

	//для внутр исп
	//public $out;
	//public $fields;

	public function init() {
		$model = new ContactForm();
		//$this->view->registerJsFile('@web/js/filter.js',['depends' => JqueryAsset::className()]);
		//parent::init();
		//$this->params = json_decode($this->params, true);
//		//вытаскиваем параметры параметров
//		$this->fields= \app\modules\pages\models\Param::find()
//				->where(['param_id'=>$this->params])
//				->asArray()
//				->all();
		//dump($this->params);exit;		
	}

	public function run() {

		$model = new ContactForm();
				if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
					Yii::$app->session->setFlash('contactFormSubmitted');
					//$model->refresh();
					//$this->refresh();
		}
		?>

		<div class="site-contact">
			<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

				<div class="alert alert-success text-center">
					Сообщение отправлено.<br/> Мы вяжемся с Вами в ближайшее время. <br/>Спасибо.
				</div>


			<?php else: ?>

				<p class="small">
					Не нашли что хотели? Спросите у нас! Мы всегда сможем предложить Вам какие либо варианты! Напишите нам, и мы свяжемся с Вами в самые короткие сроки.
				</p>

					<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

					<?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

					<?= $form->field($model, 'email') ?>

					<?= $form->field($model, 'subject') ?>

					<?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

					<?php
					//echo $form->field($model, 'verifyCode')->widget(Captcha::className(), [
					//	'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
					//])
					?>

					<div class="form-group">
						<?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
					</div>

					<?php ActiveForm::end(); ?>


			<?php endif; ?>
		</div>
		<?php 

	}

}
