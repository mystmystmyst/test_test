<?php

namespace app\components\Contact\models;

use Yii;
use yii\base\Model;

//use app\modules\pages\models\Widget;

/**
 * форма поиска
 * $params - параметры виджета
 */
class ContactForm extends Model {

	public function init() {
		parent::init();
		//echo'1111';
	}

	public $name;
	public $email;
	public $phone;
	public $subject;
	public $body;
	public $verifyCode;

	/**
	 * @return array the validation rules.
	 */
	public function rules() {
		return [
			// name, email, subject and body are required
			[['name', 'subject', 'body'], 'required'],
			// email has to be a valid email address
			['email', 'email'],
			['phone', 'number'],
				// verifyCode needs to be entered correctly
				//['verifyCode', 'captcha'],
		];
	}

	/**
	 * @return array customized attribute labels
	 */
	public function attributeLabels() {
		return [
				// 'verifyCode' => 'Verification Code',
		];
	}

	/**
	 * Sends an email to the specified email address using the information collected by this model.
	 * @param string $email the target email address
	 * @return boolean whether the model passes validation
	 */
	public function contact($email) {//dump($this);exit;
//	echo 'setTo='.$email.'<br>';
//	echo 'setFrom='.$this->email .'<br>';
//	echo 'setSubject='.$this->subject.'<br>';
//	echo 'setTextBody='.$this->body.'<br>';
		if ($this->validate()) {
			Yii::$app->mailer->compose()
					->setTo($email)
					->setFrom([$this->email => $this->name])
					->setSubject($this->subject)
					->setTextBody($this->body)
					->send();
			return true;
		}

		return false;
	}

}
