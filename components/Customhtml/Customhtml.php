<?php

/*
  Виджет фильтра по параметрам
 */

namespace app\components\Customhtml;

use yii\base\Widget;

class Customhtml extends Widget {
	public $out;
	public $params;
	public $widget_id;
	public function init() {		
		parent::init();
		//$this->params = json_decode($this->params, true);
		$this->out=
			$this->params;

	}

	public function run() {
		return $this->out;
	}
}
