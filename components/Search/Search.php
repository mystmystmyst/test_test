<?php

/*
  Виджет Ajax поиска
 */

namespace app\components\Search;

use yii\base\Widget;
use yii\web\JqueryAsset;

class Search extends Widget {

	public $opt;
	public $out;
	public $params;
	public $widget_id;

	public function init() {
		$this->view->registerJsFile('@web/js/shop/search.js',['depends' => JqueryAsset::className()]);
		parent::init();
		$this->params = json_decode($this->params, true);
		$this->out = $this->params;
		$cs=  \app\modules\shop\models\Category::find()->where(['type'=>1])->all(); //создаем список категорий
		foreach ($cs as $c) {
			$this->opt .='<li data-id="'.$c['cat_id'].'"><a href="#">'.$c['name'].'</a></li>';
		}
	}

	public function run() {
		$this->out =
'<div class="input-group">
      <input id="search" type="text" class="form-control" aria-label="..." placeholder="Введите текст для поиска">
      <div class="input-group-btn">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">В категории <span class="caret"></span></button>
        <ul class="dropdown-menu dropdown-menu-right">'.
          $this->opt
        .'</ul>
      </div>
    </div>
<div id="search-result" class="hidden"></div>
';
		return $this->out;
	}

}
