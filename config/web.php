<?php
use kartik\mpdf\Pdf;
$params = require(__DIR__ . '/params.php');

$config = [
	'id' => 'basic',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'layoutPath' => '@app/modules/pages/views/layouts',
	'defaultRoute' => 'shop/category/home', //pages/page/mainpage
	'language' => 'ru',
	
	
	'modules' => [
		'admin' => ['class' => 'app\modules\admin\Admin',
			'layout' => '@app/modules/admin/views/layouts/main',
			'modules' => [
				'pages' => [
					'class' => 'app\modules\pages\Pages',
					'controllerNamespace' => 'app\modules\pages\controllers\admin',
					'viewPath' => '@app/modules/pages/views/admin',
				],
				'shop' => [
					'class' => 'app\modules\shop\Shop',
					'controllerNamespace' => 'app\modules\shop\controllers\admin',
					'viewPath' => '@app/modules/shop/views/admin',
				],
				'biz' => [
					'class' => 'app\modules\biz\Biz',
					'controllerNamespace' => 'app\modules\biz\controllers\admin',
					'viewPath' => '@app/modules/biz/views/admin',
				],
				'users' => [
					'class' => 'app\modules\users\Users',
					'controllerNamespace' => 'app\modules\users\controllers\admin',
					'viewPath' => '@app/modules/users/views/admin',
				],
				'impexp' => [
					'class' => 'app\modules\impexp\Impexp',
					'controllerNamespace' => 'app\modules\impexp\controllers\admin',
					'viewPath' => '@app/modules/impexp/views/admin',
				],
			],
		],
		'pages' => [
			'class' => 'app\modules\pages\Pages',
		//layout' => '@app/modules/pages/views/layouts/admin',
		//'controllerNamespace' => 'app\modules\pages\controllers',
		//'viewPath' => '@app/modules/pages/views/admin',
		],
		'shop' => ['class' => 'app\modules\shop\Shop'],
		'work' => ['class' => 'app\modules\work\Work'],
		'tickets' => ['class' => 'app\modules\tickets\Tickets'],
		'users' => ['class' => 'app\modules\users\Users',
//					'controllerNamespace' => 'app\modules\users\controllers',
//					'viewPath' => '@app/modules/users/views',
				],
	//'biz' =>['class' =>'app\modules\biz\Biz'],
	'objects' => ['class' => 'app\modules\objects\Objects',
		'layout' => '@app/modules/objects/views/layouts/main',
		],
		
	],
	'components' => [
		//значение для пустых строк в таблицах
		'formatter' => [
			'class' => 'yii\i18n\Formatter',
			'nullDisplay' => '',
		],
		// setup Krajee Pdf component
		'pdf' => [
			'class' => Pdf::classname(),
			'format' => Pdf::FORMAT_A4,
			'orientation' => Pdf::ORIENT_PORTRAIT,
			'destination' => Pdf::DEST_BROWSER,
			'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
		// refer settings section for all configuration options
		],
		'assetManager' => [
			//'linkAssets' => true, //подключение рессурсных файлов ссылкой на них
			'converter' => [
				'class' => 'nizsheanez\assetConverter\Converter',
				'destinationDir' => 'css', //at which folder of @webroot put compiled files
				'parsers' => [
					'less' => [ // file extension to parse
						'class' => 'nizsheanez\assetConverter\Less',
						'output' => 'css', // parsed output file type
						'options' => [
							'auto' => true, // optional options
						]
					]
				]
			],
//			'bundles' => [
//				'yii\bootstrap\BootstrapAsset' => [
//					'css' => [],
//				],
//			],
		],
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'IC2QDAnujW-Tnjn-YSoxvz4Xi0gjqFRa_1',
		//'baseUrl' => '',
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
			'defaultDuration'=>60,
		],
		'user' => [
			'identityClass' => 'app\modules\users\models\User',
			'enableAutoLogin' => true,
			'loginUrl' => ['users/default/login'],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => true,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'localhost',
				'username' => 'myst',
				'password' => 'ssss123',
				'port' => '587',
				'encryption' => 'tls',
			],
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'db' => require(__DIR__ . '/db.php'),
//         'urlManager' => [
// 		'enablePrettyUrl' => true,
// 		'showScriptName' => false,
// 		'enableStrictParsing' => false,
// 		'rules' =>[
//			//'shop/' => 'shop/category/',
//// 			['pattern' => '/', 'route' => 'pages/page/mainpage'],
//// 			['pattern' => 'site/<action>', 'route' => 'site/<action>'],
//// 			['pattern' => '<cat_url>/<page_name>', 'route' => 'pages/default/view'],
//// 			['pattern' => '<cat_url>', 'route' => 'pages/default/category'],
// 		],
//         ],
	],
	'params' => $params,
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
		'allowedIPs' => [ '127.0.0.1','*'],
	];
	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		'class' => 'yii\gii\Module', 'allowedIPs' => ['127.0.0.1'] // adjust this to your needs
	];
}

return $config;
