<?php

/*
 * [key]=>['value','label','desc','help_link']
 */
return [
	'adminEmail' => 'mystsys@gmail.com',
	'siteEmail' => 'mystsys@gmail.com',
	'siteName' => '',
	'shortSiteName' => '',
	'user.passwordResetTokenExpire' => 3600,	//время жизни токена сброса пароля 
	
	'shop' => [
		'shopEmail' => ['mystsys@gmail.com','описание'],
		'company' => '',
		'ogrn' => '',
		'uraddr' =>'',
		'sms' => 1,						//отправлять смс при заказе? 0 1
		'tel' => '',			//телефон для смс информирования о заказах
		'cost' => '490',
		'days' => '1',
	
		'template' => [
			'Название/Цена/Экспорт в YM' => '{"29":"Название","5":"Цена","24":"Экспорт в YM"}',
			'Название/Цена' =>'{"29":"Название", "5":"Цена"}',
			'Картинка/Название/Цена' =>'{"3":"image","29":"Название","5":"Цена"}'
		]
		
	],
    'admin.product.search.template' => [
        0 => [ //template 1
            2 => 'Desc'
        ],
        1 => [ //template 2
            5 => 'Price'
        ],
        2 => [ //template 3
            2 => 'Desc',
            5 => 'Price'
        ],
    ],

];
