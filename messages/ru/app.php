<?php

/* 
 * Myst 
 * mystsys@gmail.com
 */

return [
	'Search' => 'Найти',
	'Reset' => 'Сброс',
	'Delete'=>'Удалить',
	'Create'=>'Создать',
	'Description' => 'Описание',
	'Published' => 'Опубликован',
	'Price' => 'Цена',
	'Export' => 'Экспорт в Ym'	
];