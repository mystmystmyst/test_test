Аудиотехника
Array
(
    [Categories] => Array
        (
            [ResultsTotal] => 29
            [ResultsPerPage] => 29
            [Page] => 1
            [Listing] => Array
                (
                    [0] => Array
                        (
                            [Id] => 97
                            [ParentId] => 78
                            [Name] => Наушники
                            [Nick] => headphones
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 2932
                        )

                    [1] => Array
                        (
                            [Id] => 85
                            [ParentId] => 78
                            [Name] => MP3-плееры
                            [Nick] => mp3players
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 166
                        )

                    [2] => Array
                        (
                            [Id] => 79
                            [ParentId] => 78
                            [Name] => Музыкальные центры
                            [Nick] => minisystems
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 122
                        )

                    [3] => Array
                        (
                            [Id] => 86
                            [ParentId] => 78
                            [Name] => Диктофоны, аксессуары
                            [Nick] => diktofony-aksessuary
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 477
                        )

                    [4] => Array
                        (
                            [Id] => 80
                            [ParentId] => 78
                            [Name] => Магнитолы
                            [Nick] => boomboxes
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 61
                        )

                    [5] => Array
                        (
                            [Id] => 87
                            [ParentId] => 78
                            [Name] => Акустические системы
                            [Nick] => speakers
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 4927
                        )

                    [6] => Array
                        (
                            [Id] => 2568
                            [ParentId] => 78
                            [Name] => Комплекты акустики
                            [Nick] => komplekty-akustiki
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 1180
                        )

                    [7] => Array
                        (
                            [Id] => 82
                            [ParentId] => 78
                            [Name] => Портативные CD-плееры
                            [Nick] => portativnye-cd-pleery
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 1
                        )

                    [8] => Array
                        (
                            [Id] => 1977
                            [ParentId] => 78
                            [Name] => Сабвуферы
                            [Nick] => subwoofers
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 1001
                        )

                    [9] => Array
                        (
                            [Id] => 91
                            [ParentId] => 78
                            [Name] => Усилители и ресиверы
                            [Nick] => amplifiers
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 1841
                        )

                    [10] => Array
                        (
                            [Id] => 1070
                            [ParentId] => 78
                            [Name] => Электронные переводчики
                            [Nick] => translators
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 3
                        )

                    [11] => Array
                        (
                            [Id] => 1974
                            [ParentId] => 78
                            [Name] => AV-процессоры
                            [Nick] => av-processory
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 100
                        )

                    [12] => Array
                        (
                            [Id] => 1973
                            [ParentId] => 78
                            [Name] => CD-проигрыватели
                            [Nick] => cdplayers
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 346
                        )

                    [13] => Array
                        (
                            [Id] => 92
                            [ParentId] => 78
                            [Name] => Портативные колонки
                            [Nick] => audio-doc-stations
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 626
                        )

                    [14] => Array
                        (
                            [Id] => 81
                            [ParentId] => 78
                            [Name] => Радиоприемники
                            [Nick] => radiopriemniki
                            [ChildrenTotal] => 0
                            [Type] => parameterized
                            [ModelsTotal] => 975
                        )

                    [15] => Array
                        (
                            [Id] => 93
                            [ParentId] => 78
                            [Name] => Тюнеры
                            [Nick] => tyunery
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 72
                        )

                    [16] => Array
                        (
                            [Id] => 1976
                            [ParentId] => 78
                            [Name] => Виниловые проигрыватели
                            [Nick] => vinilovye-proigryvateli
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 1586
                        )

                    [17] => Array
                        (
                            [Id] => 96
                            [ParentId] => 78
                            [Name] => Аксессуары
                            [Nick] => aksessuary-96
                            [ChildrenTotal] => 4
                        )

                    [18] => Array
                        (
                            [Id] => 94
                            [ParentId] => 78
                            [Name] => Эквалайзеры
                            [Nick] => ekvalajzery
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 21
                        )

                    [19] => Array
                        (
                            [Id] => 1860
                            [ParentId] => 78
                            [Name] => Ретро аудиотехника
                            [Nick] => retro-audio-tehnika
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 184
                        )

                    [20] => Array
                        (
                            [Id] => 4421
                            [ParentId] => 78
                            [Name] => Мегафоны
                            [Nick] => megafony
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 87
                        )

                    [21] => Array
                        (
                            [Id] => 4592
                            [ParentId] => 78
                            [Name] => Тонармы
                            [Nick] => ekvalajzery-4592
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 112
                        )

                    [22] => Array
                        (
                            [Id] => 4596
                            [ParentId] => 78
                            [Name] => Головки звукоснимателя
                            [Nick] => golovki-zvukosnimatelya
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 71
                        )

                    [23] => Array
                        (
                            [Id] => 4676
                            [ParentId] => 78
                            [Name] => Цифро-аналоговые преобразователи
                            [Nick] => cifro-analogovye-preobrazovateli
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 282
                        )

                    [24] => Array
                        (
                            [Id] => 4800
                            [ParentId] => 78
                            [Name] => Караоке
                            [Nick] => karaoke
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 43
                        )

                    [25] => Array
                        (
                            [Id] => 4848
                            [ParentId] => 78
                            [Name] => Мультирум системы
                            [Nick] => multirum-sistemy
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 730
                        )

                    [26] => Array
                        (
                            [Id] => 4849
                            [ParentId] => 78
                            [Name] => Сетевые проигрыватели
                            [Nick] => setevye-proigryateli
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 164
                        )

                    [27] => Array
                        (
                            [Id] => 4979
                            [ParentId] => 78
                            [Name] => Микшеры
                            [Nick] => mikshery
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 934
                        )

                    [28] => Array
                        (
                            [Id] => 1124
                            [ParentId] => 78
                            [Name] => Прочее
                            [Nick] => prochee-1124
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 2328
                        )

                )

        )

)

{
"options":[
{"object_type_id":"1","options":"{\"fillcolor\":\"#FFFF73\",\"strokeColor\": \"#FFFF40\", \"opacity\": \"0.5\",\"strokeWidth\": 1,\"zIndex\":1}"},
{"object_type_id":"2","options":"{\"fillOpacity\":0,\"strokeColor\": \"#0000FF\", \"opacity\": \"0.5\",\"strokeWidth\": 5, \"strokeStyle\": \"shortdash\",\"zIndex\":0,\"showHintOnHover\":false}"},
{"object_type_id":"3","options":"1"},{"object_type_id":"20","options":""},
{"object_type_id":"21","options":""},
{"object_type_id":"22","options":""},
{"object_type_id":"23","options":""},
{"object_type_id":"24","options":""},
{"object_type_id":"25","options":""}],"type":"FeatureCollection","features":[{"type":"Feature","id":0,"geometry":{"type":"Polygon","coordinates":[[[58.16343353489,52.653785066193],[58.151637860892,52.664428071564],[58.15020850305,52.658591584747],[58.149573214404,52.65902073819],[58.149255565813,52.657432870453],[58.148506811448,52.658119515961],[58.148234533214,52.656746224945],[58.147780731512,52.654643373077],[58.14916480855,52.653312997406],[58.148711018752,52.651167230194],[58.151705924113,52.640352563446],[58.153294028871,52.636876420563],[58.155800820906,52.624023274963],[58.156719555503,52.624667005127],[58.159520981911,52.622413949555],[58.160666440682,52.627520875519],[58.16347889529,52.625246362274],[58.165070200812,52.632537330734],[58.162647175168,52.635365103393],[58.16343353489,52.653785066193]]]},"properties":{"type":"2","balloonContent":"","hintContent":"\u0410\u041e \u0427\u041c\u0417"}},{"type":"Feature","id":1,"geometry":{"type":"Polygon","coordinates":[[[58.150079164831,52.655108242838],[58.149926015372,52.654435008376],[58.150057894112,52.654322355597],[58.150077746783,52.654397457449],[58.150062148257,52.654410868494],[58.150179846059,52.654947310297],[58.15020395279,52.654928534834],[58.15021671517,52.655003636687],[58.150079164831,52.655108242838]]]},"properties":{"type":"1","balloonContent":"","hintContent":"\u041a\u043e\u0440\u043f\u0443\u0441 80"}},{"type":"Feature","id":2,"geometry":{"type":"Polygon","coordinates":[[[58.150300677888,52.657352404183],[58.150256718663,52.657384590691],[58.150249628461,52.657355086392],[58.1502099233,52.657389955109],[58.15021701351,52.657424823826],[58.150139021116,52.657481150215],[58.150171636121,52.657617942875],[58.150185107535,52.657601849621],[58.15023261196,52.657827155178],[58.150212759384,52.657843248432],[58.15024821042,52.657977358883],[58.150414120796,52.657853977268],[58.150300677888,52.657352404183]]]},"properties":{"type":"1","balloonContent":"","hintContent":"\u041a\u043e\u0440\u043f\u0443\u0441 \u211634"}},{"type":"Feature","id":3,"geometry":null,"properties":{"type":"3","balloonContent":"\u043f\u0440\u0438\u043d\u0442\u0435\u0440 \u043c\u0430\u0440\u0430\u0442\u0430","hintContent":"\u041f\u0440\u0438\u043d\u0442\u0435\u0440 90002289 (Ricoh SP100SF)"}},{"type":"Feature","id":4,"geometry":null,"properties":{"type":"21","balloonContent":"\u043c\u0430\u0433\u0438\u0441\u0442\u0440\u0430\u043b\u044c \u041c1","hintContent":"\u041c1"}}]}


Здравствуйте, задание по YII2: создание sql запроса к трем таблицам базы EAV mysql и вывод данных в GridView.
Должен работать классический yii2 фильтр по всем выведенным полям.

таблицы:
product - продукты (ключ: product_id);
params - параметры продуктов (ключи: product_id, param_id);
value - значения параметров продуктов (ключ: param_id)


результирующая таблица должна иметь вид:
product_id | param_id1 | param_id2 | param_id3
1          | value1    | value2    | value3
2          | value1    | value2    | value3
3          | value1    | value2    | value3

конкретные Id параметров должны браться из массива вида [param_id1, param_id2, param_id3]


Здравствуйте, нужно выполнить серию заданий по YII2 после предварительной консультации по скайпу (крайне желательно голосом):
1. вывод таблицы в GridView с произвольным количеством столбцов и фильтрацией данных по ним 
index.php?r=admin%2Fshop%2Fproduct%2Findex
ячейки должны выводится с назначенным для них data-attribute
2. редактирование\создание товара с произвольным количеством значений из таблицы
index.php?r=admin%2Fshop%2Fproduct%2Fupdate&id=1
3. добавить dropdown в фильтр связанного поля в GridView 
/index.php?r=admin%2Fshop%2Forder%2Findex
4. вывод связанных товаров на странице товара в админке и их выбор\добавление\удаление
5. bootstrap, asset в модулях, как хранить и загружать из модуля или виджета
6. htaccess не работает, /web/ не убирается, разобраться
7. организовать авторизацию на сайте по ролям из базы

если есть достаточный опыт и свободное время - пишите в скайп.
оплата по договоренности частями, размер оплаты устанавливаем совместно по результатам собеседования.
дальнейше сотрудничество по результатам.

категории электроника с торгмаил
Array
(
    [Categories] => Array
        (
            [ResultsTotal] => 14
            [ResultsPerPage] => 14
            [Page] => 1
            [Listing] => Array
                (
                    [0] => Array
                        (
                            [Id] => 102
                            [ParentId] => 2529
                            [Name] => Теле- и видеотехника
                            [Nick] => tele-i-videotehnika
                            [ChildrenTotal] => 16
                        )

                    [1] => Array
                        (
                            [Id] => 78
                            [ParentId] => 2529
                            [Name] => Аудиотехника
                            [Nick] => audiotehnika
                            [ChildrenTotal] => 29
                        )

                    [2] => Array
                        (
                            [Id] => 172
                            [ParentId] => 2529
                            [Name] => Фото- и видеокамеры
                            [Nick] => foto
                            [ChildrenTotal] => 28
                        )

                    [3] => Array
                        (
                            [Id] => 833
                            [ParentId] => 2529
                            [Name] => Связь
                            [Nick] => svyaz
                            [ChildrenTotal] => 23
                        )

                    [4] => Array
                        (
                            [Id] => 34
                            [ParentId] => 2529
                            [Name] => Автоэлектроника и GPS
                            [Nick] => avtoelektronika-i-gps
                            [ChildrenTotal] => 21
                        )

                    [5] => Array
                        (
                            [Id] => 335
                            [ParentId] => 2529
                            [Name] => Ноутбуки и компьютерная техника
                            [Nick] => kompyutery
                            [ChildrenTotal] => 15
                        )

                    [6] => Array
                        (
                            [Id] => 343
                            [ParentId] => 2529
                            [Name] => Комплектующие
                            [Nick] => komplektuyuschie
                            [ChildrenTotal] => 16
                        )

                    [7] => Array
                        (
                            [Id] => 422
                            [ParentId] => 2529
                            [Name] => Периферийные устройства
                            [Nick] => periferiya
                            [ChildrenTotal] => 16
                        )

                    [8] => Array
                        (
                            [Id] => 398
                            [ParentId] => 2529
                            [Name] => Сетевые устройства
                            [Nick] => setevoe-oborudovanie
                            [ChildrenTotal] => 23
                        )

                    [9] => Array
                        (
                            [Id] => 420
                            [ParentId] => 2529
                            [Name] => Программное обеспечение
                            [Nick] => programmnoe-obespechenie
                            [ChildrenTotal] => 21
                        )

                    [10] => Array
                        (
                            [Id] => 118
                            [ParentId] => 2529
                            [Name] => Техника для дома
                            [Nick] => tehnika-dlya-doma
                            [ChildrenTotal] => 20
                        )

                    [11] => Array
                        (
                            [Id] => 129
                            [ParentId] => 2529
                            [Name] => Техника для кухни
                            [Nick] => tehnika-dlya-kuhni
                            [ChildrenTotal] => 57
                        )

                    [12] => Array
                        (
                            [Id] => 164
                            [ParentId] => 2529
                            [Name] => Климатическая техника
                            [Nick] => klimaticheskaya-tehnika
                            [ChildrenTotal] => 28
                        )

                    [13] => Array
                        (
                            [Id] => 3001
                            [ParentId] => 2529
                            [Name] => Техника для красоты
                            [Nick] => personalnyj-uhod
                            [ChildrenTotal] => 17
                        )

                )

        )

)
Array
(
    [Categories] => Array
        (
            [ResultsTotal] => 16
            [ResultsPerPage] => 16
            [Page] => 1
            [Listing] => Array
                (
                    [0] => Array
                        (
                            [Id] => 109
                            [ParentId] => 102
                            [Name] => Телевизоры
                            [Nick] => tv
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 1480
                        )

                    [1] => Array
                        (
                            [Id] => 1023
                            [ParentId] => 102
                            [Name] => Проекторы
                            [Nick] => projectors
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 1668
                        )

                    [2] => Array
                        (
                            [Id] => 113
                            [ParentId] => 102
                            [Name] => DVD/Blu-Ray плееры
                            [Nick] => dvdplayers
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 115
                        )

                    [3] => Array
                        (
                            [Id] => 2098
                            [ParentId] => 102
                            [Name] => Медиаплееры
                            [Nick] => mediaplayers
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 94
                        )

                    [4] => Array
                        (
                            [Id] => 110
                            [ParentId] => 102
                            [Name] => Домашние кинотеатры
                            [Nick] => hometheaters
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 36
                        )

                    [5] => Array
                        (
                            [Id] => 1026
                            [ParentId] => 102
                            [Name] => Экраны
                            [Nick] => projectionscreens
                            [ChildrenTotal] => 0
                            [Type] => parameterized
                            [ModelsTotal] => 18686
                        )

                    [6] => Array
                        (
                            [Id] => 116
                            [ParentId] => 102
                            [Name] => Спутниковое и кабельное ТВ
                            [Nick] => sputnikovoe-i-kabelnoe-tv
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 1019
                        )

                    [7] => Array
                        (
                            [Id] => 4494
                            [ParentId] => 102
                            [Name] => Телевизионные ресиверы
                            [Nick] => televizionnye-resivery
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 1146
                        )

                    [8] => Array
                        (
                            [Id] => 191
                            [ParentId] => 102
                            [Name] => Аксессуары
                            [Nick] => aksessuary-191
                            [ChildrenTotal] => 6
                        )

                    [9] => Array
                        (
                            [Id] => 2085
                            [ParentId] => 102
                            [Name] => Лампы для проекторов
                            [Nick] => projectorlamps
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 1799
                        )

                    [10] => Array
                        (
                            [Id] => 4714
                            [ParentId] => 102
                            [Name] => Объективы для проекторов
                            [Nick] => obektivy-dlya-proektorov
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 359
                        )

                    [11] => Array
                        (
                            [Id] => 4416
                            [ParentId] => 102
                            [Name] => Антенны
                            [Nick] => anteny
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 1109
                        )

                    [12] => Array
                        (
                            [Id] => 2636
                            [ParentId] => 102
                            [Name] => Электроника - Услуги
                            [Nick] => elektronika-uslugi
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 269
                        )

                    [13] => Array
                        (
                            [Id] => 4420
                            [ParentId] => 102
                            [Name] => 3D-очки
                            [Nick] => 3d-ochki
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 134
                        )

                    [14] => Array
                        (
                            [Id] => 4850
                            [ParentId] => 102
                            [Name] => Коммутационное оборудование
                            [Nick] => kommutacionnoe-oborudovanie
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 291
                        )

                    [15] => Array
                        (
                            [Id] => 1117
                            [ParentId] => 102
                            [Name] => Прочее
                            [Nick] => prochee-1117
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 993
                        )

                )

        )

)



ТОВАРЫ ДЛЯ ДОМА
Array
(
    [Categories] => Array
        (
            [ResultsTotal] => 19
            [ResultsPerPage] => 19
            [Page] => 1
            [Listing] => Array
                (
                    [0] => Array
                        (
                            [Id] => 119
                            [ParentId] => 118
                            [Name] => Пылесосы
                            [Nick] => vacuumcleaners
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 1446
                        )

                    [1] => Array
                        (
                            [Id] => 120
                            [ParentId] => 118
                            [Name] => Стиральные машины
                            [Nick] => washingmachines
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 1447
                        )

                    [2] => Array
                        (
                            [Id] => 121
                            [ParentId] => 118
                            [Name] => Швейные и вязальные машины
                            [Nick] => shvejnye-i-vyazalnye-mashiny
                            [ChildrenTotal] => 4
                        )

                    [3] => Array
                        (
                            [Id] => 122
                            [ParentId] => 118
                            [Name] => Вентиляторы
                            [Nick] => fans
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 214
                        )

                    [4] => Array
                        (
                            [Id] => 123
                            [ParentId] => 118
                            [Name] => Утюги
                            [Nick] => irons
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 1111
                        )

                    [5] => Array
                        (
                            [Id] => 2593
                            [ParentId] => 118
                            [Name] => Пароочистители и отпариватели
                            [Nick] => otparivateli
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 3411
                        )

                    [6] => Array
                        (
                            [Id] => 126
                            [ParentId] => 118
                            [Name] => Сушильные автоматы
                            [Nick] => sushilnye-avtomaty
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 740
                        )

                    [7] => Array
                        (
                            [Id] => 2539
                            [ParentId] => 118
                            [Name] => Аксессуары для пылесосов
                            [Nick] => aksessuary-dlya-pylesosov
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 5898
                        )

                    [8] => Array
                        (
                            [Id] => 4268
                            [ParentId] => 118
                            [Name] => Машинки для удаления катышков
                            [Nick] => mashinki-dlya-udaleniya-katyshkov
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 100
                        )

                    [9] => Array
                        (
                            [Id] => 2509
                            [ParentId] => 118
                            [Name] => Мини-мойки
                            [Nick] => washers
                            [ChildrenTotal] => 0
                            [Type] => model
                            [ModelsTotal] => 335
                        )

                    [10] => Array
                        (
                            [Id] => 4372
                            [ParentId] => 118
                            [Name] => Сушилки для обуви
                            [Nick] => sushilki-dlya-obuvi
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 129
                        )

                    [11] => Array
                        (
                            [Id] => 4457
                            [ParentId] => 118
                            [Name] => Стеклоочистители
                            [Nick] => stekloochistiteli
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 112
                        )

                    [12] => Array
                        (
                            [Id] => 4480
                            [ParentId] => 118
                            [Name] => Очистители ультразвуковые
                            [Nick] => ochistiteli-ultrazvukovye
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 86
                        )

                    [13] => Array
                        (
                            [Id] => 4573
                            [ParentId] => 118
                            [Name] => Проращиватели семян
                            [Nick] => proraschivateli-semyan
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 24
                        )

                    [14] => Array
                        (
                            [Id] => 4607
                            [ParentId] => 118
                            [Name] => Гладильные системы
                            [Nick] => gladilnye-sistemy
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 616
                        )

                    [15] => Array
                        (
                            [Id] => 4754
                            [ParentId] => 118
                            [Name] => Электровеники
                            [Nick] => elektroveniki
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 78
                        )

                    [16] => Array
                        (
                            [Id] => 4376
                            [ParentId] => 118
                            [Name] => Аксессуары и запчасти для бытовой техники
                            [Nick] => aksessuary-dlya-bytovoj-tehniki
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 6014
                        )

                    [17] => Array
                        (
                            [Id] => 2635
                            [ParentId] => 118
                            [Name] => Бытовая техника - услуги
                            [Nick] => bytovaya-tehnika-uslugi
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 159
                        )

                    [18] => Array
                        (
                            [Id] => 1133
                            [ParentId] => 118
                            [Name] => Прочее
                            [Nick] => prochee-1133
                            [ChildrenTotal] => 0
                            [Type] => general
                            [OffersTotal] => 4805
                        )

                )

        )

)