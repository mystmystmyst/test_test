<?php

namespace app\modules\shop\controllers;

use Yii;
use app\models\Category;
use app\modules\shop\models\Product;
use app\modules\shop\models\Value;
use app\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use yii\data\ActiveDataProvider;

class CategoryController extends Controller {

	public function behaviors() {
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}
	/**
	 * главная стр модуля shop.
	 * @return mixed
	 */
		public function actionHome() {
	$category = Category::find()
				->where(['cat_id' => 3])
				//->select(['name','item_structure'])
				->asArray()
				->one();
	//вытаскиваем виджеты для этой страницы
		 $widgets =  \app\models\Widgetset::find()
			->where(['widgetset_id' => $category['widgetset_id'], 'widgetset.published'=>1])
			->innerJoinWith('widget')
			->orderBy('ordering')
			->asArray()
			->all();
		return $this->render('home', [
			'category' =>$category,
			'widgets'=>$widgets,
		]);
	}
	/**
	 * показать список подкатегорий.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new CategorySearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
					'searchModel' => $searchModel,
					'dataProvider' => $dataProvider,
		]);
	}
	
	//показать страницу категории Category с $id = $cat_id.
	public function actionView($id, $param_id='') {
		
		
		
		$category = Category::find()
				->where(['cat_id' => $id])
				//->select(['name','item_structure'])
				->asArray()
				->one();
		//достаем ГРУППЫ параметров
		//$params=json_decode($category['params'], true);
//		$params = \app\modules\shop\models\Param::find()
//				//->where(['group_id'=>$params])
//				->where(['cat_id'=>$id])
//				->select('param_id')
//				->indexBy('param_id')
//				->asarray()
//				->all();
		
		//строим запрос для пагинатора
		if($param_id){  //если задан поиск по параметру
			//ищем продукты с нужными параметрами
			echo 'переделать в контроллере';exit;
			$pid= Value::find()
					->where(['param_id'=>$param_id])
					->select('product_id')->asArray()
					->indexBy('product_id')
					->all();
		
			$query = Product::find()
				->where(['shop_product.cat_id' => $id,'shop_product.published' => 1])
				->where(['shop_product.product_id' => array_keys($pid)])
				->with(['value' => function ($query)  {	//use($item_structure)
							$query->andWhere(['shop_product_value.param_id' => array_keys($params)]);
						},
					'value.param']) //жадная загрузка из связей в модели product
				->asArray();
		}else{
			$query = Product::find()
					->where(['shop_product.cat_id' => $id])
					->with([
						//'value',
//						'value' => function ($query) use($id) {	
//							$query->andWhere(['shop_product_value.cat_id' => $id]);
//						},
						'value.param'
						]) //жадная загрузка из связей в модели product
						//->innerJoinWith(['value.param','value'])
						->asArray()
						;
		}
		//данные для пагинатора
		//$pagination = new \yii\data\Pagination(['totalCount'=>$query->count(), 'pageSize'=>12, 'pageSizeParam'=>false, 'forcePageParam'=>false]);
		//$product = $query->offset($pagination->offset)->limit($pagination->limit)->asarray()->all();
		$dataProvider = new \yii\data\ActiveDataProvider([
							'query' => $query,
							'pagination' => [
								'pageSize' => 12,
							],
						]);

//		$params = \app\modules\shop\models\Param::find()
//				->where(['cat_id' => $id])
//				//->indexby('group_id')
//				->joinWith('value', true, 'INNER JOIN')
//				->with(['value' => function ($q) use($id){
//						$q->andWhere(['product_id' => $id]);
//					},
//					])
//					->asarray()
//					->all();
					
					//dump($product);exit;
		//вытаскиваем виджеты для этой страницы
		 $widgets =  \app\models\Widgetset::find()
			->where(['widgetset_id' => $category['widgetset_id'], 'widgetset.published'=>1])
			//->with(['value','value.param']) //жадная загрузка из связей в модели Page
			->innerJoinWith('widget')
			->orderBy('ordering')	 //ORDERING виджетов по порядку
			//->indexBy('position')
			->asArray()
			->all();
		 
		
	//dump($product);
		return $this->render('view', [
			//'structure' => json_decode($category['item_structure'], true), //структура одного item категории
			//'layout'=> json_decode($category['layout']['template_layout'], true),
			//'template_structure'=> json_decode($category['template_structure'], true), //структура шаблона страницы
			//'pagination'=>$pagination,
			//'product' => $product,
			'category' =>$category,
			'widgets'=>$widgets,
			'dataProvider'=>$dataProvider
		]);
	}

	/**
	 * Creates a new Category model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Category();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->cat_id]);
		} else {
			return $this->render('create', [
						'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Category model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->cat_id]);
		} else {
			return $this->render('update', [
						'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Category model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Category model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Category the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Category::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
	
	

}
