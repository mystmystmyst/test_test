<?php

namespace app\modules\shop\controllers\admin;

use Yii;
use app\modules\shop\models\Product;
use app\modules\shop\models\AdminProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{

    public $layout = '@app/modules/admin/views/layouts/main';

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Product models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new AdminProductSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		return $this->render('index', [
					'searchModel' => $searchModel,
					'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Product model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
					'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Product model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Product();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->product_id]);
		} else {
			return $this->render('create', [
						'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Product model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$cat_id = Product::find()->where(['product_id' => $id])->select('cat_id')->asarray()->one();
		$model = \app\modules\shop\models\Param::find()
				->where(['cat_id' => $cat_id['cat_id']])
				->orwhere(['cat_id' => 0])
				->andwhere(['parent' => 0])	//достаем только верхние группы
				->with('parentparams.value')
				->asarray()
				->all();
//dump($model);		
//---------------------------------------------------------------
//		$model = \app\modules\shop\models\ParamsGroup::find()
//				->where(['shop_product_params_group.cat_id' => $cat_id['cat_id']])
//				->orwhere(['shop_product_params_group.cat_id' => 0])
//				->indexby('name')
//				->innerJoinWith([
//					'param' => function ($query) {
//						$query->select(['param_id', 'name', 'type', 'link_type', 'name_visible', 'class', 'visible', 'tag', 'group_id'])->indexby('name');
//					},
//							'param.value' => function ($query) use($id) {
//						$query->andWhere(['product_id' => $id])->indexby('name');
//					}])
//						->with('param', 'param.value')
//						->asArray()
//						->all();
//---------------------------------------------------------------
		//$product['group']=$params;
		//dump($model);	
//		$model = Product::find()
//					->where(['product_id' => $id])
//					->with([
//						'value' => function ($q) use($id) {
//							$q->andWhere(['shop_product_value.product_id' => $id]);
//						},
//						'value.param'
//						]) //жадная загрузка из связей в модели product
//						//->innerJoinWith(['value.param','value'])
//						//->asArray()
//								->one()
//						;

		return $this->render('update', [
					'model' => $model,
						//'value' => $value,
		]);
		//если нажата кнопка сохранить
//		if ($model->load(Yii::$app->request->post()) && $model->save()) {
//			return $this->redirect(['view', 'id' => $model->product_id, 'value' => $value,]);
//		} else {
//			return $this->render('update', [
//						'model' => $model,
//						//'value' => $value,
//			]);
//		}
	}

	/**
	 * Deletes an existing Product model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Product model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Product the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Product::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function test() {

		$aParams = [];

		$aParams["options"] = array("item_text" => 2, "item_text_preview" => 5, "item_price" => 6); //это список необходимых дополнительных полей
		///$aParams["search_options"] = array("item_price" => "10000"); //это поиск по дополнительным полям
		$aParams["search"] = array("category_name" => "Телевизоры"); //это поиск по основным полям, нужно перечислить возможные поля для поиска
		$aParams["order"] = 1; //сортировка, список возможных сортировок зададим заранее
		$aParams["order_options"] = "item_price DESC"; //дополнительная сортировка, имеет приоритет
		$aParams["page"] = 3; //номер страницы
		$aParams["items_on_page"] = 15; //кол-во элементов на странице

		function aGetContent($aParams) {
			$aData = [];
			$aData["items_sql"] = "";
			$aData["options_sql"] = "";
			$aData["page_count_sql"] = "";

			//сортировка

			$aOrderTypes = [];

			$aOrderTypes[1] = "p.product_id";
			$aOrderTypes[2] = "p.product_id DESC";
			$aOrderTypes[3] = "c.name, p.product_id";
			$aOrderTypes[4] = "c.name DESC, p.product_id DESC";

			if (isset($aParams["order_options"]) and ! empty($aParams["order_options"])) {
				foreach ($aOrderTypes as $iKey => $sValue) {
					$aOrderTypes[$iKey] = $aParams["order_options"] . ", " . $sValue;
				}
			}

			if (isset($aParams["options"]) and ! empty($aParams["options"])) {
				//доп. параметры пришли
				//фильтры по основным параметрам

				$aWhere = array();
				$aWhere[] = "TRUE";

				if (isset($aParams["search"]["category_name"]) and ! empty($aParams["search"]["category_name"])) {
					//$aWhere[] = "c.name = '" . $oDB->escape_string($aParams["search"]["category_name"]) . "'"; или так $aWhere[] = "c.name = '" . mysql_real_escape_string($aParams["search"]["category_name"]) . "'"; - это нужно обязательно сделать, не знаю какой синтаксис использовать
					$aWhere[] = "c.name = '" . $aParams["search"]["category_name"] . "'";
				}

				if (isset($aParams["search"]["category_id"]) and ! empty($aParams["search"]["category_id"])) {
					$aWhere[] = "p.cat_id = " . $aParams["search"]["cat_id"] . "";
				}

				//другие фильтры, если они есть, нужно добавить самостоятельно по аналогии с категориями
				//фильтры по доп. парметрам

				$aHaving = [];

				if (isset($aParams["search_options"]) and ! empty($aParams["search_options"])) {
					foreach ($aParams["search_options"] as $sOptionName => $sOptionValue) {
						if (!empty($sOptionValue)) {
							//$aHaving[] = $sOptionName . " = '" . $oDB->escape_string($sOptionValue) . "'"; или так $aHaving[] = $sOptionName . " = '" . mysql_real_escape_string($sOptionValue) . "'"; - это нужно обязательно сделать, не знаю какой синтаксис использовать
							$aHaving[] = $sOptionName . " = '" . $sOptionValue . "'";
						}
					}
				}

				$sSql = "SELECT SQL_CALC_FOUND_ROWS
							p.product_id,
							p.published AS product_published,
							c.cat_id,
							c.name AS cat_name";

				foreach ($aParams["options"] as $sOptionName => $iOptionId) {
					$sSql .= ",
							COALESCE(GROUP_CONCAT(IF(pv.param_id = " . $iOptionId . ", pv.value, NULL)), '') AS " . $sOptionName;
				}

				$sSql .= "
					FROM
					  shop_product AS p
					  INNER JOIN category AS c ON p.cat_id = c.cat_id
					  LEFT JOIN shop_product_value AS pv ON
						p.product_id = pv.product_id AND
						pv.param_id IN (" . implode(", ", $aParams["options"]) . ")
					WHERE
					  " . implode(" AND
					  ", $aWhere) . "
					GROUP BY
					  p.product_id";

				if (!empty($aHaving)) {
					$sSql .= "
					HAVING
					  " . implode(",
					  ", $aHaving);
				}

				$sSql .= "
					ORDER BY
					  " . $aOrderTypes[$aParams["order"]] . "
					LIMIT
					  " . (($aParams["page"] - 1) * $aParams["items_on_page"]) . ", " . $aParams["items_on_page"];

						$aData["items_sql"] = $sSql;

						$aData["page_count_sql"] = "SELECT CEIL(FOUND_ROWS() / " . $aParams["items_on_page"] . ")";

						$aData["options_sql"] = "SELECT
							o.param_id AS option_id,
							o.name AS option_name
						  FROM
							shop_product_param AS o
						  WHERE
							o.param_id IN (" . implode(", ", $aParams["options"]) . ")";
			}

			return $aData;
		}

		$aData = aGetContent($aParams);
return $aData;
		//dump($aData['items_sql'] . ';' . $aData['page_count_sql'] . ';' . $aData['options_sql'] . ';');


//$aData["page_count_sql"]; - этот запрос нужно выполнить после запроса $aData["items_sql"]
	}


}
