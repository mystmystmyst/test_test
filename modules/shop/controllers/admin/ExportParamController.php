<?php

namespace app\modules\shop\controllers\admin;

use Yii;
use app\modules\shop\models\ExportParam;
use app\modules\shop\models\ExportParamSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExportParamController implements the CRUD actions for ExportParam model.
 */
class ExportParamController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ExportParam models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExportParamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ExportParam model.
     * @param integer $export_id
     * @param integer $param_id
     * @return mixed
     */
    public function actionView($export_id, $param_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($export_id, $param_id),
        ]);
    }

    /**
     * Creates a new ExportParam model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ExportParam();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'export_id' => $model->export_id, 'param_id' => $model->param_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ExportParam model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $export_id
     * @param integer $param_id
     * @return mixed
     */
    public function actionUpdate($export_id, $param_id)
    {
        $model = $this->findModel($export_id, $param_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'export_id' => $model->export_id, 'param_id' => $model->param_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ExportParam model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $export_id
     * @param integer $param_id
     * @return mixed
     */
    public function actionDelete($export_id, $param_id)
    {
        $this->findModel($export_id, $param_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ExportParam model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $export_id
     * @param integer $param_id
     * @return ExportParam the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($export_id, $param_id)
    {
        if (($model = ExportParam::findOne(['export_id' => $export_id, 'param_id' => $param_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
