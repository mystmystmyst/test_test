<?php

namespace app\modules\shop\controllers\admin;

use Yii;
use app\modules\shop\models\Export;
use app\modules\shop\models\ExportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExportController implements the CRUD actions for Export model.
 */
class ExportController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Export models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new ExportSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
					'searchModel' => $searchModel,
					'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Export model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
					'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Export model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Export();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
						'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Export model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
						'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Export model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Export model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Export the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Export::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * создаем файл yml с заданным именем и кешируем его
	 */
	public function actionCreateyml($id=1) {
		
//смотрим какой параметр отвечает за выгрузку
$export = Export::find($id)->asArray()->one();
$params = json_decode($export['params'], TRUE);
$params['exp']=$export['param_id'];
//dump($params);exit;

//достаем все продукты с параметром и параметры, указанные в шаблоне
$products = \app\modules\shop\models\Product::find()
	//->where(['shop_product.published'=>1])
	-> joinWith([
		'value' => function ($query) use($params) {
			$query->andWhere(['shop_product_value.param_id' => $params])
				//->andWhere(['shop_product_value.param_id' => $export['param_id']])
					;
		}
		])
	//->select('shop_product.alias')
	->asarray()->limit(3)
	->all();
		

//достаем категории для блока <categories>	
$cat_id = \yii\helpers\ArrayHelper::getColumn($products, 'cat_id');

$cats = \app\models\Category::find()
		->where(['cat_id'=>$cat_id])
		->asarray()
		->select(['name','cat_id'])
		->all();
//dump($categories);exit;

//
//		if ($model->load(Yii::$app->request->post()) && $model->save()) {
//			return $this->redirect(['view', 'id' => $model->id]);
//		} else {
			return $this->render('createyml', [
						'cats'=>$cats,
						'params' => $params,
			]);
//		}
	}
	
	
//	function getYML($data) {
//		
//	}

}
