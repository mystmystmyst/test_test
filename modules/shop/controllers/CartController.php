<?php

namespace app\modules\shop\controllers;

use yii\web\Controller;
use yii\helpers\HtmlPurifier;
use app\modules\shop\models\Cart;
use \app\modules\users\models\User;
use app\modules\biz\models\BizOrg;
use app\modules\shop\models\Order;
use app\modules\shop\models\OrderItem;
use app\components\SMSCenter;
use Yii;

/**
 *  
  //[cart]=>[
  //	[1]	=>[    //id страницы товара
  //		[qty]=>1
  //		[name]=>имя продукта
  //		[price]=>цена
  //		[img]=>картинка
  //	]
  //	[22]	=>[
  //		[qty]=>1
  //		[name]=>имя продукта
  //		[price]=>цена
  //		[img]=>картинка
  //	]
  //]
  //[qty]=> общ количество товаров
  //[sum]=> общ сумма
  //[compare] =>[1,2,22] id товаров
  //[wish] => [1,2,22] id товаров
 */
class CartController extends Controller {

//ДОБАВЛЕНИЕ В КОРЗИНу   
	public function actionAdd() {

		$product_id = Yii::$app->request->get('id'); //получаем id товара из запроса
		$product = \app\modules\shop\models\Value::find()
				->where(['product_id' => $product_id])
				->indexBy('param_id')
				->asArray()
				->all();
		//dump($product_id);exit;
		if (empty($product))
			return false; // Если товар не найдет ничего не возвращаем

		$session = Yii::$app->session;
		$session->open(); //ОТКР  СЕССИЮ
		$cart = new Cart();
		$cart->addToCart($product, $product_id);
		$this->layout = false;
		return $this->render('cart-modal', ['session' => $session]);
	}

	//очистка корзины
	public function actionClear() {
		$session = Yii::$app->session;
		$session->open(); //ОТКР  СЕССИЮ
		$session->remove('cart');
		$session->remove('sum');
		$session->remove('qty');
		//$session->remove('cart.qty');
		//$session->remove('cart.sum');
		$this->layout = false;
		return $this->render('cart-modal', ['session' => $session]);
	}

	//удаление итема
	public function actionDelItem() {

		$id = Yii::$app->request->get('id'); //получаем id из ссылки
		$session = Yii::$app->session;
		$session->open(); //ОТКР  СЕССИЮ
		$cart = new Cart();
		$cart->recalc($id);
		$this->layout = false;
		return $this->render('cart-modal', ['session' => $session]);
	}

	//получение minicart
	public function actionMinicart() {
		$session = Yii::$app->session;
		$session->open();
		$this->layout = false;
		return $this->render('cart-modal', ['session' => $session]);
	}

	//основная корзина
	/**
	 * второй шаг корзины
	 */
	public function actionView() {
		$cartform1 = new \app\modules\shop\models\CartForm1(); //форма 
		//если нажата кнопка:
		if ($cartform1->load(Yii::$app->request->post()) && $cartform1->validate()) {
				$session = Yii::$app->session;
				$session->open();
				
				$session['cart.delivery']=$cartform1->delivery;
				$session['cart.payment']=$cartform1->payment;
			return $this->redirect(['view2']);		
		}

		$session = Yii::$app->session;
		$session->open();
		$delivery = \app\modules\shop\models\Delivery::findAll(['published' => 1]);
		$payment = \app\modules\shop\models\Payment::findAll(['published' => 1]);
		
				//вытаскиваем виджеты для этой страницы

		$cat = \app\models\Category::findOne(['alias' => 'cart']);
		$widgets = \app\models\Widgetset::find()
				->where(['widgetset_id' => $cat->widgetset_id])
				->andwhere(['widgetset.published' => 1])
				->innerJoinWith('widget')
				->orderBy('ordering')  //ORDERING виджетов по порядку
				->asArray()
				->all();


		return $this->render('view', [
					'session' => $session,
					'delivery' => $delivery,
					'payment' => $payment,
					'cartform1' => $cartform1,
					//'user' => $user,
					'widgets' => $widgets,
		]);
	}
	
	/**
	 * второй шаг корзины
	 */
	public function actionView2() {
				
	$cartform2 = new \app\modules\shop\models\CartForm2(); //форма 
		//если нажата кнопка:
		if ($cartform2->load(Yii::$app->request->post()) && $cartform2->validate()) {

			//проверяем заполнена ли организация
				if (!isset($cartform2->inn)) {
					//сначала проверяем существование организации, чтобы получить ее id...........
					$org = BizOrg::findOrgIdByInn($cartform2->inn);
					//если нет то создаем
					if (!isset($org)) {
						$org = new BizOrg;
						$org->full_name = HtmlPurifier($cartform2->org_name);
						$org->inn = HtmlPurifier($cartform2->inn);
						$org->ogrn = HtmlPurifier($cartform2->ogrn);
						$org->gorod = HtmlPurifier($cartform2->gorod);
						$org->fact_addr = HtmlPurifier($cartform2->addr);
						$org->tel = HtmlPurifier($cartform2->tel1);
						$org->email = HtmlPurifier($cartform2->em);
						$org->save();
					}
				}
				//ищем пользователя по email, ..................
				$user_id = User::findByEmail($cartform2->email)['id'];

				//если нет то создаем иначе просим зарегится
				if (!isset($user_id)) {
					dump('создание пользователя');
					$user = new User;
					$user->username = $cartform2->email;
					$user->email = $cartform2->email;
					$user->tel1 = $cartform2->tel1;
					$user->name = $cartform2->name;
					$cartform2->org_name ? $user->short_name = $cartform2->org_name : $user->short_name = $cartform2->name;
					$user->addr = $cartform2->addr;
					!isset($org) ? $user->org_id = $org->org_id : $user->org_id = 0;
					$user->group_id = 0;
					$org->status = User::STATUS_WAIT;
					//$user->auth_key = User::generateAuthKey();
					$user->password_hash = User::setPassword(User::generatePassword());
//					$user->created_at = new \yii\db\Expression('NOW()');
//					$user->updated_at = $user->created_at;

					if ($user->save()) {
						dump($user->errors);
					}
				}else{
					
//					yii::$app->getUser()->setReturnUrl(yii\helpers\Url::to(['/shop/cart/view2']));
//					 return $this->redirect(yii\helpers\Url::to(['/users/default/login']));	
				}
				$session = Yii::$app->session;
				$session->open();
//СОХРАНИТЬ ЗАКАЗ
				$order = new Order;
				$order->initiator_id = $user_id;
				$order->sum = $session['sum'];
				$order->qty = $session['qty'];
				$order->status_id = 0; //новый
				$order_items['cart'] = $session['cart'];
				$order_items['qty'] = $session['qty'];
				if (isset($org_id))
					$order_items['org'] = $org_id;
				$order->order_items = json_encode($order_items, JSON_UNESCAPED_UNICODE);
//dump($order);exit;
				//сохраняем заказ, создаем pdf и отправляем
				if ($order->save()) {
					//dump($order);
					//генерируем PDF
					$PDFcontent = $this->renderPartial('order-html',['order' =>$order]);
					$pdf = Yii::$app->pdf;
					$pdf->destination = 'F'; //'I'
					
					$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
					$pdf->filename = $_SERVER['DOCUMENT_ROOT'] . '/modules/shop/orders/' . $order->order_id . '.pdf';
					$pdf->content = $PDFcontent;
					echo $pdf->render();

//dump($session['cart']);exit;

					//ОТПРАВЛЯЕМ СООБЩЕНИЕ ПОЛЬЗОВАТЕЛЮ
					Yii::$app->mailer->compose('@app/modules/shop/mail/views/order', ['session' => $session]) // здесь устанавливается результат рендеринга вида в тело сообщения
							->setFrom([Yii::$app->params['siteEmail'] => Yii::$app->params['siteName']])   // от кого будет получено письмо
							->setTo($cartform2->email)   //кому будет отправлено
							->setSubject('Заказ принят')  //тема письма
							->attach($_SERVER['DOCUMENT_ROOT'] .'/modules/shop/orders/' . $order->order_id . '.pdf')
							->send();

					//ОТПРАВЛЯЕМ СООБЩЕНИЕ админу
					Yii::$app->mailer->compose('@app/modules/shop/mail/views/order', ['session' => $session]) // здесь устанавливается результат рендеринга вида в тело сообщения
							->setFrom([Yii::$app->params['siteEmail'] => Yii::$app->params['siteName']])   // от кого будет получено письмо
							->setTo(Yii::$app->params['adminEmail'])   //кому будет отправлено
							->setSubject('Новый заказ с сайта')  //тема письма
							->send();
					
					if(Yii::$app->params['shop']['sms'] == 1){
					// ОТПРАВЛЯЕМ SMS
						// Инициализация
						$smsc = new SMSCenter('vmela.ru', md5('vmela.ru'), false, [
							'charset' => SMSCenter::CHARSET_UTF8,
							'fmt' => SMSCenter::FMT_XML,
						]);

						// Отправка сообщения					
						$smsc->send(Yii::$app->params['shop']['tel'],
								'Received a new order '.$order->order_id.' for the Vmela.ru to the amount of '.$order['sum'].' rub',
								'Vmela.ru');
					}
					
					yii::$app->session->setFlash('success', 'Ваш заказ принят! На Ваш email выслана информация о заказе. Контролировать состояние заказа Вы можете по ссылке: ');

					//ОЧИЩАЕМ СЕССИЮ
						$session->remove('cart');
						$session->remove('qty');
						$session->remove('sum');
					return $this->redirect(['view3']);	
				} else {
					//ошибка
					//dump($order->errors);
					yii::$app->session->setFlash('error', 'Ошибка сохранения заказа');
					//yii::$app->session->getFlash('error');
				}
			
				
		}

		$session = Yii::$app->session;
		$session->open();
		
				//вытаскиваем виджеты для этой страницы

		$cat = \app\models\Category::findOne(['alias' => 'cart']);
		$widgets = \app\models\Widgetset::find()
				->where(['widgetset_id' => $cat->widgetset_id])
				->andwhere(['widgetset.published' => 1])
				->innerJoinWith('widget')
				->orderBy('ordering')  //ORDERING виджетов по порядку
				->asArray()
				->all();


		return $this->render('view2', [
					'session' => $session,
					'cartform2' => $cartform2,
					'widgets' => $widgets,
		]);
	}
	
	public function actionView3() {
//		$session = Yii::$app->session;
//		$session->open();

		//$user = new \app\modules\users\models\User;

		//ЗАКЛЮЧИТЬ В ТРАНЗАКЦИЮ
		//dump('rerere');



	
		
		//вытаскиваем виджеты для этой страницы

		$cat = \app\models\Category::findOne(['alias' => 'cart']);
		$widgets = \app\models\Widgetset::find()
				->where(['widgetset_id' => $cat->widgetset_id])
				->andwhere(['widgetset.published' => 1])
				->innerJoinWith('widget')
				->orderBy('ordering')  //ORDERING виджетов по порядку
				->asArray()
				->all();


		return $this->render('view3', [
					'widgets' => $widgets,
		]);
	}

	
//СРАВНЕНИЕ ТОВАРОВ
	//добавить к сравнению
	public function actionAddCompare() {
		$page_id = Yii::$app->request->get('id'); //получаем id из ссылки
		$product = \app\modules\pages\models\Value::find()
				->where(['page_id' => $page_id])
				->indexBy('param_id')
				->asArray()
				->all();
		//dump($product);exit;
		if (empty($product))
			return false; // Если товар не найден, ничего не возвращаем
		$session = Yii::$app->session;
		$session->open(); //ОТКР  СЕССИЮ
		$cart = new Cart();
		$cart->addToCart($product, $page_id);
		$this->layout = false;
		return $this->render('cart-modal', ['session' => $session]);
	}

	//удалить из сравнения
	public function actionDelCompare() {
		
	}

	//показать сравнение
	public function actionViewCompare() {
		
	}

	//очистить отложенные
	public function actionClearCompare() {
		
	}

//ОТЛОЖЕННЫЕ ТОВАРЫ
	//добавить к отложенным
	public function actionAddWish() {
		
	}

	//удалить из отложенныx
	public function actionDelWish() {
		
	}

	//показать отложенные
	public function actionViewWish() {
		
	}

	//очистить отложенные
	public function actionClearWish() {
		
	}

}
