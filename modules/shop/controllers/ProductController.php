<?php

namespace app\modules\shop\controllers;

use Yii;
use app\modules\shop\models\Product;
use app\modules\shop\models\ProductSearch;
use app\models\Category;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

//use yii\filters\VerbFilter;

class ProductController extends Controller {

	//   public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['post'],
//                ],
//            ],
//        ];
//    }


	/**
	 * Lists all Page models.
	 * @return mixed
	 */
//    public function actionIndex()
//    {
//        $searchModel = new PageSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }

	/**
	 * Displays a single Page model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id) {
		//(!) запросы страницы унифицированы с запросами категорий для того чтобы использовать общий цикл
		//достаем ID параметров страницы
//		$product= Product::find()
//				->where(['product_id'=>$id])
//				->asarray()
//				->one();
		

//dump($product);		
//		$params = \app\modules\shop\models\ParamsGroup::find()
//				->where(['shop_product_params_group.cat_id' => $product['cat_id']])
//				->orwhere(['shop_product_params_group.cat_id' =>0])
//				->indexby('name')
//				->innerJoinWith([
//					'param' => function ($query) {
//							$query->select(['param_id' ,'name','type','link_type','name_visible','class','visible','tag', 'group_id'])->indexby('name');
//						},
//					'param.value' => function ($query) use($id) {
//							$query->andWhere(['product_id' => $id])->indexby('name');
//						}])
//				->with('param','param.value')
//				->asArray()
//				->all();
//		$product['group']=$params;
//dump($params);
//		//достаем ГРУППЫ параметров
		//$param_groups=json_decode($category['params'], true);
		//из групп достаем список параметров

//		$params =array_keys($params);
////		----------------------------------------------------------------------------------
		$product = Product::find()
					->where(['shop_product.product_id' => $id])
					->asArray()->limit(1)
					//->JoinWith('value.param', true, 'LEFT JOIN')
					->with(['value.param'])
					->one();
		//dump($product);
		//достаем id категории
		$cat = Category::find()
			->where(['cat_id' => $product['cat_id']])
			->select(['cat_id','name', 'widgetset_id','page_view'])
			->asArray()
			->one();
		//достаем все параметры категории
//		$params = \app\modules\shop\models\Param::find()
//				->where(['cat_id'=>$cat['cat_id']])
//				->orWhere(['cat_id'=>0])
//				->indexby('name')
//				->with('value')
//				->asarray()
//				->all();
//комбинируем
		//$product['params'] = $params;
		$product['cat'] = $cat;
		
		//вытаскиваем виджеты для этой страницы , может переделать по widget_id???
		 $widgets =  \app\models\Widgetset::find()
			->where(['widgetset_id' => $cat['widgetset_id'], 'widgetset.published'=>1])
			//->with(['value','value.param']) //жадная загрузка из связей в модели Page
			->innerJoinWith('widget')
			->orderBy('ordering')	 //ORDERING виджетов по порядку
			//->indexBy('position')
			->asArray()
			->all();
	//определяем view.php страницы	 
	$view = $cat['page_view'];
//dump($widgets);
		return $this->render($view, [
					'product' => $product,
//					'params'=>$params,
					//'cat' => $cat,
					'widgets'=>$widgets,
		]);
	}

	/**
	 * Creates a new Page model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Page();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->product_id]);
		} else {
			return $this->render('create', [
						'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Page model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->product_id]);
		} else {
			return $this->render('update', [
						'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Page model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Page model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Page the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Page::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
