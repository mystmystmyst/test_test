<?php

namespace app\modules\shop\controllers;

use Yii;
//use app\models\Config;
//use app\components\Filter\models\Filter;
use yii\web\Controller;
//use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\modules\shop\models\Value;

class FilterController extends Controller
{

    public function actionFind()
    {	Yii::$app->response->format = Response::FORMAT_JSON;
	$get =Yii::$app->request->get();
//ищем параметры которые учавствуют в поиске
	$searchable =  \app\modules\shop\models\Param::find()
			->select('param_id')
			->where(['search'=>1])
			->indexby('param_id')
			->asarray()
			->all();
//dump(array_keys($searchable));exit;
//запрос к базе данных по параметрам
        $result = Value::find()
			->asArray()			
			->where(['like', 'value', $get['value']])
			->andwhere(['param_id'=>array_values($searchable)])
			//->indexby('product_id')
			->limit(10)
			->all();

       $json = json_encode($result);
	   //dump(ArrayHelper::map($result, 'product_id','id'));exit;	
        return $json;
    }  
    
}
