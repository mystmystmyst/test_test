<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\ExportParam */

$this->title = $model->export_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Export Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="export-param-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'export_id' => $model->export_id, 'param_id' => $model->param_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'export_id' => $model->export_id, 'param_id' => $model->param_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'export_id',
            'param_id',
        ],
    ]) ?>

</div>
