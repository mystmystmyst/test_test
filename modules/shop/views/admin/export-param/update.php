<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\ExportParam */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Export Param',
]) . $model->export_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Export Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->export_id, 'url' => ['view', 'export_id' => $model->export_id, 'param_id' => $model->param_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="export-param-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
