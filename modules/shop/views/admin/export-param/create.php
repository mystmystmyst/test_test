<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\ExportParam */

$this->title = Yii::t('app', 'Create Export Param');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Export Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="export-param-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
