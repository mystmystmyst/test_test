<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\ExportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Exports');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="export-index">

<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
	<?= Html::a(Yii::t('app', 'Create Export'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php Pjax::begin(); ?>    <?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [

			'id',
			'description',
			'name',
			'filename',
			'param_id',
			'params',
			['class' => 'yii\grid\ActionColumn'],
		],
	]);
	?>
<?php Pjax::end(); ?></div>
