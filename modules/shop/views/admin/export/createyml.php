<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Export */

$this->title = 'экспорт формируем';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Exports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="export-view">
	<?php
	$dom = new DOMDocument("1.0", "utf-8");
	$dom->formatOutput = true; //форматирует документ
	$yml_catalog = $dom->createElement("yml_catalog"); // Создаём корневой элемент
	$yml_catalog->setAttribute('date', date("Y-m-d H:i"));
	$dom->appendChild($yml_catalog);

//<shop>
	$shop = $dom->createElement('shop');  // создаем пустой блок tag
	$yml_catalog->appendChild($shop);
	$name = $dom->createElement('name', Yii::$app->params['siteName']);
	$shop->appendChild($name);

	$company = $dom->createElement('company', Yii::$app->params['company']);
	$shop->appendChild($company);

	$url = $dom->createElement('url', \yii\helpers\Url::home(true));
	$shop->appendChild($url);

//<currencies>
	$currencies = $dom->createElement('currencies');
	$shop->appendChild($currencies);
	$currency = $dom->createElement('currency');
	$currency->setAttribute('id', 'RUB');
	$currency->setAttribute('rate', 1);
	$currencies->appendChild($currency);

//<categories>
	$categories = $dom->createElement('categories');
	foreach ($cats as $cat) {
		$category = $dom->createElement('category', $cat['name']);
		$category->setAttribute('id', $cat['cat_id']);
		if($cat['parent']) $category->setAttribute('parentId', $cat['parent']);
		$categories->appendChild($category);
	}
	$shop->appendChild($categories);
	
//delivery

	$delivery = $dom->createElement('delivery-options');
	
		$option = $dom->createElement('option');
			$option->setAttribute('cost', Yii::$app->params['cost']);
			$option->setAttribute('days', Yii::$app->params['days']);
		$option->appendChild($delivery);
	$shop->appendChild($delivery);
//<offers>
	$offers = $dom->createElement('offers');
	foreach ($products as $p) {
		$offer = $dom->createElement('offer');
			$offer->setAttribute('id', $p['cat_id']);
			$offer->setAttribute('available', "true");
			$offer->setAttribute('bid', "80");
			$offer->setAttribute('cbid', "90");
				$url=$dom->createElement('url', 'url'); $offer->appendChild($url);
				$price=
				$currencyId=
				$categoryId=
				$picture=
				$delivery=$dom->createElement('delivery', 'true'); $offer->appendChild($delivery);
				$name=
				$vendor=	//производитель
				$description = 
				$manufacturer_warranty=
				$param=
						
						
						
		$offers->appendChild($offer);
	}
	$shop->appendChild($offers);	
	
	
	
	
	
//foreach ($structure as $key => $block) {
//
//	//$block = [block_name, tag, parent, data, attr]
//	
//	$element = $dom->createElement($block[1]); 	// создаем пустой блок tag
//	$fragment = $dom->createDocumentFragment();		// создаем пустой фрагмент 	
//	$fragment->appendXML($block[3]);					//в пустой фрагмент вставляем данные
//	$element->appendChild($fragment);
//
//	foreach ($block[4] as $attrtag => $attr) { //вставляем атрибуты из массива
//		$element->setAttribute($attrtag, $attr);
//	}
//	$dom->appendChild($element);// Вставляем новый элемент 
//}

	echo $dom->saveHTML(); //saveXML($dom,LIBXML_NOEMPTYTAG); 
	?>
	<!--$yml = '
	<?xml version="1.0" encoding="UTF-8"?>
	<yml_catalog date="2016-02-05 17:22">
	  <shop>
		<name>ABC</name>
		<company>ABC inc.</company>
		<url>http://www.abc.ru/</url>
		<currencies>
		  <currency id="RUR" rate="1"/>
		  <currency id="USD" rate="80"/>
		</currencies>
		<categories>
		  <category id="1278">Электроника</category>
		  <category id="3761" parentId="1278">Телевизоры</category>
		  <category id="1553" parentId="3761">Медиа-плееры</category>
		  <category id="3798">Бытовая техника</category>
		  <category id="1293" parentId="3798">Холодильники</category>
		</categories>
		<delivery-options>
		  <option cost="500" days="0" order-before="15"/>
		  <option cost="300" days="1-3"/>
		</delivery-options>
		<cpa>1</cpa>
		<offers>
		  <offer id="158" available="true" bid="80" cbid="90">
			<url>http://www.abc.ru/158.html</url>
			<price>55690</price>
			<currencyId>RUR</currencyId>
			<categoryId>1293</categoryId>
			<picture>http://www.abc.ru/1580.jpg</picture>
			<picture>http://www.abc.ru/1581.jpg</picture>
			<picture>http://www.abc.ru/1582.jpg</picture>
			<picture>http://www.abc.ru/1583.jpg</picture>
			<store>false</store>
			<delivery>true</delivery>
			<name>Смартфон Apple iPhone 6s 128gb Space Gray</name>
			<vendor>Apple</vendor>
			<model>iPhone 6s 128gb Space Gray</model>
			<description>Описание товара 1</description>
			<sales_notes>Необходима предоплата 50%</sales_notes>
			<barcode>7564756475648</barcode>
			<age>0</age>
			<manufacturer_warranty>false</manufacturer_warranty>
			<param name="Тип">моноблок</param>
			<param name="Материал">алюминий</param>
			<param name="Wi-Fi" unit="">есть</param>
			<param name="Размер экрана" unit="дюйм">27</param>
			<param name="Размер оперативной памяти" unit="Мб">4096</param>
			<param name="Объём жесткого диска" unit="Тб">1</param>
			<param name="Вес" unit="кг">13.8</param>
		  </offer>
		  <offer id="159" available="true" cbid="90">
			<url>http://www.abc.ru/159.html</url>
			<price>3045.5</price>
			<currencyId>RUR</currencyId>
			<categoryId>1293</categoryId>
			<picture>http://www.abc.ru/1590.jpg</picture>
			<picture>http://www.abc.ru/1591.jpg</picture>
			<store>false</store>
			<delivery>true</delivery>
			<name>Наушники Koss Sporta Pro</name>
			<vendor>Koss</vendor>
			<model>Sporta Pro</model>
			<description>Описание товара</description>
			<sales_notes>Покупка в день заказа</sales_notes>
			<cpa>0</cpa>
			<delivery-options>
			  <option cost="1000" days="1" order-before="15"/>
			</delivery-options>
			<barcode>7564756475648</barcode>
			<age>0</age>
			<manufacturer_warranty>true</manufacturer_warranty>
			<param name="Тип">12344</param>
			<param name="Материал">пластик</param>
			<param name="Wi-Fi" unit="">да</param>
			<param name="Размер экрана" unit="дюйм">27</param>
			<param name="Размер оперативной памяти" unit="Мб">4096</param>
			<param name="Объём жесткого диска" unit="Тб">1</param>
			<param name="Вес" unit="кг">13.8</param>
		  </offer>
		</offers>
	  </shop>
	</yml_catalog>';-->


	<a class="btn btn-default">Сформировать</a> <a class="btn btn-default">Посмотреть</a>
</div>
