<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\OrderStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-status-form">

    <?php $form = ActiveForm::begin(); ?>

   <div class="col-sm-8"> <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?></div class="col-sm-8">

    <div class="col-sm-8"><?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?></div>
	<div class="clearfix"></div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
