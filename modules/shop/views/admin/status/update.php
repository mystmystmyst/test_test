<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\OrderStatus */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Order Status',
]) . $model->status;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Order Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->status, 'url' => ['view', 'id' => $model->status_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="order-status-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
