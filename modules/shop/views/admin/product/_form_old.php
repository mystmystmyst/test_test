<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Product */
/* @var $form yii\widgets\ActiveForm */
dump($model);
?>

<div class="product-form">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#t1" aria-controls="t1" role="tab" data-toggle="tab">Основное</a></li>
		<li role="presentation"><a href="#t2" aria-controls="t2" role="tab" data-toggle="tab">Параметры</a></li>
		<li role="presentation"><a href="#t3" aria-controls="t3" role="tab" data-toggle="tab">Связанные товары</a></li>
		<li role="presentation"><a href="#t4" aria-controls="t4" role="tab" data-toggle="tab">Метатеги</a></li>
	</ul>
	<?php $form = ActiveForm::begin(); ?>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="t1">
			<?= $form->field($model, 'published')->checkbox([ '0', '1',]) ?>
			<?= $form->field($model, 'available')->checkbox([ '0', '1',]) ?>
			<?= $form->field($model, 'featured')->checkbox([ '0', '1',]) ?>
			<?= $form->field($model, 'sales')->checkbox([ '0', '1',]) ?>
			<?= $form->field($model, 'cat_id')->textInput() ?>
			<?= $form->field($model, 'vendor')->dropDownList(['ddd', 'dddr']) ?>
			<?= $form->field($model, 'host')->dropDownList(['ddd', 'dddr']) ?>

			<?= $form->field($model, 'alias')->textInput() ?>

		</div>
		<div role="tabpanel" class="tab-pane " id="t2">
			<?php //dump($model->value);
 foreach ($model->value as $value) {
	// if($value->param->type == 'html') echo $form->field($model, 'value')->textInput();//$value->param->name;
	dump($value);
 }
 unset($value);
			?>
		</div>
		<div role="tabpanel" class="tab-pane " id="t3">
			<?php
			//dump($value);
			?>
		</div>
		<div role="tabpanel" class="tab-pane " id="t4">
dfgfgdfgd
		</div>
	</div>


    <div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>
