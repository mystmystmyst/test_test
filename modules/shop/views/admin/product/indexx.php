<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\shop\helpers\AppGridHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Товары и услуги');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">
	<?php echo $this->render('_searchx', ['model' => $searchModel]); ?>
	<div class="edit-btn row">
		<div class="col-sm-5 text-right"><?= Html::a(Yii::t('app', 'Добавить'), ['create'], ['class' => 'btn btn-success btn-sm']) ?></div>
	</div>

<?= GridView::widget([
		'dataProvider' => $dataProvider,
'filterModel' => $searchModel,
		'columns' => (isset(Yii::$app->request->queryParams['templateid']))? AppGridHelper::getOptionsId(Yii::$app->request->queryParams['templateid']): [],
	]);
?>
<div>
<p>Вариант 2</p>
<div class="product-index row">
	<?php echo $this->render('_searchx', ['model' => $searchModel]); ?>
	<div class="edit-btn row">
		<div class="col-sm-5 text-right"><?= Html::a(Yii::t('app', 'Добавить'), ['create'], ['class' => 'btn btn-success btn-sm']) ?></div>
	</div>
<?php
$arr_visible = (isset(Yii::$app->request->queryParams['templateid']))? AppGridHelper::getOptionsIdVar2(Yii::$app->request->queryParams['templateid']): [];
?>
<?= GridView::widget([
		'dataProvider' => $dataProvider,
'filterModel' => $searchModel,
		'columns' => [
			'product_id',
		'published',
		'cat.name',
	['attribute'=>'description',
				'value' => 'description.value',
				'visible' => (in_array('description', $arr_visible)),
				'format' => 'html',
			],
			['attribute'=>'image',
				'value' => 'image.value',
				'visible' => (in_array('image', $arr_visible)),
				'format' => 'html',
			],
			['attribute'=>'name',
				'value' => 'name.value',
				'visible' => (in_array('name', $arr_visible)),
				'format' => 'html',
			],
			['attribute'=>'price',
				'value' => function($model) {
					$arr_value = (isset($model->price))? explode(',', $model->price->value) : [];
					return ((isset($arr_value[0])) ? $arr_value[0]. ' ' : '').(isset($arr_value[1]) ? $arr_value[1] : '');},
				'visible' => (in_array('price', $arr_visible)),
				'format' => 'html',
			],
			['attribute'=>'export',
				'value' => 'export.value',
				'visible' => (in_array('export', $arr_visible)),
				'format' => 'html',
			],
			['attribute'=>'description',
				'value' => function($model){return (isset($model->description) && isset($model->description->value))? $model->description->value: 'no param';},
				'visible' => true,
				'format' => 'html',
			],
		],
	]);
?>
</div>	
</div>
