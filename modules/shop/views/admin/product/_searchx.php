<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use app\modules\shop\helpers\AppGridHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

	<?php
	$form = ActiveForm::begin([
				'action' => ['indexx'],
				'method' => 'get',
	]);
	?>

	<div class="col-sm-3">	
		<input id="search" name="search" class="form-control input-sm" placeholder="Поиск товара"/>		
	</div>
	<div class="col-sm-2">	
	<?php
	$select_var = (empty(Yii::$app->request->queryParams['cat_id']))? [] : [Yii::$app->request->queryParams['cat_id'] => ['selected' => 'selected']];
		$cat = app\models\Category::find()->where(['type'=>1])->andwhere(['!=', 'parentcat', 0])->asarray()->all();
		echo Html::dropDownList('cat_id', 'cat_id',
				ArrayHelper::merge(['' => 'Все категории'], ArrayHelper::map($cat, 'cat_id', 'name')),
				['class'=>'form-control  input-sm', 'options' => $select_var,]
				);
	?>
	</div>
	<div class="col-sm-3">
		<?php
		$select_var = (empty(Yii::$app->request->queryParams['templateid']))? [] : [Yii::$app->request->queryParams['templateid'] => ['selected' => 'selected']];
		echo Html::dropDownList('templateid','',
			AppGridHelper::getList(),
			['class' => 'form-control  input-sm', 'options' => $select_var]
		);
		?>
	</div>


    <div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>