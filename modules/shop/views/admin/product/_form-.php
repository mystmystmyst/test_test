<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Product */
/* @var $form yii\widgets\ActiveForm */
//dump($model);


?>



<div class="product-form">
	<ul class="nav nav-tabs" role="tablist">
		<?php $active = 'active';	//устанавливаем первую вкладку активной
		foreach ($model as $group) {
			?>			
			<li class="<?= $active; ?>">
				<a href="#t<?= $group['param_id']; ?>" aria-controls="<?= $group['param_id']; ?>" role="tab" data-toggle="tab"><?= $group['name']; ?></a>
			</li>

			<?php
			$active = '';
		}
		unset($group);
		?>
	</ul>	




	<div class="tab-content">
		<?php $active = 'active';
		foreach ($model as $params) {
			?>
			<div role="tabpanel" class="tab-pane <?= $active; ?>" id="t<?= $params['param_id']; ?>">
				<?php foreach ($params['parentparams'] as $key => $p) {?>
				<form class="form-horizontal">
							<div class="form-group">
								<label for="<?= $p['name']; ?>" class="col-sm-2 control-label"><?= $p['name']; ?></label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="<?= $p['name']; ?>" value="<?= $p['value']['value']; ?>">
								</div>
								<div class="col-sm-2">
									<?= $p['frontend_type']; ?>
								</div>	
							</div>
						</form>
				<?php } unset($p); ?>	
			</div>
				<?php
				$active = '';
			}
			unset($params);
			?>
	</div>




</div>
