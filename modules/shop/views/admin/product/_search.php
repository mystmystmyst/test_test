<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\AdminProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

	<?php
	$form = ActiveForm::begin([
				'action' => ['index'],
				'method' => 'get',
	]);
	?>

	<div class="col-sm-3">	
		<input id="search" name="search" class="form-control input-sm" placeholder="Поиск товара"/>		
	</div>
	<div class="col-sm-2">	
	<?php
		echo $form->field($model, 'cat_id')->dropDownList($model->categoryLabels(), ['class'=>'form-control  input-sm'])->label(false);
	?>
	</div>
	<div class="col-sm-3">
		<?php
		echo $form->field($model, 'template')->dropDownList($model->templateLabels(), ['class' => 'form-control  input-sm'])->label(false);
		?>
	</div>


    <div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>
