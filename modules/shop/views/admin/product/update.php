<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Product */

//$this->title = Yii::t('app', 'Update {modelClass}: ', [
//			'modelClass' => 'Product',
//		]) . $model->product_id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->product_id, 'url' => ['view', 'id' => $model->product_id]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="product-update">

	<ul class="nav nav-tabs" role="tablist">
		<?php $active = 'active';	//устанавливаем первую вкладку активной
		foreach ($model as $group) {
			?>			
			<li class="<?= $active; ?>">
				<a href="#t<?= $group['param_id']; ?>" aria-controls="<?= $group['param_id']; ?>" role="tab" data-toggle="tab"><?= $group['name']; ?></a>
			</li>

			<?php
			$active = '';
		}
		unset($group);
		?>
	</ul>	




	<div class="tab-content">
		<?php $active = 'active';
		foreach ($model as $params) {
			?>
			<div role="tabpanel" class="tab-pane <?= $active; ?>" id="t<?= $params['param_id']; ?>">
				<?php foreach ($params['parentparams'] as $key => $p) {?>
				<form class="form-horizontal">
							<div class="form-group">
								<label for="<?= $p['name']; ?>" class="col-sm-2 control-label"><?= $p['name']; ?>
								<?php
									if($p['desc']) echo '<i class="glyphicon glyphicon-question-sign"  data-toggle="popover" title="Описание" data-content="'.$p['desc'].'"></i>';?>
								</label>
								<div class="col-sm-8">
									<?php												
										switch ($p['admin_type']){											
											case 'html': echo '<textarea class="form-control" id="'. $p['name']. '">'. $p['value']['value'].'"</textarea>';break;
											case 'select': echo '<select class="form-control"  id="'. $p['name']. '">'
													. '<option>1</option>'
													. '</select>';break;
											case 'radio': echo '<div class="radio">
												  <label>
													<input type="radio" value="">
													Option one is this and that
												  </label>
												</div>';break;	
											case 'checkbox': echo '<div class="radio">
												  <label>
													<input type="checkbox" value="">
													Option one is this and that
												  </label>
												</div>';break;
											default: echo '<input type="text" class="form-control" id="'. $p['name']. '" value="'. $p['value']['value'].'">';break;
										}
									
									
									?>
								</div>
								<div class="col-sm-2">
									<?= $p['admin_type']; ?>
								</div>	
							</div>
						</form>
				<?php } unset($p); ?>	
			</div>
				<?php
				$active = '';
			}
			unset($params);
			?>
	</div>


</div>
