<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\AdminProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Товары и услуги');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="product-index">
    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="edit-btn row">
        <div class="col-sm-5 text-right"><?= Html::a(Yii::t('app', 'Добавить'), ['create'], ['class' => 'btn btn-success btn-sm']) ?></div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            'product_id',
            'published',
            [
                    'attribute' => 'cat_id',
                    'value' => 'category.name',
            ],
        ], $searchModel->paramColumns()),
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
