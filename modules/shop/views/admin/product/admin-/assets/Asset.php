<?php
/**
 * @copyright Myst
 */

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

class Asset extends AssetBundle {

	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $sourcePath = '@app/modules/admin';
	public $css = [
		'css/admin.less',
	];
	public $js = [
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
		'yiister\adminlte\assets\Asset',
	];
	public $publishOptions = [
		'forceCopy' => true,
	];

}
