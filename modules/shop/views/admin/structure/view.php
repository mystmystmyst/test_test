<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\pages\models\PageStructure */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Page Structures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-structure-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->structure_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->structure_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'structure_id',
            'name:ntext',
            'structure:ntext',
        ],
    ]) ?>

</div>
