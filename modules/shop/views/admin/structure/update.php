<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\pages\models\PageStructure */

$this->title = 'Update Page Structure: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Page Structures', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->structure_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="page-structure-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
