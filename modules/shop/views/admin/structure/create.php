<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\pages\models\PageStructure */

$this->title = 'Create Page Structure';
$this->params['breadcrumbs'][] = ['label' => 'Page Structures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-structure-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
