<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Настройка модуля "Shop"');
?>
<div class="list-group col-sm-4">
	<h4 class="list-group-item list-group-item-success"><i class="glyphicon glyphicon-dashboard"></i> Настройка параметров</h4>
	<?= Html::a(Yii::t('app', 'Категории товаров'), ['/admin/shop/category/index'], ['class' => 'list-group-item']) ?>
	<?= Html::a(Yii::t('app', 'Группы параметров'), ['/admin/shop/paramsgroup/index'], ['class' => 'list-group-item']) ?>
	<?= Html::a(Yii::t('app', 'Параметры товаров'), ['/admin/shop/param/index'], ['class' => 'list-group-item']) ?>
	<?= Html::a(Yii::t('app', 'Статусы заказов'), ['/admin/shop/status/index'], ['class' => 'list-group-item']) ?>

</div>

<div class="list-group col-sm-4">
	<h4 class="list-group-item">
		Настройка экспорта</h4>

	<a href="#" class="list-group-item">Dapibus ac facilisis in</a>
	<a href="#" class="list-group-item">Morbi leo risus</a>
	<a href="#" class="list-group-item">Porta ac consectetur ac</a>
	<a href="#" class="list-group-item">Vestibulum at eros</a>
</div>

<div class="list-group col-sm-4">
	<h4 class="list-group-item">
		Настройка импорта</h4>

	<a href="#" class="list-group-item">Dapibus ac facilisis in</a>
	<a href="#" class="list-group-item">Morbi leo risus</a>
	<a href="#" class="list-group-item">Porta ac consectetur ac</a>
	<a href="#" class="list-group-item">Vestibulum at eros</a>
</div>

<div class="list-group col-sm-4">
	<h4 class="list-group-item">Скидки и купоны</h4>
	<?= Html::a(Yii::t('app', 'Управление категориями'), ['/admin/shop/category/index'], ['class' => 'list-group-item']) ?>
	<?= Html::a(Yii::t('app', 'Управление параметрами'), ['/admin/shop/param/index'], ['class' => 'list-group-item']) ?>
	<?= Html::a(Yii::t('app', 'Статусы заказов'), ['/admin/shop/status/index'], ['class' => 'list-group-item']) ?>

</div>

<div class="list-group col-sm-4">
	<h4 class="list-group-item">Способы доставки</h4>
	<?= Html::a(Yii::t('app', 'Управление категориями'), ['/admin/shop/category/index'], ['class' => 'list-group-item']) ?>
	<?= Html::a(Yii::t('app', 'Управление параметрами'), ['/admin/shop/param/index'], ['class' => 'list-group-item']) ?>
	<?= Html::a(Yii::t('app', 'Статусы заказов'), ['/admin/shop/status/index'], ['class' => 'list-group-item']) ?>

</div>

<div class="list-group col-sm-4">
	<h4 class="list-group-item">Способы оплаты</h4>
	<?= Html::a(Yii::t('app', 'Управление категориями'), ['/admin/shop/category/index'], ['class' => 'list-group-item']) ?>
	<?= Html::a(Yii::t('app', 'Управление параметрами'), ['/admin/shop/param/index'], ['class' => 'list-group-item']) ?>
	<?= Html::a(Yii::t('app', 'Статусы заказов'), ['/admin/shop/status/index'], ['class' => 'list-group-item']) ?>

</div>

<div class="list-group col-sm-4">
	<h4 class="list-group-item">Люди</h4>
	<?= Html::a(Yii::t('app', 'Постващики'), ['/admin/shop/category/index'], ['class' => 'list-group-item']) ?>
	<?= Html::a(Yii::t('app', 'Менеджеры'), ['/admin/shop/param/index'], ['class' => 'list-group-item']) ?>
	<?= Html::a(Yii::t('app', 'Покупатели'), ['/admin/shop/status/index'], ['class' => 'list-group-item']) ?>

</div>
<div class="clearfix"></div>