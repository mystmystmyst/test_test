<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\ParamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Params');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="param-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Param'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
	'layout' => "{items}\n{pager}\n{summary}\n",
        'columns' => [
           ['class' => 'yii\grid\ActionColumn',
				'template' => '{view} {link}',],

          //  'param_id',
            'name:ntext',
           // 'alias:ntext',
            'cat.name',
            'group_id',
            'parent',
            // 'description',
            // 'variants:ntext',
            'type',
            // 'unit',
            'search',
            'filter_ordering',
            'ordering',
            // 'name_visible',
            'visible',
            'link_type',

           ['class' => 'yii\grid\ActionColumn',
				'template' => '{update} {delete}',],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
