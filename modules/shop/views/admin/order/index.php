<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Заказы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">


	<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
		<?= Html::a(Yii::t('app', 'Создать заказ'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php Pjax::begin(); ?> 
	<a class="hide change_status" href=""></a>
   <?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'layout' => "{items}\n{pager}\n{summary}\n",
		'rowOptions' => function ($model) {
			if ($model->status_id == 0) {
				return [' class' => 'success'];
			}
			if ($model->status_id == 6) {
				return [' class' => 'danger'];
			}
		},
				'columns' => [
					['class' => 'yii\grid\ActionColumn','template' => '{view} {link}',],
					[
						'format' => 'raw',
						'value' => function($model, $key) {
							if ($model->status_id == 0) {
								return '<a href="index.php?r=admin/shop/order/inwork1&orderid='.$key.'" class = "inwork1 btn btn-block btn-warning btn-xs" title = "Взять заказ в работу">В работу</a>';
							}else{
								//изменить статус заказа
								return Html::dropDownList(
										'status',
										$model->status_id,
										\yii\helpers\ArrayHelper::map(app\modules\shop\models\OrderStatus::find()->all(), 'status_id', 'status'),
										['class'=>'status form-control']
										);
							}
						},
						'headerOptions' => ['style' => 'width:8%']
					],
					[
						'format' => 'raw',
						'value' => function($model) {
							//if ($model->status_id == 0) {
								return '<button class = "open_order btn btn-block btn-info btn-xs" title = "Быстрый просмотр заказа">Подробнее</button>';
							//}
						},
						'headerOptions' => ['style' => 'width:8%']
					],
					[ 'attribute' => 'order_id',
						'headerOptions' => ['style' => 'width:10px'],
						'contentOptions' => function ($model, $key, $index, $column) {
							return ['data-name' => $column->attribute];
						},
					],
					['attribute' => 'date_create', 'format' => ['datetime', 'dd.MM.y (H:m)']],
					['attribute' => 'date_update', 'format' => ['datetime', 'dd.MM.y (H:m)']],
					['attribute' => 'qty', 'label' => 'Кол.', 'headerOptions' => ['style' => 'width:10px']],
					['attribute' => 'sum', 'headerOptions' => ['style' => 'width:6%']],
					['attribute' => 'status.icon', 'format' => 'raw', 'headerOptions' => ['style' => 'width:2%']],
					['attribute' => 'initiator.name', 'label' => 'Заказчик', 'headerOptions' => ['style' => 'width:15%']],
					//['attribute' => 'initiator.name', 'label' => 'Инициатор'],
					['attribute' => 'executor.name', 'label' => 'Исполнитель', 'headerOptions' => ['style' => 'width:10px']],
					// 'order_items',
					['class' => 'yii\grid\ActionColumn',
						'template' => '{update} {delete}',
						'headerOptions' => ['style' => 'width:60px'],
					],
				],
			]);
	?>
	<?php Pjax::end(); ?></div>
