<div class="order-info row">
	<div class="col-xs-4">
		<?= '<b>Инициатор:</b> ' . $order['initiator']['username'] ?></br>
		<?= '<b>Телефон:</b> ' . $order['initiator']['tel1'] ?></br>
		<?= '<b>Email:</b> ' . $order['initiator']['email'] ?>
	</div>
	<div class="col-xs-3">
		<?= '<b>Номер заказа:</b> ' . $order['order_id'] ?></br>
		<?= '<b>Посупил:</b> ' . date("d.m.y (H:i)", strtotime($order['date_create'])); ?></br>
		<?= '<b>Изменен:</b> ' . date("d.m.y (H:i)", strtotime($order['date_update'])) ?>
	
	</div>
	<div class="col-xs-2">
		<?= '<b>Исполнитель:</b> ' . $order['executor_id'] ?></br>
		<?= '<b>Статус:</b> ' . $order['status']['status'] ?>
	</div>

	<div class="col-xs-3">
		<?= '<b>Количество позиций:</b> ' . $order['qty'] ?></br>
		<?= '<b>Общая сумма:</b> ' . $order['sum'] ?> руб.
	</div>

</div>

<table class="table table-striped table-condensed items">
	<tr>
		<th></th><th>id товара</th><th>Название</th><th>Кол-во</th><th>Сумма</th>
	</tr>	
	<?php
	$items = json_decode($order['order_items'])->cart;
	foreach ($items as $key => $item) {
		$img = \yii\helpers\Html::img('@web/images/shop/products/resized/' . $item->img, ['alt' => 'Наш логотип']);
		echo '<tr>'
		. '<td>' . $img . '<td>' . $key . '</td><td>' . $item->name . '</td><td>' . $item->qty . '</td><td>' . $item->price . ' руб.</td>'
		. '</tr>';
	}
	?>
</table>	

<a class="btn btn-sm btn-success inwork2" >В работу</a> <a class="close" title="Закрыть окно"><i class="glyphicon glyphicon-remove-circle"></i></a>
