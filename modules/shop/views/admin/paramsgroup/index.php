<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\ParamsGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Params Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="params-group-index">

<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
	<?= Html::a(Yii::t('app', 'Create Params Group'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<?php Pjax::begin(); ?>    <?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			[	'label' => 'Параметры',
				'format' => 'raw',
				'value' => function() {
					return Html::a(
							'<span class="glyphicon glyphicon-th-list"></span>', '$data->url',
							['title' => 'Список параметров группы',]
					);
				}
			],
			'name:ntext:Название группы',
			'parent_group',
			'cat_id',
			'order',
			'desc',
			'group_id',
			['class' => 'yii\grid\ActionColumn'],
			],
			]);
			?>
			<?php Pjax::end(); ?></div>
