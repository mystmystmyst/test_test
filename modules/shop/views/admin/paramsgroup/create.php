<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\ParamsGroup */

$this->title = Yii::t('app', 'Create Params Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Params Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="params-group-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
