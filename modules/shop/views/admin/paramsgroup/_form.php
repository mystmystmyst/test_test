<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\ParamsGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="params-group-form">

    <?php $form = ActiveForm::begin(); ?>
 <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'parent_group')->textInput() ?>

   <?= $form->field($model, 'desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order')->textInput() ?>

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
