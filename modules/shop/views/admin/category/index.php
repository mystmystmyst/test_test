<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

	<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <div class="edit-btn">
		<?= Html::a(Yii::t('app', 'Создать новую'), ['create'], ['class' => 'btn btn-success']) ?>
    </div>
	<div class="clearfix"></div>
	<?php Pjax::begin(); ?>    <?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'layout' => "{items}\n{pager}\n{summary}\n",
		'columns' => [
			['class' => 'yii\grid\ActionColumn',
				'template' => '{view} {link}',],
			'published',
			'name:ntext',
			// 'type',
			'parentcat',
			//'template_structure:ntext',
			// 'cat_structure:ntext',
			// 'page_structure:ntext',
			// 'item_structure:ntext',
			// 'admin_structure',
			'widgetset_id',
			'cat_id',
			// 'url:url',
			// 'name_visible',
			['class' => 'yii\grid\ActionColumn',
				'template' => '{update} {delete}',],
		],
	]);
	?>
	<?php Pjax::end(); ?></div>
