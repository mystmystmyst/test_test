<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cat_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'template_structure') ?>

    <?= $form->field($model, 'cat_structure') ?>

    <?php // echo $form->field($model, 'page_structure') ?>

    <?php // echo $form->field($model, 'item_structure') ?>

    <?php // echo $form->field($model, 'admin_structure') ?>

    <?php // echo $form->field($model, 'widgetset_id') ?>

    <?php // echo $form->field($model, 'published') ?>

    <?php // echo $form->field($model, 'parentcat') ?>

    <?php // echo $form->field($model, 'url') ?>

    <?php // echo $form->field($model, 'name_visible') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
