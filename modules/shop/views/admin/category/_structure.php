<?php
use app\modules\pages\helpers\PrepareHtml;
/* @var $this yii\web\View */
/* @var $model app\modules\pages\models\Pagecategory */
?>

<?php
//СЧИТЫВАЕМ СТРУКТУРУ СТРАНИЦЫ

// 				[blockname, tag, parent, data, attr]
$structure = json_decode($model->structure);//через связь getCat в модели Page

//PrepareHtml::vardump($structure);exit;

// // ЗАПОЛНЕНИЕ БЛОКОВ СТРУКТУРЫ ИМЕНАМИ БЛОКОВ
// foreach ($structure as $position => &$block) { //проходим по всем блокам структуры
// 
// 	foreach ($data['pageValues'] as &$d) {	// Ищем данные для этого блока в массиве data		
// 		
// 		if ($d['position'] == $position)	{		//если данные принадлежат текущему блоку
// 			//структура:   [blockname, tag, parent, data, attr]
// 			$block[3] .= $d['value'];//добавляем их к массиву структуры
// 			//print_r($structure[$key][2]);echo '<br>';
// 		}
// 	}
// }
// unset($block);unset($d);

// После того как структура с даннными объеденены, выводим всё в DOM
$dom = new DOMDocument();
$dom->formatOutput = true;

/**
	* @block	array	[blockname, tag, parent, data, attr]
	* 
	*/

foreach ($structure as $block) {

	//$block = [block_name, tag, parent, data, attr]
	
	$element = $dom->createElement($block[1]); 			//	создаем пустой блок tag
	$fragment = $dom->createDocumentFragment();			// 	создаем пустой фрагмент 	
	$fragment->appendXML('<span>'.$block[0].'</span>');	//	в пустой фрагмент вставляем имя блока
	$element->appendChild($fragment);

	foreach ($block[4] as $attrtag => $attr) { //вставляем стили и атрибуты из массива
		$element->setAttribute($attrtag, $attr);
		$element->setAttribute('style', 'border:1px solid red;padding:20px');
	}
	$dom->appendChild($element);// Вставляем новый элемент 
}

echo $dom->saveHTML();//saveXML($dom,LIBXML_NOEMPTYTAG); 
?>