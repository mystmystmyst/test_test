<?php
use app\modules\pages\helpers\PrepareHtml;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model app\modules\pages\models\Pagecategory */
?>

<?php
$this->registerJsFile('@web/js/pages.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

//PrepareHtml::vardump($model->params);exit;
?>
<div>
<p></p>
	<span class="btn btn-success btn-xs" id="addvalue1"><i class="glyphicon glyphicon-plus"></i>Добавить новый</span>
</div>
<table class="table table-striped table-condensed table-hover">
<tr>

	<th></th><th>Имя параметра</th><th>Тип параметра</th><th>Варианты параметра</th><th>Участвует в поиске?</th><th>Ordering</th><th>visible</th>
</tr>

<?php
foreach ($model->params as $p) {
	echo '
		<tr>
		<td><a class="glyphicon glyphicon-trash" id="delete"></a></td>	
		';
	echo '
		
		<td><input class="form-control" value="'.$p->name.'" name="name" id="name"/></td>	
		';
	echo '
		<td>
		<input class="form-control" value="'.$p->type.'" name="type" id="type"/>
		</td>';
	echo '
		<td>
		<input class="form-control" value="'.$p->variants.'" name="variants" id="variants"/>
		</td>';
	echo '
		<td>
		<input class="form-control" value="'.$p->search.'" name="search" id="search"/>
		</td>';
	echo '
		<td>
		<input class="form-control" value="'.$p->ordering.'" name="ordering" id="ordering"/>
		</td>';
	echo '
		<td>
		<input class="form-control" value="'.$p->visible.'" name="visible" id="visible"/>
		</td></tr>';

}
?>
</table>
















