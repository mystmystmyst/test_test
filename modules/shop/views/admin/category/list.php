<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\pages\models\PagecategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории материалов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pagecategory-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новая категория', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'cat_id',
            'name:ntext',
            'parentcat',
            'meta_title:ntext',
            'meta_desc:ntext',
            // 'meta_key:ntext',
            // 'intro:ntext',
            // 'full:ntext',
            // 'public',

            ['class' => 'yii\grid\ActionColumn',
// 				//'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}',
// 				'urlCreator'=>function($action, $model, $key, $index){
// 					return \yii\helpers\Url::to([$action.'cat','id'=>$model->cat_id]);
			//	}
            ],
        ],
    ]); ?>

</div>
