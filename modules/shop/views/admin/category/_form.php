<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="col-sm-5"><?= $form->field($model, 'name')->textInput()->label('Название') ?></div>
	<div class="col-sm-5"><?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?></div>
	<div class="clearfix"></div>

	<div class="col-sm-5"><?= $form->field($model, 'parentcat')->dropDownList(ArrayHelper::map(\app\models\Category::find()->all(), 'cat_id', 'name')) ?></div>
	<div class="clearfix"></div>	
	<h4>Группы параметров категории</h4>
	<button class="btn btn-success btn-sm">+ Добавить группу</button>
		<?php
	$dataProvider = new \yii\data\ActiveDataProvider([
		'query' => $ParamsGroup,
		'pagination' => [
			'pageSize' => 12,
		],
	]);
	echo yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		//'filterModel' => $searchModel,
		'layout' => "{items}\n{pager}\n{summary}\n",
		'columns' => [
//			['class' => 'yii\grid\ActionColumn',
//				'template' => '{view} {link}',],
			'group_id',
			'name:ntext:Название группы',
			'parent_group',
			'cat_id',
			'order',
			'desc',
			['class' => 'yii\grid\ActionColumn',
				'template' => '{update} {delete}',],
		],
	]);
	?>
	
	<div class="clearfix"></div>	
</div>



<div class="form-group">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
