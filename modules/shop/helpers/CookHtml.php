<?php

namespace app\modules\shop\helpers;

//use yii\helpers\BaseHtml;
use yii\helpers\Html;
//use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class CookHtml {

	function getMeta($name, $value) {
		\Yii::$app->view->registerMetaTag([
			'name' => $name,
			'content' => $value
		]);

		return false;
	}

	function getHtml($var) {
		return Html::decode($var);
	}

	function getSelect($name, $value, $vars) {
		$out = '<div class="form-group"><label class="col-sm-2 control-label">' . $name . '</label>';
		$out .= '</div>';
		$out .= Html::dropDownList(
						$name, [], $value, //['value1' =>'value1','value2' =>'value2', 'value3' =>'value3'],
						[
					'class' => 'eee',
						//'value2' => ['label' => 'value 2'],
		]);
		return $out;
	}

	/*
	 * [0][
	 * 	'src','alt'
	 * 	]
	 * [1][
	 * 	'src','alt'
	 * 	]
	 */

	function getImg($var, $option = '') {
		$out = '';
		$var = json_decode($var, true); //массив картинок
		$alt = 'Изображение товара';
		switch ($option) {
			case 'cat': //вывод в категории
				$out = '<div class="main-image"><img src="images/shop/products/resized/' . $var[0]['src'] . '" alt="' . $alt . '" /></div>';

				break;

			default:
				isset($var[0]['alt']) ? $alt = $var[0]['alt'] : $alt = '';
				$out = '<div class="main-image"><img src="images/shop/products/' . $var[0]['src'] . '" alt="' . $alt . '" /></div>';
				//unset($var[0]);
				if ($var)
					$out .= '<div class="thumbs">';
				foreach ($var as $v) {
					(isset($v['alt'])) ? $alt = $v['alt'] : $alt = '';
					$out .= '<img src="images/shop/products/resized/' . $v['src'] . '" alt="' . $alt . '" data-src="' . $v['src'] . '"/>';
				}
				unset($v);
				if ($var)
					$out .= '</div>';
		}


		return $out;
	}

	/*
	 * {"BBK":{"id":20,"img":"BBK.jpg"},.....}
	 * 'name':
	 * 'id с торг маил
	 * 'desc'
	 * 'img'
	 */

	function getMf($var) {

		$var = json_decode($var, true);
		$value = array_keys($var)[0];
		if (isset($var[$value]['img'])) {
			$out = '<img src="images/shop/mf/' . $var[$value]['img'] . '" alt="' . $value . '" />';
		} else {
			$out = '<span class="mf">' . $value . '</span>';
		}


		return $out;
	}

//function getImg($var){
//	$out = Html::decode($var);
//	return $out;
//}

	function getRadio($var) {
		$out = Html::decode($var);
		return $out;
	}

	function getColor($var) {
		$out = Html::decode($var);
		return $out;
	}

	function getCheck($var) {
		$out = Html::decode($var);
		return $out;
	}

	function getRating($var) {
		$var = round($var, 0, PHP_ROUND_HALF_UP);
		$out = '';
		for ($i = 0; $i < 5; $i++) {
			if ($var > 0) {
				$out .= '<i class="glyphicon glyphicon-star"></i> ';
			} else {
				$out .= '<i class="glyphicon glyphicon-star-empty"></i> ';
			}
			$var--;
		}

		return $out;
	}

	/*
	 * [33000,"руб.","RUB"]
	 */

	function getPrice($var) {
		$var = json_decode($var);
		//dump($var);exit;
		$out = $var[0] . ' <span>' . $var[1] . '</span>';
		return $out;
	}

	function getCart($var) {//dump($var);exit;
		$out = '<a href="' . Url::to(['/shop/cart/add', 'id' => $var]) . '" class="addtocart" data-product_id="' . $var . '">заказать</a>';
//		'<a'
//				. ' href="/cart/default/add"'
//				. ' data-page_id="'.$var['page_id'].'" '
//				. 'id="addtocart">'
//				.$var['param']['name'].'</a>';

		return $out;
	}

//	function getSku($var) {
//		$out = 'Артикул: <span>' . $var . '</span>';
//		return $out;
//	}

	/* СТОИМ БЛОКИ ПАРАМЕТРОВ НА СТРАНИЦЕ (строим view)
	 * @var $pages - данные страницы,
	 * @var $structure- структура страницы
	 */
	function getAllBlocks($pages, $structure) {
		$out = '';
		//dump($pages);exit;
		foreach ($pages as $page) {  //цикл обхода всех страниц категории
			//dump($structure);exit;	
			$str = $structure;
			$out .='<div class="page-item">';
			//ПОДГОТАВЛИВАЕМ ДАННЫЕ в HTML . ключ массива - param_id
			foreach ($page['value'] as &$v) {  //цикл обхода value одной страницы
				//dump($page['page_id']);//exit;
				//Обработка данных в соответствии с типом данных
				switch ($v['param']['type']) {
					//case 'main_img':		$v['value'] = CookHtml::getMainImg($v['value']);break;
					case 'img': $v['value'] = CookHtml::getImg($v['value']);
						break;
					case 'price1': $v['value'] = CookHtml::getPrice($v['value']);
						break;
					case 'select': $v['value'] = CookHtml::getSelect($v['param']['name'], $v['value'], json_decode($v['param']['variants']));
						break;
					case 'radio': $v['value'] = CookHtml::getRadio($v['value']);
						break;
					case 'color': $v['value'] = CookHtml::getColor($v['value']);
						break;
					case 'check': $v['value'] = CookHtml::getCheck($v['value']);
						break;
					case 'sku': $v['value'] = CookHtml::getSku($v['value']);
						break;
					case 'rating': $v['value'] = CookHtml::getRating(json_decode($v['value']));
						break;
					case 'cart': $v['value'] = CookHtml::getCart($v);
						break;
					case 'mf': $v['value'] = CookHtml::getMf($v);
						break;
					default: $v['value'] = CookHtml::getHtml($v['value']);
				}

				// Если параметр является ссылкой, добавляем ее
				switch ($v['param']['link_type']) {
					case 'page': //ссылка на страницу
						$v['value'] = '<a href="' . Url::to(['product/view', 'id' => $page['product_id']]) . '">' . $v['value'] . '</a>';
						break;

					case 'filter':  //ссылка на стр. Результатов поиска по параметру
						$v['value'] = '<a href="' . Url::to(['/shop/category/view', 'id' => 1, 'param_id' => $v['param']['param_id']]) . '">' . $v['value'] . '</a>';
						break;
				}
				//доп. структура для параметров, если имя видимое то показываем его
				if ($v['param']['name_visible']) {
					$v['value'] = '<span class="param-label">' .
							$v['param']['name'] .
							'</span>' .
							'<span class="param-body">' .
							$v['value'] .
							'</span>';
				} else {
					$v['value'] = '<span class="param-body">' .
							$v['value'] .
							'</span>';
				}
				//если параметр невидимый добавляем блок hidden
				if (!$v['param']['visible']) {
					$v['value'] = '<span class="hidden">' . $v['value'] . '</span>';
				}
			}
			unset($v);

// ЗАПОЛНЕНИЕ БЛОКОВ СТРУКТУРЫ ДАННЫМИ
			foreach ($page['value'] as $v) {
				$str[$v['param_id']][2] = $v['value'];
			}
			unset($v);

			foreach ($str as $block) {
//dump($str);exit;
				//$block = [tag, parent, data, attr]
				$out .= '<' . $block[0];
				foreach ($block[3] as $attrtag => $attr) {   //вставляем атрибуты из массива
					$out .= ' ' . $attrtag . '="' . $attr . '"';
				}
				unset($attr);
				$out .= '> ';
				if ($block[2]) {
					$out .=$block[2];   //вставляем данные
				}
				$out .='</' . $block[0] . '>';
			}
			unset($block); //dump($page);//exit;
			$out .='</div>';
		}
		unset($page);
		return $out;
	}

	//не используется ФОРМИРУЕМ ШАБЛОН (строим layout)
	function getTemplate($template_structure, $content, $widgets) {
		//dump($template_structure);exit;
		//собираем виджеты
		$wHtml = CookHtml::getWidget($widgets); // массив виджетов [position]->[виджет-html в позиции]
		//dump($wHtml);exit;
		$out = '<div class="container">';
		//строим структуру шаблона
		foreach ($template_structure as $key => &$block) {
			//dump($widgets[$key]);exit;
			// если существует виджет в позиции то вставляем его туда
			if (array_key_exists($key, $wHtml)) {
				$block[2] = $wHtml[$key];
				//dump($template_structure);exit;
			}
			//dump($block);exit;
			$out .= '<' . $block[0]; //строим тег
			foreach ($block[3] as $attrtag => $attr) {   //вставляем атрибуты из массива
				$out .= ' ' . $attrtag . '="' . $attr . '"';
			}
			unset($attr);
			$out .= '><div class="row">'; // закрываем начало тега
			//вставляем контент блока
			if ($block[2] == 'content') {
				$out .=$content;
			} else {
				//иначе вставляем виджеты
				$out .=$block[2];
			}

			$out .='</div></' . $block[0] . '>'; // закр тег
		}
		unset($block); //dump($out);//exit;
		$out .='</div>';
		return $out;
	}

	/* ФОРМИРУЕМ ВСЕ ВИДЖЕТЫ НА СТРАНИЦЕ
	 * возвращает массив виджетов
	 */

	function getWidget($widgets) {
		$out = [];
		foreach ($widgets as $w) {
			//dump($w);exit;
			// начальный тег
			if (isset($out[$w['position']])) {
				$out[$w['position']] .='<div class="w-' . $w['widget_id'] . ' ' . $w['bootstrap'] . '">';
			} else {
				$out[$w['position']] = '<div class="w-' . $w['widget_id'] . ' ' . $w['bootstrap'] . '">';
			}
			//вывод названия виджета, если видимо и если есть
			if (isset($w['widget'][0]['name']) and $w['widget'][0]['name_visible'] == 1) {
				$out[$w['position']] .='<h3 class="w-title">' . $w['widget'][0]['name'] . '</h3>';
			}
//вызываем виджет и строим его хтмл
			$html = '<div class="w-body">';
			$params['widget_id'] = $w['widget'][0]['widget_id'];
			$params['params'] = $w['widget'][0]['params'];
			//dump($params);exit;
			$comand = '$html .= \\app\\components\\' . $w['widget'][0]['type'] . '\\' . $w['widget'][0]['type'] . '::widget($params);';
			eval($comand); //dump($w['widget']['name']);exit;
			$html .='</div>';

			// присваиваем хтмл виджета в массив в нужную позицию
			$out[$w['position']] .=$html;
			$out[$w['position']] .='</div>';
		}

		unset($w);
		return $out;
	}

//	/**
//	 * вывод группы параметров на страницу
//	 * @param $group_id -ID группы параметров
//	 * @param $params - массив параметров страницы + $product_id
//	 * @return html
//	 */
//	function getGroup($group_id, $params) {
//		$out = '';
//		$product_id = $params['product_id'];
//		unset($params['product_id']); //очищаем список параметров от лишнего
//		dump($params);
////		exit;
//		foreach ($params as $param) {
//			if ($param['group_id'] == $group_id) { // Если параметр принадлежит позиции $group_id
//				$html_item = '';   //
//				//Обработка данных в соответствии с типом данных
//				switch ($param['type']) {
//					//case 'main_img':		$v['value'] = CookHtml::getMainImg($v['value']);break;
//					case 'meta': $html_item = CookHtml::getMeta($param['value'][0]['value']);
//						break;
//					case 'img': $html_item = CookHtml::getImg($param['value'][0]['value']);
//						break;
//					case 'price': $html_item = CookHtml::getPrice($param['value'][0]['value']);
//						break;
//					case 'select': $html_item = CookHtml::getSelect($v['param']['name'], $v['value'], json_decode($v['param']['variants']));
//						break;
//					case 'radio': $html_item = CookHtml::getRadio($param['value'][0]['value']);
//						break;
//					case 'color': $html_item = CookHtml::getColor($param['value'][0]['value']);
//						break;
//					case 'check': $html_item = CookHtml::getCheck($param['value'][0]['value']);
//						break;
//					case 'sku': $html_item = CookHtml::getSku($param['value'][0]['value']);
//						break;
//					case 'rating':$html_item = CookHtml::getRating(json_decode($v['value']));
//						break;
//					case 'cart': $html_item = CookHtml::getCart($product_id);
//						break;
//					default: $html_item = CookHtml::getHtml($param['value'][0]['value']);
//				}
//
//				// Если параметр является ссылкой, добавляем ее
//				switch ($param['link_type']) {
//					case 'page': //ссылка на страницу
//						$html_item = '<a href="' . Url::to(['product/view', 'id' => $product_id]) . '">' . $html_item . '</a>';
//						break;
//
//					case 'filter':  //ссылка на стр. Результатов поиска по параметру
//						$html_item = '<a href="' . Url::to(['/shop/category/view', 'id' => $product_id, 'param_id' => $param['param_id']]) . '">' . $html_item . '</a>';
//						break;
//				}
//				//доп. структура для параметров, если имя видимое то показываем его
//				if ($param['name_visible']) {
//					$html_item = '<span class="param-label">' .
//							$param['name'] .
//							'</span>' .
//							'<span class="param-body">' .
//							$html_item .
//							'</span>';
//				} else {
//					$html_item = '<span class="param-body">' .
//							$html_item .
//							'</span>';
//				}
//				$class = '';
//				//добавляем класс в обертку
//				if ($param['class']) {
//					$class = $param['class'];
//				}
//
//				//если параметр невидимый добавляем класс hidden
//				if (!$param['visible']) {
//					$class = $class . ' hidden';
//				}
//				if ($class)
//					$class = ' class="' . $class . '"';
//				//тег обертка
//				if ($param['tag']) {
//					$html_item = '<' . $param['tag'] . $class . '>' . $html_item . '</' . $param['tag'] . '>';
//				} else {
//					$html_item = '<div' . $class . '>' . $html_item . '</div>';
//				}
//
//				$out .= $html_item;
//			}
//		}
//		unset($param);
//		//$out = '<span>' . Html::decode($var) . '</span>';
//		return $out;
//	}
//
//	/**
//	 * вывод группы параметров на страницу или item категории
//	 * @param $group_id -ID группы параметров
//	 * @param $params - массив параметров страницы + $product_id
//	 * @return html
//	 */
//	function getGroup1($group_id, $product) {
//		$out = '';
//		//$product_id =$params['product_id']; unset($params['product_id']); //очищаем список параметров от лишнего
////		dump($products);
////		exit;
//		//echo $product['alias'], '<br>';
////dump($product);
//		//exit;
//		//обработка по порядку всех параметров
//		foreach ($product['value'] as $value) {
//			//dump($value);exit;
//			if ($value['param']['group_id'] == $group_id) { // Если параметр принадлежит позиции $group_id
//				$html_item = '';   //
//				//Обработка данных в соответствии с типом данных
//				switch ($value['param']['type']) {
//
//					//case 'main_img':		$v['value'] = CookHtml::getMainImg($v['value']);break;
//					case 'meta': $html_item = CookHtml::getMeta($value['param']['name'], $value['value']);
//						break;
//					case 'img': $html_item = CookHtml::getImg($value['value']);
//						break;
//					case 'price': $html_item = CookHtml::getPrice($value['value']);
//						break;
//					case 'select': $html_item = CookHtml::getSelect($v['param']['name'], $v['value'], json_decode($v['param']['variants']));
//						break;
//					case 'radio': $html_item = CookHtml::getRadio($value['value']);
//						break;
//					case 'color': $html_item = CookHtml::getColor($value['value']);
//						break;
//					case 'check': $html_item = CookHtml::getCheck($value['value']);
//						break;
//					case 'sku': $html_item = CookHtml::getSku($value['value']);
//						break;
//					case 'rating':$html_item = CookHtml::getRating(json_decode($v['value']));
//						break;
//					case 'cart': $html_item = CookHtml::getCart($product_id);
//						break;
//					default: $html_item = CookHtml::getHtml($value['value']);
//				}
//
//				// Если параметр является ссылкой, добавляем ее
//				switch ($value['param']['link_type']) {
//					case 'page': //ссылка на страницу
//						$html_item = '<a href="' . Url::to(['product/view', 'id' => $product['product_id']]) . '">' . $html_item . '</a>';
//						break;
//
//					case 'filter':  //ссылка на стр. Результатов поиска по параметру
//						$html_item = '<a href="' . Url::to(['/shop/category/view', 'id' => $value['param']['param_id'], 'param_id' => $value['param']['param_id']]) . '">' . $html_item . '</a>';
//						break;
//				}
//				//доп. структура для параметров, если имя видимое то показываем его
//				if ($value['param']['name_visible']) {
//					$html_item = '<span class="param-label">' .
//							$value['param']['name'] .
//							'</span>' .
//							'<span class="param-body">' .
//							$html_item .
//							'</span>';
//				} else {
//					$html_item = '<span class="param-body">' .
//							$html_item .
//							'</span>';
//				}
//				$class = '';
//				//добавляем класс в обертку
//				if ($value['param']['class']) {
//					$class = $value['param']['class'];
//				}
//
//				//если параметр невидимый добавляем класс hidden
//				if (!$value['param']['visible']) {
//					$class = $class . ' hidden';
//				}
//				if ($class)
//					$class = ' class="' . $class . '"';
//				//тег обертка
//				if ($value['param']['tag']) {
//					$html_item = '<' . $value['param']['tag'] . $class . '>' . $html_item . '</' . $value['param']['tag'] . '>';
//				} else {
//					$html_item = '<div' . $class . '>' . $html_item . '</div>';
//				}
//
//				$out .= $html_item;
//			}
//		}
//		unset($product);
//		//$out = '<span>' . Html::decode($var) . '</span>';
//		return $out;
//
//		unset($value);
//	}

	/**
	 * вывод группы параметров по названию группы на страницу или item категории
	 * @param $product 
	 * @param $group - название группы параметров
	 * @return html
	 */
	function getParams($product, $group) {
		$out = '';
//dump($product);
		//ищем id группы
		$group_id = \app\modules\shop\models\Param::find()
						->where(['cat_id' => $product['cat']['cat_id']])
						->orwhere(['cat_id' => 0])
						->andwhere(['name' => $group])->asarray()->one();
		//dump($group_id);
//	foreach ($product['value'] as $value) {
//		if($value['value'] == $group){
//			$group_id=$value['param_id'];
//			$out .= CookHtml::getHtmlParam($value);  //обрабатываем
//			//$out .= '<'.$value['param']['tag'].'>'.$value['param']['name'].'</'.$value['param']['tag'].'>';
//			break;
//		}
//	}	unset($value);
		if (!isset($group_id))
			return false;
		$out .='<h5 class="group_name">' . $group_id['name'] . '</h5>';
//ищем параметры группы
		foreach ($product['value'] as $value) {
			//dump($value);

			if ($value['param']['parent'] == $group_id['param_id']) { // Если параметр принадлежит группе $group - выводим
				$out .= CookHtml::getHtmlParam($value);  //обрабатываем
			}
		}
		return $out;
	}

	/**
	 *  выввод одного параметра в html
	 * @param array $product - унифицированный массив товара
	 * @param type $param -параметр для вывода
	 * @param type $option
	 * @return type	html
	 * 
	 */
	function getParam($product, $param, $option = '') {
		$out = '';
		//dump($param);
		foreach ($product['value'] as $p) {
			if ($p['param']['name'] == $param) {
				$out .=CookHtml::getHtmlParam($p, $option);  //обрабатываем		
			}
		}
		unset($p);
		//обход групп
//foreach ($product['group'] as $group) {
//		//поиск параметра в группе
//			foreach ($group['param'] as $p) {
//				if ($p['name'] == $param) {
//					$out .=CookHtml::getHtmlParam($p, $option);  //обрабатываем		
//				}
//			}
//			unset($p);
//		}unset($group);
		return $out;
	}

	/**
	 * возвращает html одного параметра на основании типа параметра
	 */
	function getHtmlParam($param, $option = '') {
		//dump($param);
		$html_item = '';
		//Обработка данных в соответствии с типом данных
		switch ($param['param']['frontend_type']) {
			//case 'main_img':		$v['value'] = CookHtml::getMainImg($v['value']);break;
			case 'meta': $html_item = CookHtml::getMeta($param['value']);
				break;
			case 'img': $html_item = CookHtml::getImg($param['value'], $option);
				break;
			case 'price': $html_item = CookHtml::getPrice($param['value']);
				break;
//			case 'select': $html_item = CookHtml::getSelect($v['param']['name'], $v['value'], json_decode($v['param']['variants']));
//				break;
			case 'radio': $html_item = CookHtml::getRadio($param['value']);
				break;
			case 'color': $html_item = CookHtml::getColor($param['value']);
				break;
			case 'check': $html_item = CookHtml::getCheck($param['value']);
				break;
//			case 'sku': $html_item = CookHtml::getSku($param['value']['value']);
//				break;
			case 'rating':$html_item = CookHtml::getRating(json_decode($param['value']));
				break;
			case 'cart': $html_item = CookHtml::getCart($param['product_id']);
				break;
			case 'mf': $html_item = CookHtml::getMf($param['value']);
				break;
			default: $html_item = CookHtml::getHtml($param['value']);
		}

		// Если параметр является ссылкой, добавляем ее
		switch ($param['param']['link_type']) {
			case 'page': //ссылка на страницу
				$html_item = '<a href="' . Url::to(['product/view', 'id' => $param['product_id']]) . '">' . $html_item . '</a>';
				break;

			case 'filter':  //ссылка на стр. Результатов поиска по параметру
				$html_item = '<a href="' . Url::to(['/shop/category/view', 'id' => $param['product_id'], 'param_id' => $param['param_id']]) . '">' . $html_item . '</a>';
				break;
		}
		//доп. структура для параметров, если имя видимое то показываем его
		if ($param['param']['name_visible']) {
			$html_item = '<span class="param-label">' .
					$param['param']['name'] .
					'</span>' .
					'<span class="param-body">' .
					$html_item .
					'</span>';
		} else {
			$html_item = '<span class="param-body">' .
					$html_item .
					'</span>';
		}
		$class = '';
		//добавляем класс в обертку
		if ($param['param']['class']) {
			$class = $param['param']['class'];
		}

		//если параметр невидимый добавляем класс hidden
		if (!$param['param']['visible']) {
			$class = 'param ' . $class . ' hidden';
		}
		$class ? $class = ' class="param  ' . $class . '"' : $class = ' class="param"';
		//тег обертка
		if ($param['param']['tag']) {
			$html_item = '<' . $param['param']['tag'] . $class . '>' . $html_item . '</' . $param['param']['tag'] . '>';
		} else {
			$html_item = '<div' . $class . '>' . $html_item . '</div>';
		}

		return $html_item;
	}

	function getComments($product_id) {
		$out = '';
		$comm = \app\modules\shop\models\Comment::find()->where(['product_id' => $product_id])->asarray()->all();
//dump($comm);exit;
		foreach ($comm as $c) {
			$out .='<div class="comment"><span class="author"> <i class="glyphicon glyphicon-user"></i> ' . $c['author'] . '</span>';
			$out .='<span class="">' . $c['date_create'] . '</span>';
			$out .='<span class="c1"><span class="small">Согласны <span class="label label-success">' . $c['agree'] . '</span> </span>';
			$out .='<span class="small">Не согласны <span class="label label-danger">' . $c['reject'] . '</span> </span>';
			$out .='<span class="small">Всего  <span class="label label-default">' . $c['grade'] . '</span></span></span>';
			$out .='<dl>
						<dt>Достоинтсва</dt>
						<dd>' . $c['plus'] . '</dd></dl>';
			$out .='<dl>
						<dt>Недостатки</dt>
						<dd>' . $c['minus'] . '</dd></dl>';
			$out .='<dl>
						<dt>Комментарий</dt>
						<dd>' . $c['comment'] . '</dd></dl></div>';
		}
		unset($param);
		return $out;
	}

}
