<?php

namespace app\modules\shop\helpers;

class AppGridHelper {

	protected static $arr_properties = [
		'one' => [
			'name' => 'one_config',
			'field' => ['description' => 2, 'price' => 5, 'export' => 24],
			'options' => [
		        'product_id',
		        'published',
		        'cat.name',
	            ['attribute'=>'description',
    				'value' => 'description.value',
    				'format' => 'html',
    			],
    			['attribute'=>'price',
    				'value' => 'price.value',
    				'format' => 'html',
    			],
    			['attribute'=>'export',
    				'value' => 'export.value',
    				'format' => 'html',
    			],
		    ],
		],
		'two' => [
			'name' => 'two_config',
			'field' => ['name' => 29, 'price' => 5, 'export' => 24],
			'options' => [
		        'product_id',
		        'published',
		        'cat.name',
	            ['attribute'=>'name',
    				'value' => 'name.value',
    				'format' => 'html',
    			],
    			['attribute'=>'price',
    				'value' => 'price.value',
    				'format' => 'html',
    			],
    			['attribute'=>'export',
    				'value' => 'export.value',
    				'format' => 'html',
    			],
		    ],
	    ],
	    'three' => [
			'name' => 'three_config',
			'field' => ['name' => 29, 'price' => 5],
			'options' => [
		        'product_id',
		        'published',
		        'cat.name',
	            ['attribute'=>'name',
    				'value' => 'name.value',
    				'format' => 'html',
    			],
    			['attribute'=>'price',
    				'value' => 'price.value',
    				'format' => 'html',
    			],
		    ],
	    ],	
		'four' => [
			'name' => 'four_config',
			'field' => ['image' => 3, 'name' => 29, 'price' => 5],
			'options' => [
		        'product_id',
		        'published',
		        'cat.name',
		        ['attribute'=>'image',
    				'value' => 'image.value',
    				'format' => 'html',
    			],
	            ['attribute'=>'name',
    				'value' => 'name.value',
    				'format' => 'html',
    			],
    			['attribute'=>'price',
    				'value' => 'price.value',
    				'format' => 'html',
    			],
		    ],
		],
	];

	protected static $arr_properties_var2 = [
		'one' => ['description', 'price', 'export'],
		'two' => ['name', 'price', 'export'],
		'three' => ['name', 'price'],
		'four' => ['image', 'name', 'price'],
		];

	public static function getOptionsId($id) {
		return (isset(self::$arr_properties[$id])) ? self::$arr_properties[$id]['options'] : [];
	}

	public static function getOptionsName($name) {
		foreach (self::$arr_properties as $value) {
			if (is_array($value) && $value['name'] === $name) {
				return $value['options'];
			}
		}
		return [];
	}

	public static function getFieldId($id) {
		return (isset(self::$arr_properties[$id])) ? self::$arr_properties[$id]['field'] : [];
	}

	public static function getList() {
		$return = [];
		foreach (self::$arr_properties as $key => $value) {
			$return[$key] = $value['name'];
		}
		return $return;
	}

	public static function getOptionsIdVar2($id) {
		return (isset(self::$arr_properties_var2[$id])) ? self::$arr_properties_var2[$id] : [];
	}

}