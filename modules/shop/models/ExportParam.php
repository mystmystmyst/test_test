<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "shop_export_param".
 *
 * @property integer $export_id
 * @property integer $param_id
 */
class ExportParam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_export_param';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['export_id', 'param_id'], 'required'],
            [['export_id', 'param_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'export_id' => Yii::t('app', 'Export ID'),
            'param_id' => Yii::t('app', 'Param ID'),
        ];
    }
	
public function getExport()
    {
        return $this->hasOne(Export::className(), ['id' => 'export_id']);
    }
}
