<?php

namespace app\modules\shop\models;

//use Yii;

/**
 * 
 *
 */
class Cart extends \yii\db\ActiveRecord{
	
	//добавление продукта в корзину
	public function addToCart($product, $product_id, $qty=1){
		//проверка на существование товара в корзине(массиве $session)
	//dump($product);
		if(isset($_SESSION['cart'][$product_id])){
			$_SESSION['cart'][$product_id]['qty'] += $qty; //обновляем количество этого товара
		}else{									//добавляем в масив корзины
			$_SESSION['cart'][$product_id] =[
				'qty'=>$qty,
				'name'=>$product[29]['value'],
				'price'=>json_decode($product[5]['value'])[0],
				'img' => json_decode($product[3]['value'],true)[0]['src']
			];
		}
		//проверяем общ количество и или кладем или добавляем
		$_SESSION['qty'] = isset($_SESSION['qty'] ) ? 
				$_SESSION['qty'] + $qty : 
				$qty;
		//dump($_SESSION['cart']);
		
		// Проверяем сумму и 
		$_SESSION['sum'] = isset($_SESSION['sum']) ? 
			$_SESSION['sum'] + $qty *  json_decode($product[5]['value'])[0]:  //добавляем
			$qty *  json_decode($product[5]['value'])[0];					// или изменяем
		//dump($_SESSION);
	}
	
	public function recalc($id){
		//dump($_SESSION['cart'][$id]);exit;
		if(!isset($_SESSION['cart'][$id])) return false; //проверяем на наличие итема в корзине
		
		$qtyMinus=$_SESSION['cart'][$id]['qty'];
		$sumMinus=$_SESSION['cart'][$id]['qty'] * $_SESSION['cart'][$id]['price'];
		$_SESSION['qty'] -= $qtyMinus;
		$_SESSION['sum'] -= $sumMinus;
		unset($_SESSION['cart'][$id]);
		
}

public function getWidgets() {
	//вытаскиваем виджеты для этой страницы
		$widgets = \app\models\Widgetset::find()
				->where(['widgetset_id' => 3])
				->andwhere(['widgetset.published' => 1])
				//->with(['value','value.param']) //жадная загрузка из связей в модели Page
				->innerJoinWith('widget')
				->orderBy('ordering')  //ORDERING виджетов по порядку
				//->indexBy('position')
				->asArray()
				->all();
		return $widgets;
}
}