<?php

namespace app\modules\shop\models;

use Yii;
use yii\base\Model;

class CartForm2 extends Model {


	public $email;
	public $name;
	public $tel1;
	public $addr;

//org
	public $org_name;
	public $inn;
	public $ogrn;
	public $kpp;
	public $gorod;
//расчетный счет
	public $pay_acc;
	public $corr_acc;
	public $bik;
	public $comment;


//public $verifyCode;

	public function rules() {
		return [
			// name, email, subject and body are required
			[['email', 'name', 'tel1', 'addr'], 'required'],
			// email has to be a valid email address
			['email', 'email'],
			//['email', 'unique', 'targetClass' => \app\modules\users\models\User::className(), 'message' => 'This email address has already been taken.'],
			[['org_name', 'comment'], 'string'],
			[['inn', 'ogrn', 'kpp', 'pay_acc', 'corr_acc', 'bik'], 'integer'],
//				// verifyCode needs to be entered correctly
//				 ['verifyCode', 'captcha'],
		];
	}

	public function attributeLabels() {
		return [
			'email' => 'Ваш email для связи',
			'name' => 'ФИО',
			'tel1' => 'Телефон для связи',
			'addr' => 'Адрес доставки',
			'org_name' => 'Полное название организации',
			'inn' => 'ИНН',
			'ogrn' => 'ОГРН',
			'kpp' => 'КПП',
			'gorod' => 'Годод',
			'pay_acc' => 'Расчетный счет',
			'corr_acc' => 'Корр. счет',
			'bik' => 'БИК',
			'comment' => 'Комментарий к заказу:',
			'verifyCode' => 'Проверочный код',
		];
	}

	/**
	 * Sends an email to the specified email address using the information collected by this model.
	 * @param string $email the target email address
	 * @return boolean whether the model passes validation
	 */
	public function contact($email) {
		dump($this);
		if ($this->validate()) {
			Yii::$app->mailer->compose()
					->setTo($email)
					->setFrom([$this->email => $this->name])
					->setSubject($this->subject)
					->setTextBody($this->body)
					->send();

			return true;
		}
		return false;
	}

}
