<?php

namespace app\modules\shop\models;

use Yii;

class Param extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'shop_product_param';
	}

	public function rules() {
		return [
			[['name'], 'required'],
			[['name', 'alias', 'unit', 'search', 'name_visible', 'visible', 'link_type', 'admin_only', 'level'], 'string'],
			[['cat_id', 'group_id', 'parent', 'filter_order', 'order', 'a1'], 'integer'],
			[['short_name'], 'string', 'max' => 100],
			[['position'], 'string', 'max' => 24],
			//[['desc'], 'string', 'max' => 3000],
			[['default'], 'string', 'max' => 255],
			[['variants'], 'string', 'max' => 10000],
			//[['type', 'tag'], 'string', 'max' => 10],
			[['class'], 'string', 'max' => 20],
			[['torgmail_id'], 'string', 'max' => 50],
		];
	}

	public function attributeLabels() {
		return [
			'param_id' => Yii::t('app', 'Param ID'),
			'name' => Yii::t('app', 'Name'),
			'short_name' => Yii::t('app', 'Short Name'),
			'alias' => Yii::t('app', 'Alias'),
			'group_id' => Yii::t('app', 'группировка параметров для админки'),
			'position' => Yii::t('app', 'не используется'),
			'parent' => Yii::t('app', 'Parent'),
			'desc' => Yii::t('app', 'описание параметра'),
			'default' => Yii::t('app', 'значение по умолчанию'),
			'variants' => Yii::t('app', 'Variants'),
			'type' => Yii::t('app', 'Type'),
			'tag' => Yii::t('app', 'html тег-обертка'),
			'class' => Yii::t('app', 'css класс'),
			'unit' => Yii::t('app', 'единица изм. параметра'),
			'search' => Yii::t('app', 'участие в поиске по значению'),
			'filter_order' => Yii::t('app', 'последовательность параметров в фильтре(0 не учавствует)'),
			'order' => Yii::t('app', 'Order'),
			'name_visible' => Yii::t('app', 'Name Visible'),
			'visible' => Yii::t('app', 'Visible'),
			'link_type' => Yii::t('app', 'тип ссылки параметра если она есть'),
			'admin_only' => Yii::t('app', 'Admin Only'),
			'level' => Yii::t('app', 'Level'),
			'torgmail_id' => Yii::t('app', 'Torgmail ID'),
			'a1' => Yii::t('app', 'A1'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getValue() {
		return $this->hasOne(Value::className(), ['param_id' => 'param_id']);
	}

//	public function getGroup() {
//		return $this->hasOne(\app\modules\shop\models\ParamsGroup::className(), ['param_group_id' => 'group_id']);
//	}
	
	public function getParentparams() {
		return $this->hasMany(Param::className(), ['parent' => 'param_id']);
	}

}
