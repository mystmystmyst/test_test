<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "shop_param_variant".
 *
 * @property integer $variant_id
 * @property integer $param_id
 * @property string $value
 */
class Variant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_product_param_variant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['param_id', 'value'], 'required'],
            [['param_id'], 'integer'],
            [['value','options'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'variant_id' => Yii::t('app', 'Variant ID'),
            'param_id' => Yii::t('app', 'Param ID'),
            'value' => Yii::t('app', 'value'),
	'options' => Yii::t('app', 'options'),
        ];
    }
}
