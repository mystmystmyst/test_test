<?php

namespace app\modules\shop\models;

use yii\base\Model;

class CartForm1 extends Model {


	public $cupon;
	public $delivery;
	public $payment;

//public $verifyCode;

	public function rules() {
		return [
			// name, email, subject and body are required
			[['delivery','payment'], 'required'],
			[['delivery','payment'], 'string'],
			[['cupon'], 'string'],	
		];
	}

	public function attributeLabels() {
		return [
			'cupon' => 'Введите № купона:',
			'payment' => 'Способ оплаты:',
			'delivery' => 'Способ доставки:',
		];
	}


}
