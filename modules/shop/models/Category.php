<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "page_category".
 *
 * @property integer $cat_id
 * @property string $name
 * @property string $page_structure
 * @property string $published
 * @property integer $parentcat
 * @property string $url
 *
 * @property Page[] $pages
 * @property PageCategoryParams[] $pageCategoryParams
 * @property PageParams[] $params
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
//    public function rules()
//    {
//        return [
//            [['name', 'cat_structure', 'page_structure', 'item_structure', 'published','url'], 'string'],
//            [['cat_structure', 'page_structure', 'item_structure'], 'required'],
//            [['parentcat'], 'integer'],
//	
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
		'cat_id' => 'ID',
		'name' => 'Категория',
		'url' => 'URL',
		'published' => 'Опубликована',
		'parentcat' => 'Родитель',
		
		];
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['cat_id' => 'cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategoryParams()
    {
        return $this->hasMany(ProductCategoryParams::className(), ['cat_id' => 'cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParam()
    {
        return $this->hasMany(ProductParam::className(), ['param_id' => 'param_id'])->viaTable('page_category_params', ['cat_id' => 'cat_id']);
    }
	
    public function getWidgetset()
    {
        return $this->hasOne(Widgetset::className(), ['widgetset_id' => 'widgetset_id']);
    }
}
