<?php

namespace app\modules\shop\models;

//use Yii;

/**
 * 
 *
 */
class Wish extends \yii\db\ActiveRecord{
	
	//добавление продукта в корзину
	public function addToCart($product, $page_id, $qty=1){
		//проверка на существование товара в корзине(массиве $session)
		
		if(isset($_SESSION['cart'][$page_id])){
			$_SESSION['cart'][$page_id]['qty'] += $qty; //обновляем количество этого товара
		}else{									//добавляем в масив корзины
			$_SESSION['cart'][$page_id] =[
				'qty'=>$qty,
				'name'=>$product[1]['value'],
				'price'=>$product[6]['value'],
				'img' => json_decode($product[3]['value'],true)['src']
			];
		}
		//проверяем общ количество и или кладем или добавляем
		$_SESSION['qty'] = isset($_SESSION['qty'] ) ? 
				$_SESSION['qty'] + $qty : 
				$qty;
		//dump($_SESSION['cart']);
		
		// Проверяем сумму и 
		$_SESSION['sum'] = isset($_SESSION['sum']) ? 
			$_SESSION['sum'] + $qty *  $product[6]['value']:  //добавляем
			$qty *  $product[6]['value'];					// или изменяем
		//dump($_SESSION);
	}
	
	public function recalc($id){
		//dump($_SESSION['cart'][$id]);exit;
		if(!isset($_SESSION['cart'][$id])) return false; //проверяем на наличие итема в корзине
		
		$qtyMinus=$_SESSION['cart'][$id]['qty'];
		$sumMinus=$_SESSION['cart'][$id]['qty'] * $_SESSION['cart'][$id]['price'];
		$_SESSION['qty'] -= $qtyMinus;
		$_SESSION['sum'] -= $sumMinus;
		unset($_SESSION['cart'][$id]);
		
}
}