<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "shop_comment".
 *
 * @property integer $comment_id
 * @property integer $product_id
 * @property string $plus
 * @property string $minus
 * @property string $comment
 * @property string $autor
 * @property string $date_create
 * @property integer $agree
 * @property integer $reject
 * @property integer $grade
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_comment';
    }

    /**
     * @inheritdoc
     */
//    public function rules()
//    {
//        return [
//            [['comment_id', 'product_id', 'plus', 'minus', 'comment', 'autor', 'date_create'], 'required'],
//            [['comment_id', 'product_id', 'agree', 'reject', 'grade'], 'integer'],
//            [['date_create'], 'safe'],
//            [['plus', 'minus'], 'string', 'max' => 500],
//            [['comment'], 'string', 'max' => 1000],
//            [['autor'], 'string', 'max' => 50],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comment_id' => Yii::t('app', 'Comment ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'plus' => Yii::t('app', 'Достоинства'),
            'minus' => Yii::t('app', 'Недостатки'),
            'comment' => Yii::t('app', 'Comment'),
            'autor' => Yii::t('app', 'Autor'),
            'date_create' => Yii::t('app', 'Date Create'),
            'agree' => Yii::t('app', 'колич. согласных'),
            'reject' => Yii::t('app', 'колич. Не согласных'),
            'grade' => Yii::t('app', 'общая оценка'),
        ];
    }
}
