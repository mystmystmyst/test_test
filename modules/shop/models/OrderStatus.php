<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "shop_order_status".
 *
 * @property integer $status_id
 * @property string $status
 * @property string $description
 */
class OrderStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_order_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'description'], 'required'],
            [['status'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Статус'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }
}
