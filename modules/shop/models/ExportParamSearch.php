<?php

namespace app\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\ExportParam;

/**
 * ExportParamSearch represents the model behind the search form about `app\modules\shop\models\ExportParam`.
 */
class ExportParamSearch extends ExportParam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['export_id', 'param_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ExportParam::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'export_id' => $this->export_id,
            'param_id' => $this->param_id,
        ]);

        return $dataProvider;
    }
}
