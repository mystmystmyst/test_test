<?php

namespace app\modules\shop\models;

use Yii;


class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'cat_id'], 'required'],
            [['product_id', 'cat_id', 'vendor', 'host', 'featured', 'sales'], 'integer'],
            [['published', 'available', 'alias'], 'string'],
            [['date_create', 'date_update'], 'safe'],
           // [['meta_title', 'meta_key', 'meta_desc'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('app', 'Product ID'),
            'published' => Yii::t('app', 'Published'),
            'available' => Yii::t('app', 'доступен для заказа'),
            'cat_id' => Yii::t('app', 'Cat ID'),
            'alias' => Yii::t('app', 'Alias'),
//            'meta_title' => Yii::t('app', 'Meta Title'),
//            'meta_key' => Yii::t('app', 'Meta Key'),
//            'meta_desc' => Yii::t('app', 'Meta Desc'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'vendor' => Yii::t('app', 'продавец'),
            'host' => Yii::t('app', 'на каком сайте отображать'),
            'featured' => Yii::t('app', 'Featured'),
            'sales' => Yii::t('app', 'Sales'),
            'description' => Yii::t('app', 'Description'),
            'price' => Yii::t('app', 'Price'),
            'export' => Yii::t('app', 'Export'),
            'name' => 'Наименование',
            'image' => 'Изображение',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['cat_id' => 'cat_id']);
    }

    public function getParamValues()
    {
        return $this->hasMany(Value::className(), ['product_id' => 'product_id'])->indexBy('param_id');
    }
}
