<?php

namespace app\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\Param;

/**
 * ParamSearch represents the model behind the search form about `app\modules\shop\models\Param`.
 */
class ParamSearch extends Param
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['param_id',  'group_id', 'parent', 'filter_order', 'order'], 'integer'],
            [['name', 'alias', 'description', 'variants', 'type', 'unit', 'search', 'name_visible', 'visible', 'link_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Param::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'param_id' => $this->param_id,
            'group_id' => $this->group_id,
            'parent' => $this->parent,
            'filter_order' => $this->filter_order,
            'order' => $this->order,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'variants', $this->variants])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'search', $this->search])
            ->andFilterWhere(['like', 'name_visible', $this->name_visible])
            ->andFilterWhere(['like', 'visible', $this->visible])
            ->andFilterWhere(['like', 'link_type', $this->link_type]);

        return $dataProvider;
    }
}
