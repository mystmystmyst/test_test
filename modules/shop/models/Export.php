<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "shop_export".
 *
 * @property integer $id
 * @property string $description
 * @property string $name
 */
class Export extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_export';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'name'], 'required'],
            [['description', 'name'], 'string', 'max' => 100],
	 [['filename'], 'string', 'max' => 255],
	[['param_id'], 'integer'],
	[['filename'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'name' => Yii::t('app', 'Name'),
	'filename' => Yii::t('app', 'filename'),
	'param_id' => Yii::t('app', 'параметр-индикатор'),
			'params' => Yii::t('app', 'Список параметров'),
        ];
    }
	
	
public function getParams()
    {
        return $this->hasMany(ExportParam::className(), ['export_id' => 'param_id']);
    }
}
