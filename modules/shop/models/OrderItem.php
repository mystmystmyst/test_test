<?php

namespace app\modules\shop\models;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "shop_order_item".
 *
 * @property string $item_id
 * @property string $order_id
 * @property integer $page_id
 * @property integer $qty_item
 * @property double $sum_item
 * @property double $price
 */
class OrderItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_order_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'page_id', 'qty_item', 'sum_item', 'price'], 'required'],
            [['order_id', 'page_id', 'qty_item'], 'integer'],
            [['sum_item', 'price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'order_id' => 'Order ID',
            'page_id' => 'Page ID',
            'qty_item' => 'количество товара',
            'sum_item' => 'сумма',
            'price' => 'цена товара на момент покупки',
        ];
    }
	
		public function getOrderItems(){
		return $this->hasOne(Order::className(), ['order_id'=>'order_id']);
	}
}
