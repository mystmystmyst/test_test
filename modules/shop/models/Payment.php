<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "shop_payment".
 *
 * @property integer $payment_id
 * @property string $name
 * @property string $description
 * @property integer $price
 * @property integer $currency
 * @property string $param
 * @property integer $vendor
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'param'], 'required'],
            [['price', 'currency', 'vendor', 'published'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['description', 'param'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_id' => Yii::t('app', 'Payment ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'price' => Yii::t('app', 'Price'),
            'currency' => Yii::t('app', 'Currency'),
            'param' => Yii::t('app', 'Param'),
            'vendor' => Yii::t('app', 'Vendor'),
	'published' => Yii::t('app', 'published'),		
        ];
    }
}
