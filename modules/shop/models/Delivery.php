<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "shop_delivery".
 *
 * @property integer $delivery_id
 * @property string $name
 * @property string $description
 * @property integer $price
 * @property integer $currency
 * @property string $param
 * @property integer $published
 * @property integer $vendor
 */
class Delivery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'price', 'param'], 'required'],
            [['price', 'currency', 'published', 'vendor'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['description', 'param'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'delivery_id' => Yii::t('app', 'Delivery ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'price' => Yii::t('app', 'Price'),
            'currency' => Yii::t('app', 'Currency'),
            'param' => Yii::t('app', 'Param'),
            'published' => Yii::t('app', 'Published'),
            'vendor' => Yii::t('app', 'Vendor'),
        ];
    }
}
