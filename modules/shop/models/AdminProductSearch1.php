<?php

namespace app\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\Product;
use app\modules\shop\helpers\AppGridHelper;

/**
 * ProductSearch represents the model behind the search form about `app\modules\shop\models\Product`.
 */
class AdminProductSearch1 extends Product {

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {		
		
		$query = Product::find()->select(Product::tableName().'.*')->joinWith(['category']);//->alias('p');
		/*SELECT p.*, v1.value as par1, v2.value as par2, v3.value as par3 FROM `shop_product` as p left join `shop_product_value` as v1 on v1.product_id = p.product_id and v1.param_id = 2 left join `shop_product_value` as v2 on v2.product_id = p.product_id and v2.param_id = 5 left join `shop_product_value` as v3 on v3.product_id = p.product_id and v3.param_id = 29*/
		$sort = ['attributes' => [
            'product_id',
            'published',
		    'cat.name' => [
							'asc' => [
								'category.name' => SORT_ASC
							],
							'desc' => [
								'category.name' => SORT_DESC
							],
							'default' => SORT_ASC
						],
        ]];
		if (isset($params['templateid'])) {
			$fields = AppGridHelper::getFieldId($params['templateid']);
			$index = 1;
			foreach ($fields as $key => $value) {
				$query->addSelect('v'.$index.'.value as '.$key);
				$query->leftjoin('shop_product_value AS v'.$index, Product::tableName().'.product_id = v'.$index.'.product_id AND v'.$index.'.param_id = '.$value);
				$sort = array_merge_recursive($sort, [
					'attributes' => [
						$key => [
							'asc' => [
								'v'.$index.'.value' => SORT_ASC
							],
							'desc' => [
								'v'.$index.'.value' => SORT_DESC
							],
							'default' => SORT_ASC
						],
					]
				]);
				$index++;
			}
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 10,
			],
		]);

		$dataProvider->setSort($sort);

		$this->load($params);

		(empty($params['cat_id']))?: $query->andFilterWhere([Product::tableName().'.cat_id' => $params['cat_id'],]);

		return $dataProvider;
	}

}