<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "Value".
 *
 * @property integer $page_id
 * @property integer $param_id
 * @property string $value
 *
 * @property PageParams $param
 * @property Page $page
 */
class Value extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_product_value';
    }

    /**
     * @inheritdoc
     */
//    public function rules()
//    {
//        return [
//            [['product_id', 'param_id', 'value'], 'required'],
//            [['product_id', 'param_id'], 'integer'],
//            [['value'], 'string']
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Page ID',
            'param_id' => 'Param ID',
            'value' => 'Value',
       
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParam()
    {
        return $this->hasOne(Param::className(), ['param_id' => 'param_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['product_id' => 'product_id']);
    }
}
