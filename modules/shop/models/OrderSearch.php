<?php

namespace app\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\modules\shop\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'initiator_id', 'qty', 'status_id'], 'integer'],
            [['date_create', 'date_update', 'order_items'], 'safe'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find()->orderBy('order_id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'initiator_id' => $this->initiator_id,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
            'qty' => $this->qty,
            'sum' => $this->sum,
            'status_id' => $this->status_id,
        ]);

        $query->andFilterWhere(['like', 'order_items', $this->order_items]);

        return $dataProvider;
    }
}
