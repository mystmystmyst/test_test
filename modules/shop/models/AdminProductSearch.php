<?php

namespace app\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\db\Expression;
use yii\helpers\Html;

/**
 * ProductSearch represents the model behind the search form about `app\modules\shop\models\Product`.
 */
class AdminProductSearch extends Model
{

    public $template;

    public $product_id;
    public $cat_id;
    public $vendor;
    public $host;

    public $published;
    public $available;
    public $alias;

    public $paramValues = [
    ];

    public function paramLabels()
    {
        return Yii::$app->params['admin.product.search.template'][$this->template];
    }

    public function paramColumns()
    {
        $columns = [];
        foreach ($this->paramLabels() as $id => $label) {
            $columns[] = [
                'attribute' => "paramValues[{$id}]",
                'filter' => Html::activeTextInput($this, "paramValues[{$id}]"),
                'label' => $label,
                'value' => "paramValues.{$id}.value",
            ];
        }

        return $columns;
    }

    /**
	 * @inheritdoc
	 */
    public function rules()
    {
        return [
            ['paramValues', 'safe'],
            ['template', 'in', 'range' => array_keys($this->templateLabels())],
            [['product_id', 'cat_id', 'vendor', 'host'], 'integer'],
            [['published', 'available', 'alias'], 'safe'],
        ];
    }

    public function categoryLabels()
    {
        return Category::find()->select(['name', 'cat_id'])
            ->andWhere(['and',
                ['!=', 'parentcat', 0],
                ['type' => 1],
            ])
            ->indexBy('cat_id')
            ->asArray()
            ->column();
    }
    
    public function templateLabels()
    {
        return [
            0 => 'Template 1',
            1 => 'Template 2',
            2 => 'Template 3',
            3 => 'Template 4',
        ];
    }

    public function paramSortAttributes()
    {
        $attributes = [];
        foreach ($this->paramLabels() as $id => $label) {
            $attributes["paramValues[{$id}]"] = [
                'asc' => ['paramValues.value' => SORT_ASC],
                'desc' => ['paramValues.value' => SORT_DESC],
            ];
        }
        return $attributes;
    }

	public function init()
    {
        parent::init();
        $this->template = Yii::$app->session->get('admin.editor.template', 0);
    }

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
    {
        $query = Product::find()->innerJoinWith(['category']);
        $query->with(['paramValues' => function(ActiveQuery $query) {
            $query->alias('paramValues');
        }]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => array_merge([
                    'product_id', 'cat_id', 'vendor', 'host',
                    'published', 'available', 'alias',
                    'category.name' => [
                        'asc' => ['category.name' => SORT_ASC],
                        'desc' => ['category.name' => SORT_DESC],
                        'default' => SORT_ASC,
                    ]
                ],$this->paramSortAttributes())
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        Yii::$app->session->set('admin.editor.template', $this->template);

        $flip = array_flip($this->paramValues);
        if (!empty($flip) && (count($flip) > 1 || !isset($flip['']))) {
            $query->joinWith(['paramValues' => function(ActiveQuery $query) {
                $query->alias('shop_product_value');

                $cond = ['or'];
                foreach ($this->paramValues as $id => $value) {
                    $cond[] = ['and',
                        ['shop_product_value.param_id' => $id],
                        ['like', 'shop_product_value.value', $value]
                    ];

                }
                $query->andWhere($cond);

            }], false, 'RIGHT JOIN');
            $query->andHaving(new Expression('COUNT(shop_product_value.product_id) = :count', [':count' => count($this->paramValues)]));
            $query->groupBy(['shop_product_value.product_id']);
        }

		$query->andFilterWhere(['and',
            [
                'shop_product.product_id' => $this->product_id,
                'shop_product.cat_id' => $this->cat_id,
                'shop_product.vendor' => $this->vendor,
                'shop_product.host' => $this->host,
                'shop_product.published' => $this->published,
                'shop_product.available' => $this->available,
            ],
            ['like', 'shop_product.alias', $this->alias],
        ]);

        return $dataProvider;
	}

}
