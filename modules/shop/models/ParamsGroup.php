<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "shop_product_params_group".
 *
 * @property integer $param_group_id
 * @property string $name
 * @property integer $parent
 * @property integer $order
 * @property string $desc
 */
class ParamsGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_product_params_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'parent_group', 'order', 'desc'], 'required'],
            [['name'], 'string'],
            [['parent_group', 'order'], 'integer'],
            [['desc'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'param_group_id' => Yii::t('app', 'ID группы параметров'),
            'name' => Yii::t('app', 'Название группы параметров и вкладок в админке'),
            'parent' => Yii::t('app', 'Parent'),
            'order' => Yii::t('app', 'Order'),
            'desc' => Yii::t('app', 'Desc'),
        ];
    }
	
public function getParam()
    {
        return $this->hasMany(Param::className(), ['group_id' => 'group_id']);
    }
}
