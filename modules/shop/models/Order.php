<?php

namespace app\modules\shop\models;

//use Yii;
use \yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use \app\modules\users\models\User;

/**
 * This is the model class for table "shop_order".
 *
 * @property string $order_id
 * @property string $date_create
 * @property string $date_update
 * @property integer $qty
 * @property double $sum
 * @property integer $status_id
 */
class Order extends ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'shop_order';
	}

	public function behaviors() {
		parent::behaviors();

		return[
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['date_create', 'date_update'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['date_update'],
				],
				//если вместо метки юникс используется datetime:
				'value' => new \yii\db\Expression('NOW()'),
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['initiator_id'], 'required'],
			[['date_create', 'date_update'], 'safe'],
			[['qty', 'status_id', 'initiator_id', 'executor_id'], 'integer'],
			[['sum'], 'number'],
			[['order_items'], 'string', 'max' => 500],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'initiator_id' => 'Заказчик',
			'status_id' => 'Статус',
			'qty' => 'Количество',
			'sum' => 'Сумма',
			'date_create' => 'Создан',
			'date_update' => 'Изменён',
			'executor_id' => 'Исполнитель',
			
		];
	}

	public function getStatus() {
		return $this->hasOne(OrderStatus::className(), ['status_id' => 'status_id']);
	}

	public function getInitiator() {
		return $this->hasOne(User::className(), ['id' => 'initiator_id']);
	}
	
	public function getExecutor() {
		return $this->hasOne(User::className(), ['id' => 'executor_id']);
	}

}
