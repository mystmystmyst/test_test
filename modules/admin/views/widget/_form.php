<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Widget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="widget-form">

    <?php 	$form = ActiveForm::begin(); ?>
<?= 'ID: '.$model->widget_id;?>
        <?= $form->field($model, 'published')->checkbox([ '0', '1',])?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'name_visible')->checkbox([ '0', '1',]) ?>

    <?= $form->field($model, 'type')->dropDownList(Yii::$app->params['widgets']) ?>

    <?= $form->field($model, 'params')->widget(\mihaildev\ckeditor\CKEditor::className(),
					[
						'editorOptions'=>[
							'preset'=>'basic',	//full, basic, standard
							'inline'=>false,
						]
					]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
