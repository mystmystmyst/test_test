<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\WidgetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Widgets');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<small>Список виджетов, установленных на сайте.</small>
    <div class="edit-btn">
        <?= Html::a(Yii::t('app', 'Create Widget'), ['create'], ['class' => 'btn btn-success']) ?>
    </div>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
	 'options' => ['class'=>'table-responsive'],
	'layout' => "{items}\n{pager}\n{summary}\n",
	
        'columns' => [
			['class' => 'yii\grid\ActionColumn',
				'template' => '{view} {link}',],
           // 'widget_id',
            'published',
			'name:ntext',
			 'type:ntext',
            'name_visible',
           
           // 'params:ntext',
            

           ['class' => 'yii\grid\ActionColumn',
				'template' => '{update} {delete}',],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
