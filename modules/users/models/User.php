<?php

namespace app\modules\users\models;

use Yii;
use \yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

class User extends ActiveRecord implements IdentityInterface {

	const STATUS_BLOCKED = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_WAIT = 2;

	public static function tableName() {
		return 'user';
	}

	public function behaviors() {
		return [
			\yii\behaviors\TimestampBehavior::className(),
		];
	}

	public function getStatusName() {
		return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
	}

	public static function getStatusesArray() {
		return [
			self::STATUS_BLOCKED => 'Заблокирован',
			self::STATUS_ACTIVE => 'Активен',
			self::STATUS_WAIT => 'Не подтвержден',
		];
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
	}

	public static function findIdentityByAccessToken($token, $type = null) {
		throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

	public function getAuthKey() {
		return $this->auth_key;
	}

	public function validateAuthKey($authKey) {
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 * @return static|null
	 */
	public static function findByUsername($username) {
		return static::findOne(['username' => $username]);
	}

	/**
	 * Finds user by email
	 *
	 * @param string $email
	 * @return static|null
	 */
	public static function findByEmail($email) {
		return static::findOne(['email' => $email]);
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password) {
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * Перед записью в базу для каждого пользователя нужно генерировать хэш пароля и дополнительный ключ автоматической аутентификации.
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey() {
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token) {
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}
		return static::findOne([
					'password_reset_token' => $token,
					'status' => self::STATUS_ACTIVE,
		]);
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 * @return boolean
	 */
	public static function isPasswordResetTokenValid($token) {
		if (empty($token)) {
			return false;
		}
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		$parts = explode('_', $token);
		$timestamp = (int) end($parts);
		return $timestamp + $expire >= time();
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken() {
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken() {
		$this->password_reset_token = null;
	}

//ПОДТВерждение EMAIL

	/**
	 * @param string $email_confirm_token
	 * @return static|null
	 */
	public static function findByEmailConfirmToken($email_confirm_token) {
		return static::findOne(['email_confirm_token' => $email_confirm_token, 'status' => self::STATUS_WAIT]);
	}

	/**
	 * Generates email confirmation token
	 */
	public function generateEmailConfirmToken() {
		$this->email_confirm_token = Yii::$app->security->generateRandomString();
	}

	/**
	 * Removes email confirmation token
	 */
	public function removeEmailConfirmToken() {
		$this->email_confirm_token = null;
	}

//ПОДТВерждение EMAIL END--------------------------------------


	public static function generatePassword() {
		$len = rand(7, 11);
		$useChars = '123456789abcdefghkmnpqrstuvwxyz';
		for ($i = 0; $i < $len; $i++) {
			$value .= $useChars[mt_rand(0, strlen($useChars) - 1)];
		}
		return $value;
	}

	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if ($insert) {
				$this->generateAuthKey();
			}
			return true;
		}
		return false;
	}

	public function rules() {
		return [
			['username', 'required'],
			['username', 'match', 'pattern' => '#^[\w_-]+$#i'],
			['username', 'unique', 'targetClass' => self::className(), 'message' => 'This username has already been taken.'],
			['username', 'string', 'min' => 2, 'max' => 255],
			['name', 'string', 'max' => 255],
			['email', 'required'],
			['email', 'email'],
			['email', 'unique', 'targetClass' => self::className(), 'message' => 'This email address has already been taken.'],
			['email', 'string', 'max' => 255],
			['status', 'integer'],
			['status', 'default', 'value' => self::STATUS_ACTIVE],
			['status', 'in', 'range' => array_keys(self::getStatusesArray())],
		];
	}

	public function attributeLabels() {
		return [
			'id' => Yii::t('app', 'User ID'),
			'short_name' => Yii::t('app', 'краткое имя для таблиц'),
			'username' => Yii::t('app', 'Username'),
			'group_id' => Yii::t('app', 'Group ID'),
			'org_id' => Yii::t('app', 'наличие связи с организацией'),
			'auth_key' => Yii::t('app', 'Auth Key'),
			'password_hash' => Yii::t('app', 'Password Hash'),
			'password_reset_token' => Yii::t('app', 'Password Reset Token'),
			'email' => Yii::t('app', 'Email'),
			'status' => Yii::t('app', 'Status'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'name' => Yii::t('app', 'ФИО'),
			'tel1' => Yii::t('app', 'Телефон'),
			'tel2' => Yii::t('app', 'Tel2'),
			'addr' => Yii::t('app', 'Адрес доставки'),
		];
	}

	public function getGroup() {
		return $this->hasOne(UserGroup::className(), ['group_id' => 'group_id']);
	}

}
