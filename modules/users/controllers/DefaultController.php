<?php

namespace app\modules\users\controllers;

use app\modules\users\models\EmailConfirmForm;
use app\modules\users\models\LoginForm;
use app\modules\users\models\PasswordResetRequestForm;
use app\modules\users\models\PasswordResetForm;
use app\modules\users\models\SignupForm;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use Yii;

class DefaultController extends Controller {

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout', 'signup'],
				'rules' => [
					[
						'actions' => ['signup'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
		];
	}

	public function actions() {
		return [
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_DEBUG ? 'testme' : null,
			],
		];
	}

	public function actionLogin() {
		//dump(\yii::$app->getUser()->getReturnUrl());
		if (!Yii::$app->user->isGuest) {
			Yii::$app->session->setFlash('success', 'Вы уже авторизоавны!');
		return $this->goBack();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			//echo Yii::$app->request->referrer,'<br>';
			Yii::$app->session->setFlash('success', 'Вы авторизованы как'. $model->username.'!');
			//dump(\yii::$app->getUser()->getReturnUrl());
			return $this->goBack();
		} else {
//вытаскиваем виджеты для этой страницы
		
		$cat = \app\models\Category::findOne(['alias'=>'users']);
		$widgets = \app\models\Widgetset::find()
				->where(['widgetset_id' => $cat->widgetset_id])
				->andwhere(['widgetset.published' => 1])
				//->with(['value','value.param']) //жадная загрузка из связей в модели Page
				->innerJoinWith('widget')
				->orderBy('ordering')  //ORDERING виджетов по порядку
				//->indexBy('position')
				->asArray()
				->all();
			return $this->render('login', [
						'model' => $model,
						'widgets' => $widgets,
			]);
		}
	}

	public function actionLogout() {
		print_r(Yii::$app->user->isGuest);
		Yii::$app->user->logout();
		return $this->goBack(Yii::$app->request->referrer);
	}

	public function actionSignup() {
		$model = new SignupForm();
		if ($model->load(Yii::$app->request->post())) {
			if ($user = $model->signup()) {
				Yii::$app->getSession()->setFlash('success', 'Подтвердите ваш электронный адрес.');
				return $this->goHome();
			}
		}
//вытаскиваем виджеты для этой страницы
		
		$cat = \app\models\Category::findOne(['alias'=>'users']);
		$widgets = \app\models\Widgetset::find()
				->where(['widgetset_id' => $cat->widgetset_id])
				->andwhere(['widgetset.published' => 1])
				//->with(['value','value.param']) //жадная загрузка из связей в модели Page
				->innerJoinWith('widget')
				->orderBy('ordering')  //ORDERING виджетов по порядку
				//->indexBy('position')
				->asArray()
				->all();
		return $this->render('signup', [
			'model' => $model,
			'widgets' => $widgets,
		]);
	}

	public function actionEmailConfirm($token) {
		try {
			$model = new EmailConfirmForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->confirmEmail()) {
			Yii::$app->getSession()->setFlash('success', 'Спасибо! Ваш Email успешно подтверждён.');
		} else {
			Yii::$app->getSession()->setFlash('error', 'Ошибка подтверждения Email.');
		}

		return $this->goHome();
	}

	public function actionPasswordResetRequest() {
		$model = new PasswordResetRequestForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail()) {
				Yii::$app->getSession()->setFlash('success', 'Спасибо! На ваш Email было отправлено письмо со ссылкой на восстановление пароля.');

				return $this->goHome();
			} else {
				Yii::$app->getSession()->setFlash('error', 'Извините. У нас возникли проблемы с отправкой.');
			}
		}

		return $this->render('passwordResetRequest', [
					'model' => $model,
		]);
	}

	public function actionPasswordReset($token) {
		try {
			$model = new PasswordResetForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->getSession()->setFlash('success', 'Спасибо! Пароль успешно изменён.');

			return $this->goHome();
		}

		return $this->render('passwordReset', [
					'model' => $model,
		]);
	}

}
