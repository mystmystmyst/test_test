<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\users\models\UserGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-group-index">

		<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
    <div class="edit-btn">
<?= Html::a(Yii::t('app', 'Create User Group'), ['create'], ['class' => 'btn btn-success btn-xs']) ?>
    </div>

	<?php Pjax::begin(); ?>    <?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [

			'name' => [
				'attribute' => 'name',
				'label' => Yii::t('app', 'User Group'),
				'headerOptions' => ['style' => 'width:40%']
			],
			'desc' => [
				'attribute' => 'desc',
				'label' => Yii::t('app', 'Описание'),
				'headerOptions' => ['style' => 'width:50%']
			],
			['class' => 'yii\grid\ActionColumn',
				'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}',],
		],
	]);
	?>
<?php Pjax::end(); ?></div>
