<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\users\models\UserGroup */

$this->title = Yii::t('app', 'Create User Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-group-create">

 

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
