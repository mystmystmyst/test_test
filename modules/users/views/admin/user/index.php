<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\users\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\users\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">


	<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
<div class="edit-btn">
<?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success btn-xs']) ?>
</div>

	<?php Pjax::begin(); ?>    <?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'tableOptions'=>['class'=>'table table-striped table-hover table-bordered table-condensed'],
		'columns' => [
			['class' => 'yii\grid\ActionColumn',
				'template' => '{view}'],
			'name',
			'email',
			'tel1:ntext:Телефон',
			'group'=>[
				'label'=>'Группа',
				'format'=>'raw',
				'value' =>function ($model) {
					return '<button class="btn btn-sm btn-info">'.$model->group->name.'</button>';
				}
			],
			'status' =>[
				'format'=>'html',
				'value' =>function ($model) {
					switch ($model->status) {
						case User::STATUS_BLOCKED:
							return  '<span class="label label-danger">'.ArrayHelper::getValue(User::getStatusesArray(), $model->status).'</span>';
						case User::STATUS_WAIT:
							return  '<span class="label label-warning">'.ArrayHelper::getValue(User::getStatusesArray(), $model->status).'</span>';
						default: return '<span class="label label-success">'.ArrayHelper::getValue(User::getStatusesArray(), $model->status).'</span>';
							
					}
			},
			] ,
			[
				'attribute' => 'updated_at',
				'value' => function ($model) {
					if ($model->updated_at > 0) {
						return $model->updated_at;
					}
				},
//			'filterInputOptions' => [
//					'attribute' => 'name',
//					'placeholder' => 'название книги',
//					'class' => 'form-control',
//					'id' => null,
//				],
			//'headerOptions' => ['style' => 'width:8%']
			],
			['class' => 'yii\grid\ActionColumn',
				'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}',
			],
		],
	]);
	?>
	<?php Pjax::end(); ?></div>
