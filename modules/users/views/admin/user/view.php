<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\users\models\User */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
<div class="pull-right text-right">
		<?= 'Дата создания: ' . $model->created_at ?>
		<?= 'Дата изменения: ' . $model->updated_at ?>
	</div>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
          //  'short_name',
            'username',
            'group_id',
          //  'org_id',
            'email:email',
            'status',
            'created_at',
            'updated_at',
            'name:ntext',
            'tel1:ntext',
            'addr',

        ],
    ]) ?>

</div>
