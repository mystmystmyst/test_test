<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\users\models\User */

$this->title = Yii::t('app', 'Update account: ') . $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-update">

	<div class="pull-right text-right">
		<?= 'Дата создания: ' . $model->created_at ?>
		<?= 'Дата изменения: ' . $model->updated_at ?>
	</div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
