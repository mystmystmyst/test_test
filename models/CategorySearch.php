<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Category;

/**
 * CategorySearch represents the model behind the search form about `app\models\Category`.
 */
class CategorySearch extends Category
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'type', 'widgetset_id', 'parentcat'], 'integer'],
            [['name', 'template_structure', 'cat_structure', 'page_structure', 'item_structure', 'admin_structure', 'published', 'url', 'name_visible'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Category::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cat_id' => $this->cat_id,
            'type' => $this->type,
            'widgetset_id' => $this->widgetset_id,
            'parentcat' => $this->parentcat,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'template_structure', $this->template_structure])
            ->andFilterWhere(['like', 'cat_structure', $this->cat_structure])
            ->andFilterWhere(['like', 'page_structure', $this->page_structure])
            ->andFilterWhere(['like', 'item_structure', $this->item_structure])
            ->andFilterWhere(['like', 'admin_structure', $this->admin_structure])
            ->andFilterWhere(['like', 'published', $this->published])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'name_visible', $this->name_visible]);

        return $dataProvider;
    }
}
