<?php

namespace app\models;

use Yii;

class Widgetset extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'widgetset';
    }

//    public function rules()
//    {
//        return [
//            [['name', 'cat_id', 'variants', 'type', 'search', 'ordering', 'visible'], 'required'],
//            [['name', 'variants'], 'string'],
//            [['cat_id', 'search', 'ordering', 'visible'], 'integer'],
//            [['type'], 'string', 'max' => 10]
//        ];
//    }


//    public function attributeLabels() {
//		return [
//			'param_id' => 'Param ID',
//			'name' => 'Name',
//			'alias' => 'alias',
//			'label_visible' => 'label_visible',
//			'cat_id' => 'Cat ID',
//			'variants' => 'Variants',
//			'type' => 'Type',
//			'search' => 'Search',
//			'ordering' => 'Ordering',
//			'visible' => 'Visible',
//		];
//	}
 public function getWidget()
    {
        return $this->hasMany(Widget::className(), ['widget_id' => 'widget_id']);
    }
}
