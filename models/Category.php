<?php

namespace app\models;

//use Yii;

class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'cat_structure', 'page_structure', 'item_structure', 'published','url'], 'string'],
            [['cat_structure', 'page_structure', 'item_structure'], 'required'],
            [['parentcat','type'], 'integer'],
	
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
		'cat_id' => 'ID',
		'name' => 'Категория',
		'url' => 'URL',
		'cat_structure' => 'Структура категории',
		'page_structure' => 'Структура страницы категории',
		'item_structure' => 'Структура краткого описания категории',
		'published' => 'Опубликована',
		'parentcat' => 'Родитель',
		'type'=>'Тип категории'
		];
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['cat_id' => 'cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageCategoryParams()
    {
        return $this->hasMany(PageCategoryParams::className(), ['cat_id' => 'cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParam()
    {
        return $this->hasMany(PageParam::className(), ['param_id' => 'param_id'])->viaTable('page_category_params', ['cat_id' => 'cat_id']);
    }
	
    public function getWidgetset()
    {
        return $this->hasOne(Widgetset::className(), ['widgetset_id' => 'widgetset_id']);
    }
}
