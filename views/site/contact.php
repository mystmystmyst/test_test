<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->params['widgets'] = \app\modules\shop\helpers\CookHtml::getWidget($widgets);
$this->title = 'Как с нами связаться:';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

	<div class="col-md-4">
		<h3 class="title">Контакты:</h3>		
		<div class="media">
			<div class="media-left">
				<span class="glyphicon glyphicon-info-sign"></span>
			</div>
			<div class="media-body">
				<span class="contact-street">ООО "ИНФИНИТИ", ОГРН: 1147746223393</span>
			</div>
		</div>
		<div class="media">
			<div class="media-left">
				<span class="glyphicon glyphicon-home"></span>
			</div>
			<div class="media-body">
				<span class="contact-street">129515, г. Москва, ул. Хованская, 22, строение 1, офис 1 </span>
			</div>
		</div>
		<div class="media">
			<div class="media-left">
				<span class="glyphicon glyphicon-envelope"></span>
			</div>
			<div class="media-body">
				Почта: <a href="mailto:shop@vmela.ru">shop@vmela.ru</a>
			</div>
		</div>
		<div class="media">
			<div class="media-left">
				<span class="glyphicon glyphicon-earphone"></span>
			</div>
			<div class="media-body">
				Телефоны: 
				<ul class="list-unstyled">
					<li><strong>8-(800)-301-09-95</strong> Бесплатный по России</li>
					<li><strong>8-(495)-308-89-06</strong> по Москве и области</li>
					<li><strong>8-(495)-722-58-27</strong> по Москве и области</li>
				</ul>
			</div>
		</div>
		<div class="media">
			<div class="media-left">
				<span class="glyphicon glyphicon-calendar"></span>
			</div>
			<div class="media-body">
				<span>Часы работы: с 09:00 до 18:00  Без выходных.</span>
			</div>
			
			<div>
				<noindex>
				
<div class="col-xs-4" style="padding: 30px;"><a href="https://vkontakte.ru/vmela_ru" rel="nofollow" title="Мы ВКонтакте" target="_blank"><img src="https://image.freepik.com/free-icon/vk_318-136413.jpg"/></a></div>
   <div class="col-xs-4" style="padding: 30px;"> <a href="https://www.facebook.com/vmela_ru" rel="nofollow" title="Мы в Facebook" target="_blank"><img src="https://image.freepik.com/free-icon/facebook_318-136394.jpg"/></a></div>
   <div class="col-xs-4" style="padding: 30px;"> <a href="https://twitter.com/vmela_ru" rel="nofollow" title="Мы в Twitter" target="_blank"><img src="https://image.freepik.com/free-icon/twitter_318-136405.jpg"/></a></div>
</noindex>
			</div>
		</div>






	</div>
	<div class="col-sm-4">
		<h3 class="title">Сообщение с сайта:</h3>	
		<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

			<div class="alert alert-success">
				Благодарим Вас за обращение к нам. Мы ответим Вам как можно скорее.
			</div>

			<p>
				<?php if (Yii::$app->mailer->useFileTransport): ?>
					Because the application is in development mode, the email is not sent but saved as
					a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
					Please configure the <code>useFileTransport</code> property of the <code>mail</code>
					application component to be false to enable email sending.
				<?php endif; ?>
			</p>

		<?php else: ?>

			<p>Если у вас есть какие либо вопросы, заполните пожалуйста форму, чтобы связаться с нами.
				Спасибо!</p>

			<div class="row">
				<div class="col-lg-12">

					<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

					<?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

					<?= $form->field($model, 'email') ?>

					<?= $form->field($model, 'subject') ?>

					<?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

					<?=
					$form->field($model, 'verifyCode')->widget(Captcha::className(), [
						'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
					])
					?>

					<div class="form-group">
						<?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
					</div>

					<?php ActiveForm::end(); ?>

				</div>
			</div>

		<?php endif; ?>
	</div>
	<div class="col-sm-4">
		<h3 class="title">Полезная информация:</h3>
		<p>
Полезные ссылки:</p>
		<ul class="list-unstyled">
			<li><i class="glyphicon glyphicon-paperclip"></i> <a href="">Информация о доставке</a></li>
			<li><i class="glyphicon glyphicon-paperclip"></i> <a href="">Условия обслуживания</a></li>
			<li><i class="glyphicon glyphicon-paperclip"></i> <a href="">Ваши заказы</a></li>
			<li><i class="glyphicon glyphicon-paperclip"></i> <a href="">Ваши данные</a></li>

		</ul>
		
	</div>
	<div class="clearfix"></div>
	
	
	<h3 class="title">Где мы находимся:</h3>
	<div class="map row">
<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=bZAaCWZQ2phc00zP0mVmgYWR3j7aLuv9&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
</div>
