<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],

            Yii::$app->user->isGuest ?
                ['label' => 'Login', 'url' => ['/site/login']] :
                [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ],
	          //PAGES      
            ['label' => 'PAGES',
            'items'=>[
				['label' => 'категория', 'url' => ['/users/user/index']],
				['label' => 'добавить категорию', 'url' => ['/users/user/create']],
				['label' => 'главная', 'url' => ['/users/org/index']],				
            ]            
            ],
          //USERS MENU      
            ['label' => 'Users',
            'items'=>[
				'<li class="dropdown-header">Клиенты</li>',
				['label' => 'список юзеров', 'url' => ['/users/user/index']],
				['label' => 'Добавить юзера', 'url' => ['/users/user/create']],
				'<li class="divider"></li>',
				'<li class="dropdown-header">Организации</li>',
				['label' => 'Список организаций', 'url' => ['/users/org/index']],
				['label' => 'Добавить организацию', 'url' => ['/users/org/create']],
				'<li class="divider"></li>',
				'<li class="dropdown-header">Должности</li>',
				['label' => 'Dolzhnost', 'url' => ['/users/dolzhnost/index']],
				['label' => 'Добавить должность', 'url' => ['/users/dolzhnost/create']],
				
            ]            
            ],
            //OBJECTS MENU      
            ['label' => 'Объекты',
            'items'=>[
				['label' => 'Все объекты', 'url' => ['/objects/object/index']],
				['label' => 'Добавить объект', 'url' => ['/objects/object/create']],
				'<li class="divider"></li>',
				['label' => 'Типы объектов', 'url' => ['/objects/objecttype/index']],
				['label' => 'Добавить тип объекта', 'url' => ['/objects/objecttype/create']],
            ]            
            ],
            //CONFIG MENU      
            ['label' => 'Настройки',
            'items'=>[
				['label' => 'Все настройки', 'url' => ['/config/index']],
				['label' => 'Добавить свойство', 'url' => ['/config/create']],
				
				
            ]            
            ],

        ],
    ]);
    NavBar::end();
    ?>

    <div class="container-fluid">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container-fluid">
      <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

<!--          <p class="pull-right"><?= Yii::powered() ?></p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
